import copy
import itertools
import logging
from enum import Enum
from multiprocessing.sharedctypes import Value

import numpy as np
from pycbc.detector import Detector
from pycbc.filter import overlap, sigmasq
from pycbc.types import FrequencySeries, TimeSeries
from pycbc.waveform import get_fd_waveform

from .utils import bilby_to_pycbc_map, remap_dict_keys

logger = logging.getLogger(__name__)


class ProjectedStrainKeys(Enum):
    h = 1
    hprime = 2
    noise = 3


class UnprojectedStrainKeys(Enum):
    h_plus = 1
    h_cross = 2
    hprime_plus_H1 = 3
    hprime_cross_H1 = 4
    noise_H1 = 5
    hprime_plus_L1 = 6
    hprime_cross_L1 = 7
    noise_L1 = 8
    hprime_plus_V1 = 9
    hprime_cross_V1 = 10
    noise_V1 = 11


def compute_hphc_fd(
    param_args,
    approximant,
    delta_f=1 / 8.0,
    f_min=20,
    f_max=512,
):
    """
    Generate hp and hc FrequencySeries

    Inputs:
    -------
    param_args ::dict
        A dictionary of arguments to pass get_fd_waveform; will convert names to pycbc if necessary
    approximant ::str
        The waveform approximant to use
    delta_f ::float
        The frequency step size; equivalent to 1 / duration in time domain
    f_min ::float
        The frequency at which to start waveform generation
    f_max ::float
        The frequency at which to cut the FrequencySeries

    Returns:
    ---------
    hp_tilde ::pycbc.types.FrequencySeries
        The plus polarization component of the incoming strain, in FD
    hc_tilde ::pycbc.types.FrequencySeries
        The cross polarization component of the incoming strain, in FD
    """
    # If 'mass_1' then we are in Bilby convention, and must switch to pycbc convention
    if "mass_1" in param_args:
        param_args = remap_dict_keys(param_args, bilby_to_pycbc_map)

    # Using pycbc get_fd_waveform, **kwargs for all system params
    hptilde, hctilde = get_fd_waveform(
        approximant=approximant,
        f_lower=f_min,
        f_final=f_max,
        delta_f=delta_f,
        **param_args,
    )

    return hptilde, hctilde


def project_and_combine(
    position_args,
    plus_and_cross,
    end_time,
    detector_obj=None,
    ifo=None,
):
    """
    Takes plus and cross, projects them onto given detector

    Parameters
    --------------
    position_args ::dict
        Contains labelled ra, dec, and psi; will convert to pycbc names if necessary
    plus_and_cross ::dict
        Contains hp and hc for the system - can be TD or FD
    end_time ::float
        The gps time at which segment will end (for antenna pattern)
    detector_obj ::pycbc.detector.Detector
        A detector object, for use in computing the antenna pattern.
        If not passed, then will be inferred from ifo if possible.
    ifo ::str
        The IFO name (e.g. H1); only used if detector_obj is not passed.

    Returns
    -------------
    straintilde ::(matches type of values in plus_and_cross)
        The strain projected onto the detector
    """

    if "ra" in position_args:
        position_args = remap_dict_keys(position_args, bilby_to_pycbc_map)

    # If possible use preloaded detector, else make one
    if detector_obj is not None:
        det = detector_obj
    elif ifo is not None:
        det = Detector(ifo)
    else:
        raise ValueError(
            "Cannot determine IFO - one of detector_obj or ifo must not be None"
        )

    # Get antenna pattern
    fp, fc = det.antenna_pattern(t_gps=end_time, **position_args)

    logger.debug(f"project_and_combine: fp, fc are {fp}, {fc}")

    # Combine strains
    strain = fp * plus_and_cross["plus"] + fc * plus_and_cross["cross"]

    return strain


def compute_snr_fd(
    strains,
    psd_dict,
    f_min=20,
    f_max=2048,
):
    """
    Compute optimal network SNR for some set of ifos

    Parameters
    -------
    strains ::dict (str:dict(template=FrequencySeries, =FrequencySeries))
        Computed strain at each IFO,
        If only one will use twice (optimal SNR)
        Should always pass template.
        Can Optionally also pass "data"
        Ex. {'H1' : dict(template=Fseries1, data=Fseries2)}
    psd_dict ::dict (str:FrequencySeries)
        PSDs for each IFO; it is assumed correct interpolation and cleaning has been performed.
    f_min ::float
        Minimum frequency of integration
    f_max ::float
        Maximum frequency of integration

    Returns
    --------
    opt_snr_dict
        Optimal SNRs for each detector, and network SNR
    h_hprime_dict ::dict
        <h' | h> WIP
    h_noise_dict ::dict
        < noise | h > WIP
    """
    # Preliminaries
    opt_snr_dict = dict()
    h_noise_dict = dict()
    h_hprime_dict = dict()
    ifos = psd_dict.keys()
    optnetsnrsq = 0.0

    # Loop over ifos
    for i, ifo in enumerate(ifos):
        # Do a sanity check on the data inputs
        if type(strains[ifo]) != dict:
            raise ValueError("Each ifo entry of the strains object should be a dict")
        if "h" not in strains[ifo].keys():
            raise ValueError(
                "Each ifo entry of the strains object must include a template h"
            )
        for el in strains[ifo].values():
            if type(el) != FrequencySeries and type(el) != TimeSeries:
                raise ValueError(
                    "Each waveform for the ifo must be a pycbc FrequencySeries or TimeSeries"
                )

        # Get the strain template (i.e. clean waveform)
        strain_template = strains[ifo]["h"]

        # Compute optimal SNR with the appropriate pycbc function and args
        opt_snr = np.sqrt(
            float(
                sigmasq(
                    strain_template,
                    psd=psd_dict[ifo],
                    low_frequency_cutoff=f_min,
                    high_frequency_cutoff=f_max,
                )
            )
        )

        # If if hprime and h are passed in, then get the inner product
        if "hprime" in strains[ifo]:
            strain_hprime = strains[ifo]["hprime"]
            # The pycbc overlap function is the appropriately wrapped < d | h >
            # (as long as normalized=False)
            h_hprime_wip = overlap(
                strain_template,
                strain_hprime,
                psd=psd_dict[ifo],
                low_frequency_cutoff=f_min,
                high_frequency_cutoff=f_max,
                normalized=False,
            )
            h_hprime_dict[ifo] = h_hprime_wip
        else:
            h_hprime_dict[ifo] = 0

        if "noise" in strains[ifo]:
            strain_noise = strains[ifo]["noise"]
            # The pycbc overlap function is the appropriately wrapped < d | h >
            # (as long as normalized=False)
            h_noise_wip = overlap(
                strain_template,
                strain_noise,
                psd=psd_dict[ifo],
                low_frequency_cutoff=f_min,
                high_frequency_cutoff=f_max,
                normalized=False,
            )
            h_noise_dict[ifo] = h_noise_wip
        else:
            h_noise_dict[ifo] = 0

        # Set, and track for network
        opt_snr_dict[ifo] = opt_snr
        optnetsnrsq += opt_snr**2

    # Compute final network optimeal SNR
    opt_snr_dict["net"] = np.sqrt(optnetsnrsq)

    # TODO unittest that
    return opt_snr_dict, h_hprime_dict, h_noise_dict


def sky_scatter_snr(
    position_args_arrays,
    component_terms,
    end_time,
    psd_dict,
    detector_obj_dict=None,
    ifo_list=None,
    f_min=20,
    f_max=512,
):
    """
    A function to compute many SNRs over sky position / polarization for the same waveform inputs

    Parameters
    ----------------------
    position_args_arrays ::dict
        A dict containing arrays of values or floats for each of ra, dec, and psi.
        If a parameter is passed as an array,
        it should have the same dimensions as all other parameters passed as arrays
    component_terms ::dict
        keys should be in UnprotectedStrainKeys enum
        plus_template and cross_template are the outputs of get_strain_fd
        plus_data_{ifo} and cross_data_{ifo} are the ifo specific modified +/x, for example by applying calibration
        If (due to, e.g. TGR) these are the same for all detectors, the just pass the same for each accordingly
        noisy_data_{ifo} are non-projected additions to the data, for e.g. noise simulation
        The values should all be FrequencySeries, all with the same dimensions
    psd_dict ::dict (str:FrequencySeries)
        PSDs for each IFO; it is assumed correct interpolation and cleaning has been performed.
    detector_obj_dict ::dict
        A dict of detector objects, labelled by IFO
    ifo ::list
        A list of ifo names to use, if detector_obj_dict is not passed.
        Will accordingly create detectors
    f_min ::float
        Minimum frequency of integration
    f_max ::float
        Maximum frequency of integration

    Returns
    -------------
    opt_snr_arrays
    """
    # Name conversion if necessary
    if "ra" in position_args_arrays:
        position_args_arrays = remap_dict_keys(position_args_arrays, bilby_to_pycbc_map)

    # If possible use preloaded detectors, else make them now
    if detector_obj_dict is not None:
        detector_dict = detector_obj_dict
    elif ifo_list is not None:
        detector_dict = dict()
        for ifo in ifo_list:
            detector_dict[ifo] = Detector(ifo)
    else:
        raise ValueError(
            "Cannot determine IFOs - one of detector_obj_dict or ifo_list must not be None"
        )

    # Check that all inputs are valid
    for key, val in component_terms.items():
        if key not in UnprojectedStrainKeys.__members__:
            raise ValueError(
                # A silly unicode hack in use here
                f"{key} is not a valid key for component_terms.\n \
                Valid keys are:{chr(10).join(UnprojectedStrainKeys._member_names_)}"
            )
        if type(val) != FrequencySeries and type(val) != TimeSeries:
            raise ValueError("Each input term must be a FrequencySeries or TimeSeries")

    # Preliminary setup
    antenna_patterns_dict = dict()
    rho_opt_dict = dict()
    h_hprime_wip_dict = dict()
    h_noise_wip_dict = dict()

    # I want to pregenerate this, but I want convenient dimensions from the loop, so a hack
    rho_opt_net = None

    # Loop over the ifos:
    for ifo_name, detector in detector_dict.items():
        # Get antenna pattern array
        fp, fc = detector.antenna_pattern(t_gps=end_time, **position_args_arrays)
        antenna_patterns_dict[ifo_name] = (fp, fc)

        logger.debug(f"sky_scatter_snr: fp, fc are {fp},{fc}")

        # For cases which have an ifo term, only use the terms corresponding to the current ifo
        component_terms_for_ifo = dict()
        for key, val in component_terms.items():
            if ifo_name in key or key in ["h_plus", "h_cross"]:
                component_terms_for_ifo[key] = val

        # Compute plus and cross inner products
        # Since we are passing in hprime instead of deltas on h
        # There are now 6 terms in the numerator
        # And the standard bottom 3 for the denominator
        inner_products_keys = itertools.product(
            component_terms_for_ifo, ["h_plus", "h_cross"]
        )

        # setup the weighted inner product arrays
        h_h_wip = np.zeros(fp.shape)
        h_hprime_wip = np.zeros(fp.shape)
        h_noise_wip = np.zeros(fp.shape)

        inner_products = dict()
        # Now loop over the combinations
        for ip_key in inner_products_keys:
            # Get the strains
            strain_1 = component_terms_for_ifo[ip_key[0]]
            strain_2 = component_terms_for_ifo[ip_key[1]]

            # Overlap will always work in this case, so use it for convenience
            non_norm_overlap = overlap(
                strain_1,
                strain_2,
                psd=psd_dict[ifo_name],
                low_frequency_cutoff=f_min,
                high_frequency_cutoff=f_max,
                normalized=False,
            )

            inner_products[ip_key] = non_norm_overlap

            # A silly hack to get the appropriate number of Fplus and Fcross to use
            plus_power = len([x for x in ip_key if "plus" in x])
            cross_power = len([x for x in ip_key if "cross" in x])

            # We want separately compute inner products which are entirely template dependent
            # (i.e. those which go to optimal SNR)
            # and those which relate to the data
            if ifo_name not in ip_key[0]:
                # Non-templates will always be on the left
                # Catch the double count when doing the h
                h_h_wip += (
                    non_norm_overlap
                    * np.power(fp, plus_power)
                    * np.power(fc, cross_power)
                )
            elif "hprime" in ip_key[0]:
                # The case of hprime_{plus/cross}_ifo | h_{plus/cross}
                h_hprime_wip += (
                    non_norm_overlap
                    * np.power(fp, plus_power)
                    * np.power(fc, cross_power)
                )
            elif "noise" in ip_key[0]:
                # The case of noise_ifo | h_{plus/cross}
                h_noise_wip += (
                    non_norm_overlap
                    * np.power(fp, plus_power)
                    * np.power(fc, cross_power)
                )

        # Get the final arrays of values, and write them into the ifo entry
        rho_opt_dict[ifo_name] = np.sqrt(h_h_wip)
        h_hprime_wip_dict[ifo_name] = h_hprime_wip
        h_noise_wip_dict[ifo_name] = h_noise_wip

        # A silly exercise to build up network SNR
        if rho_opt_net is None:
            rho_opt_net = h_h_wip
        else:
            rho_opt_net += h_h_wip

    # Finally get network SNRs
    rho_opt_dict["net"] = np.sqrt(rho_opt_net)

    return rho_opt_dict, h_hprime_wip_dict, h_noise_wip_dict
