import copy
import enum
import logging
from textwrap import fill
from typing import Type

import numpy as np
from astropy import units as u
from astropy.cosmology import units as cu
from bilby.core.prior import Interped
from bilby.core.prior.conditional import (
    IllegalRequiredVariablesException,
    conditional_prior_factory,
)
from bilby.core.prior.joint import BaseJointPriorDist, JointPrior
from bilby.gw.cosmology import z_at_value
from bilby.gw.prior import Cosmological
from scipy.interpolate import interp1d

logger = logging.getLogger(__name__)


class LuminosityDistanceCosmological(Cosmological):
    def __init__(
        self,
        noncosmo_prior,
        cosmology=None,
        name=None,
        minimum=None,
        maximum=None,
        latex_label=None,
        unit=None,
        boundary=None,
    ):
        """
        Produce a Cosmological Luminosity Distance prior

        Parameters
        ----------
        noncosmo_prior ::bilby.core.prior.Prior
            The base prior, before cosmology.
            e.g. PowerLaw(alpha=2, minimum=1, maximum=5000, name="luminosity_distance", unit=u.Mpc)
        cosmology
            See superclass
        name
            See superclass
        minimum
            See superclass
        maximum
            See superclass
        latex_label
            See superclass
        unit
            See superclass
        boundary
            See superclass
        """
        # If not passed, use the priors arguments for these
        if minimum is None:
            minimum = noncosmo_prior.minimum
        if maximum is None:
            maximum = noncosmo_prior.maximum
        if name is None:
            name = noncosmo_prior.name
        if latex_label is None:
            latex_label = noncosmo_prior.latex_label
        # Set to astropy units for later
        if unit is None:
            if name == "luminosity_distance":
                unit = u.Mpc
            elif name == "redshift":
                unit = cu.redshift
        if boundary is None:
            boundary = noncosmo_prior.boundary
        # keep the prior
        self.noncosmo_prior = noncosmo_prior
        super(LuminosityDistanceCosmological, self).__init__(
            minimum,
            maximum,
            cosmology=cosmology,
            name=name,
            latex_label=latex_label,
            unit=unit,
            boundary=boundary,
        )

    def _get_redshift_arrays(self):
        """
        Necessary method to allow conversion
        """
        if self.name == "luminosity_distance":
            # If luminosity distance, make grid of zs by conversion
            z_min = z_at_value(
                self.cosmology.luminosity_distance, self.minimum * u.Unit(self.unit)
            )
            z_max = z_at_value(
                self.cosmology.luminosity_distance, self.maximum * u.Unit(self.unit)
            )
            zs = np.linspace(z_min, z_max, 1000)
            # Get dls at each z value
            dls = self.cosmology.luminosity_distance(zs).value
            # Get the jacobian
            ddl_dz = np.gradient(dls, zs)
            # Compute p(dL)
            p_ddl = self.noncosmo_prior.prob(dls)
            # Convert to p(z) with jacobian
            p_dz = p_ddl * ddl_dz
        elif self.name == "redshift":
            # If redshift, make the grid directly
            zs = np.linspace(self.minimum, self.maximum, 1000)
            # Get dLs
            dls = self.cosmology.luminosity_distance(zs).value
            # Get Jacobian
            ddl_dz = np.gradient(dls, zs)
            # Get p(dL)
            p_ddl = self.noncosmo_prior.prob(dls)
            # Convert to p(z) with jacobian
            p_dz = p_ddl * ddl_dz
        return zs, p_dz


class RedshiftedPrior(Interped):
    """
    From https://git.ligo.org/colm.talbot/tbs/-/blob/master/tbs/prior.py#L51
    """

    def __init__(
        self,
        unredshifted_prior,
        redshift_prior,
        name=None,
        latex_label=None,
        unit=None,
        boundary=None,
    ):
        """
        Generate the redshifted, lab-frame, prior given a source-frame prior and
        a redshift prior.

        p(detector_mass) = \\int dz \\int dm p(z, m) delta(detector_mass - m * (1 + z))

        Parameters
        ----------
        unredshifted_prior: bilby.core.prior.Prior
            Prior which you want to redshift
        redshift_prior: bilby.core.prior.Prior
        name: str
        latex_label: str
        unit: str
        """
        # Get the grid of redshifted masses
        unredshifted_masses = unredshifted_prior.rescale(
            np.linspace(0 + 1e-8, 1 - 1e-8, 1000)
        )
        # Convert to redshift prior
        redshift_prior = self._convert_redshift_prior(redshift_prior)
        # Get grid of redshifts
        redshifts = redshift_prior.rescale((np.linspace(0, 1, 1000)))
        # Find minimum and maximum detector frame masses
        new_min = unredshifted_masses[0] * (1 + redshifts[0])
        new_max = unredshifted_masses[-1] * (1 + redshifts[-1])
        # Grid in detector frame masses
        xx = np.linspace(new_min, new_max, 101)
        # For grid of detector frame masses, get corresponding probability from redshift probability
        # and source mass probability via integration
        yy = [
            np.trapz(
                np.nan_to_num(
                    unredshifted_prior.prob(x / (1 + redshifts))
                    / (1 + redshifts)
                    * redshift_prior.prob(redshifts)
                ),
                redshifts,
            )
            for x in xx
        ]
        # Set the priors for future use
        self.unredshifted_prior = unredshifted_prior
        self.redshift_prior = redshift_prior
        # Initialize with Interped superclass
        Interped.__init__(
            self,
            xx=xx,
            yy=yy,
            minimum=new_min,
            maximum=new_max,
            name=name,
            latex_label=latex_label,
            unit=unit,
            boundary=boundary,
        )

    @staticmethod
    def _convert_redshift_prior(redshift_prior):
        """
        Produces a redshift prior if the cosmological prior provided waas not redshift

        Parameters
        -------------
        redshift_prior ::bilby.core.prior.Prior
            The prior which this will attempt to convert

        Returns
        ------------
        redshift_prior ::bilby.core.prior.Prior
            The converted redshift prior
        """
        if (
            isinstance(redshift_prior, Cosmological)
            and not redshift_prior.name == "redshift"
        ):
            logger.debug(
                f"{redshift_prior.name} prior provided, converting to redshift"
            )
            return redshift_prior.get_corresponding_prior("redshift")
        else:
            return redshift_prior


class RedshiftGivenLabMass(Interped):
    def __init__(
        self,
        detector_mass=0,
        minimum=None,
        maximum=None,
        source_mass_prior=None,
        detector_mass_prior=None,
        redshift_prior_unconditioned=None,
        name=None,
        latex_label=None,
        unit=None,
        boundary=None,
    ):
        if redshift_prior_unconditioned.name == "redshift":
            self.redshift_prior_unconditioned = redshift_prior_unconditioned
        elif isinstance(redshift_prior_unconditioned, Cosmological):
            self.redshift_prior_unconditioned = (
                redshift_prior_unconditioned.get_corresponding_prior("redshift")
            )
        else:
            raise TypeError(
                "Cannot form redshift prior from given redshift prior \
                - not named 'redshift' and not an instance of Cosmological"
            )
        self.xx = None
        self.source_mass_prior = source_mass_prior
        self.detector_mass_prior = detector_mass_prior

        self.detector_mass = detector_mass

        xx, yy = self.get_p_z_given_detector_mass_on_grid()

        Interped.__init__(
            self,
            xx=xx,
            yy=yy,
            name=name,
            latex_label=latex_label,
            unit=unit,
            boundary=boundary,
        )

    @property
    def detector_mass(self):
        return self._detector_mass

    @detector_mass.setter
    def detector_mass(self, detector_mass):
        self._detector_mass = detector_mass
        if self.xx is not None:
            self._update_instance()

    @property
    def yy(self):
        """Return p(xx) values of the interpolated prior function.

        Updates the prior distribution if it is changed

        Returns
        =======
        array_like: p(xx) values

        """
        return self._yy

    @yy.setter
    def yy(
        self,
    ):
        self._update_instance()

    def _update_instance(self):
        xx, yy = self.get_p_z_given_detector_mass_on_grid()

        self.xx = xx
        self._yy = interp1d(xx, yy, bounds_error=False, fill_value=0)(self.xx)
        self._initialize_attributes()

    def get_p_z_given_detector_mass_on_grid(self):
        redshifts = self.redshift_prior_unconditioned.rescale(np.linspace(0, 1, 1000))
        source_mass_grid = self.detector_mass / (1 + redshifts)
        yy = np.nan_to_num(
            self.source_mass_prior.prob(source_mass_grid)
            * self.redshift_prior_unconditioned.prob(redshifts)
            / (1 + redshifts)
            / self.detector_mass_prior.prob(self.detector_mass)
        )

        return redshifts, yy

    def _initialize_attributes(self):
        from scipy.integrate import cumtrapz

        if np.trapz(self._yy, self.xx) != 1:
            logger.debug(
                f"Supplied PDF for {self.name} is not normalised, normalising."
            )
        self._yy /= np.trapz(self._yy, self.xx)
        self.YY = cumtrapz(self._yy, self.xx, initial=0)
        # Need last element of cumulative distribution to be exactly one.
        self.YY[-1] = 1
        self.probability_density = interp1d(
            x=self.xx, y=self._yy, bounds_error=False, fill_value=0
        )
        self.cumulative_distribution = interp1d(
            x=self.xx, y=self.YY, bounds_error=False, fill_value=(0, 1)
        )
        self.inverse_cumulative_distribution = interp1d(
            x=self.YY, y=self.xx, bounds_error=True
        )


class RedshiftConditionedOnLabMass(conditional_prior_factory(RedshiftGivenLabMass)):
    def __init__(
        self,
        condition_func,
        source_mass_prior=None,
        detector_mass_prior=None,
        redshift_prior_unconditioned=None,
        name=None,
        latex_label=None,
        unit=None,
        boundary=None,
        **reference_params,
    ):
        if redshift_prior_unconditioned.name == "redshift":
            redshift_prior_unconditioned = redshift_prior_unconditioned
        elif isinstance(redshift_prior_unconditioned, Cosmological):
            redshift_prior_unconditioned = (
                redshift_prior_unconditioned.get_corresponding_prior("redshift")
            )
        else:
            raise TypeError(
                "Cannot form redshift prior from given redshift prior \
                - not named 'redshift' and not an instance of Cosmological"
            )
        reference_params.update(
            source_mass_prior=source_mass_prior,
            detector_mass_prior=detector_mass_prior,
            redshift_prior_unconditioned=redshift_prior_unconditioned,
        )
        self.source_mass_prior = source_mass_prior
        self.detector_mass_prior = detector_mass_prior
        self.redshift_prior_unconditioned = redshift_prior_unconditioned

        super(RedshiftConditionedOnLabMass, self).__init__(
            condition_func=condition_func,
            name=name,
            latex_label=latex_label,
            unit=unit,
            boundary=boundary,
            **reference_params,
        )

    def update_conditions(self, **required_variables):
        """
        This method updates the conditional parameters (depending on the parent class
        this could be e.g. `minimum`, `maximum`, `mu`, `sigma`, etc.) of this prior
        class depending on the required variables it depends on.

        If no variables are given, the most recently used conditional parameters are kept

        Parameters
        ==========
        required_variables:
            Any required variables that this prior depends on. If none are given,
            self.reference_params will be used.

        """
        if sorted(list(required_variables)) == sorted(self.required_variables):
            parameters = self.condition_func(
                self.reference_params.copy(), **required_variables
            )
            for key, value in parameters.items():
                setattr(self, key, value)
        elif len(required_variables) == 0:
            return
        else:
            raise IllegalRequiredVariablesException(
                "Expected kwargs for {}. Got kwargs for {} instead.".format(
                    self.required_variables, list(required_variables.keys())
                )
            )


################################################################
################################################################
####     Currently Broken / Deprecated but not Removed      ####
################################################################
################################################################


# class JointInterped(BaseJointPriorDist):
#     def __init__(self, xx_i: list, yy: np.ndarray, names:list) -> None:
#         """
#         Gets an Interped2D prior for joint distribution (x, y)

#         Parameters
#         ---------
#         xx_i ::list
#             Each element is an input grid
#         yy ::np.ndarray
#             The probability grid over the xx_i
#             Will be normalized if necessary
#             For dimensions n_i of xx_i, should be ndarray of shape
#             (n_0, n_1,...n_i)
#         names ::list
#             The names of the input parameters
#         bounds ::list
#             The bounds on the input parameters, default np.min, np.max of xx_i inputs
#         """
#         super(JointInterped, self).__init__(names=names, bounds=None)
#         self.xx_i = xx_i
#         for ii, name in enumerate(self.names):
#             if self.bounds[name] == (-np.inf, np.inf):
#                 self.bounds[name] = (np.min(xx_i[ii]), np.max(xx_i[ii]))
#         self._yy = yy
#         self.probability_density = None
#         self.cumulative_distribution = None
#         self.inverse_cumulative_distribution = None
#         self.__all_interpolated = LinearNDInterpolator(self.xx_i, self._yy)
#         self._update_instance()

#     def __eq__(self, other):
#         if self.__class__ != other.__class__:
#             return False
#         try:
#             for ii, xx in enumerate(self.xx_i):
#                 if xx != other.xx_i[ii]:
#                     return False
#                 if self.names[ii] != other.names[ii]:
#                     return False
#                 if self._yy != other._yy:
#                     return False
#         except (IndexError, KeyError) as exception:
#             logger.info(exception)
#             return False
#         return True

#     def _update_instance(self):
#         pass

#     def _initialize_attributes(self):
#         from scipy.integrate import cumtrapz
#         normalization_factor = copy.deepcopy(self._yy)
#         for ii in range(len(self.xx_i)):
#             normalization_factor = np.trapz(normalization_factor, x=self.xx_i[ii], axis=0)
#         if normalization_factor != 1:
#             logger.info(f"Distribution was not normalized,\
#                  automatically normalizing by dividing input by {normalization_factor}")
#             self._yy /= normalization_factor
#         ln_yy = np.log(self.yy)
#         self.ln_probability_density = LinearNDInterpolator(self.xx_i, ln_yy)
#         # Note - cannot return CDF, ICDF, or execute sample methods


#     def _ln_prob(self, samp, lnprob, outbounds):
#         return self.ln_probability_density(samp)

#     def generate_conditional(self, condition_variables):
#         for ii, var in enumerate(condition_variables):


#     def _sample(self, size, **kwargs):
#         logger.info("No sampling method currently implemented")
#         pass

#     def _rescale(self, samp, **kwargs):
#         logger.info("No rescaling method currently implemented")
#         pass
