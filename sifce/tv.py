import copy
import logging
from re import M

import astropy.units as u
import numpy as np
import pandas as pd
from astropy import constants as const
from astropy.cosmology import z_at_value
from bilby.gw import conversion, cosmology
from scipy import integrate, interpolate, stats
from tqdm import tqdm

from sifce.sims import SimulationSet

logger = logging.getLogger(__name__)


class TimeVolume:
    """
    A class for methods related to the calculation of the sensitive time-volume

    """

    def __init__(self, simulation: SimulationSet) -> None:
        """
        Constructor for the class
        Parameters
        --------------
        simulation ::SimulationSet obj
            SimulationSet object
        function_detection ::python function
            The function that returns the detection probability given a row
        """
        self.simset = simulation
        self.cosmo_model = cosmology.get_cosmology(simulation.cosmology_model)

    def get_sensitive_volume(self, importance_sampling=False):

        max_redshift = self.simset.uniform_source_frame_distribution.maximum
        v0 = self.get_v_naught(max_redshift)

        duration = (
            self.simset.sampling_distribution["geocent_time"].maximum
            - self.simset.sampling_distribution["geocent_time"].minimum
        ) * u.s

        coefficient = duration * v0

        population_weights = np.exp(
            copy.deepcopy(
                self.simset.detection_dataframe["ln_prob_usf_vs_sampling_distance"]
            )
        )

        if importance_sampling:
            population_weights *= np.exp(
                self.simset.detection_dataframe["ln_prob_target_vs_usf_distance"]
            )
            population_weights *= np.exp(
                self.simset.detection_dataframe["ln_target_prob"]
                - self.simset.detection_dataframe["ln_sampling_prob"]
            )

        vt_sens_dict = dict()
        vt_variance_dict = dict()
        eff_dict = dict()

        for ii, detection_function in enumerate(self.simset.detection_function_names):
            detection_weights = self.simset.detection_dataframe[f"p_det_{ii}"]

            weights = population_weights * detection_weights

            mc_sum = np.sum(weights) / weights.shape[0]

            n_dist = (
                self.simset.detection_dataframe[
                    self.simset.detection_dataframe["intrinsic_index"]
                    == self.simset.detection_dataframe["intrinsic_index"].min()
                ]["luminosity_distance"]
                .unique()
                .shape[0]
            )

            intrinsic_index_combined = np.average(
                weights.array.reshape(-1, n_dist), axis=1
            )
            variance_sum = (
                np.sum(intrinsic_index_combined**2)
                / intrinsic_index_combined.shape[0]
                - mc_sum**2
            )

            # variance_sum = (
            #     np.sum(weights ** 2) / weights.shape[0] - mc_sum ** 2
            # )

            vt_sens = (coefficient * mc_sum).to(u.Gpc**3 * u.yr)
            vt_variance = (coefficient * variance_sum).to(u.Gpc**3 * u.yr)

            vt_sens_dict[ii] = vt_sens
            vt_variance_dict[ii] = vt_variance
            eff_dict[ii] = (
                np.sum(population_weights) ** 2
                / np.sum(population_weights**2)
                / weights.shape[0]
            )

        return (
            vt_sens_dict,
            (
                self.simset.detection_function_names,
                self.simset.detection_function_kwargs,
            ),
            eff_dict,
            vt_variance_dict,
        )

    def get_v_naught(self, max_redshift):
        """
        A function that will calculate the integral of C(z)DL^2dz over a grid of redshifts
        to get the normalizing V_0 for a uniform in source frame distribution

        Parameters
        ----------------------
        max_redshift ::float
            maximum redshift for the integral computation

        Returns
        -------------
        v0
            The surveyed hypervolume for the uniform source frame distribution at this maximum dL
        """
        # TODO generalize to distributions other than uniform in source frame

        # Generate zs grid
        zs = np.linspace(0, max_redshift, 1000)

        # Get corresponding dLs
        dLs = self.cosmo_model.luminosity_distance(zs)

        # Get E Function at each redshift
        es = self.cosmo_model.efunc(zs)

        # The integrand is now D_L(z)^2 / (1 + z)^3 / E(z)
        integrand = dLs**2 / es / (1 + zs) ** 3
        integral = integrate.trapz(integrand, x=zs)

        v0 = integral * const.c / self.cosmo_model.H(0) * 4 * np.pi

        return v0.to(u.Gpc**3)

    ################################################################
    ################################################################
    ####                        Deprecated                      ####
    ################################################################
    ################################################################

    # def sensitive_volume_monte_carlo(self):
    #     """
    #     A function that will calculate the sensitive time-volume
    #     of the simulations dataframe using direct Monte Carlo methods
    #     Assumes that sampling distribution == target distribution

    #     Parameters
    #     ----------------------

    #     Returns
    #     -------------
    #     sensitive-volume: integer
    #     """
    #     if self.simset.sampling_distribution != self.simset.target_distribution:
    #         raise ValueError(
    #             "Monte Carlo sampling is not robust if the target distribution does not match\
    #             the sampline distribution"
    #         )

    #     max_distance = np.max(self.simset.simulations_dataframe["luminosity_distance"])
    #     f = self.get_integral_dredshift_function(max_distance)

    #     if self.simset.simulations_dataframe["redshift"].isnull().values.any():
    #         self.simset.simulations_dataframe = self.simset.fill_out_redshift(
    #             self.simset.simulations_dataframe, self.simset.cosmology_model
    #         )
    #     z_list = self.simset.simulations_dataframe["redshift"]

    #     n_total = self.simset.simulations_dataframe["p_det"].count()
    #     total_sum = np.sum(f(z_list) * self.simset.simulations_dataframe["p_det"])
    #     total_sum = total_sum * u.Mpc**2

    #     # calculate sensitive time-volume
    #     h0 = self.cosmo_model.H(0)

    #     # Get the duration in seconds from the prior
    #     duration = (
    #         self.simset.sampling_distribution["geocent_time"].maximum
    #         - self.simset.sampling_distribution["geocent_time"].minimum
    #     )

    #     volume_sens = (
    #         (duration * u.second) * (const.c / h0) * 4 * np.pi * total_sum / n_total
    #     )

    #     return volume_sens.to(u.Gpc**3 * u.yr)

    # def sensitive_volume_importance_sampling(self):
    #     """
    #     A function that will calculate the sensitive time-volume
    #     of the simulations dataframe using importance sampling MC methods
    #     """
    #     # Compute preliminaries - probability of detection and cosmology
    #     self.simset.simulations_dataframe["p_det"] = self.function(
    #         self.simset.simulations_dataframe
    #     )
    #     max_distance = np.max(self.simset.simulations_dataframe["luminosity_distance"])
    #     f = self.get_integral_dredshift_function(max_distance)

    #     # Get z values, assign to dataframe
    #     if self.simset.simulations_dataframe["redshift"].isnull().values.any():
    #         self.simset.simulations_dataframe = self.simset.fill_out_redshift(
    #             self.simset.simulations_dataframe, self.simset.cosmology_model
    #         )
    #     z_list = self.simset.simulations_dataframe["redshift"]

    #     # Compute the sum
    #     n_total = self.simset.simulations_dataframe["p_det"].count()
    #     probability_ratio = np.exp(
    #         self.simset.simulations_dataframe["ln_target_prob"]
    #         - self.simset.simulations_dataframe["ln_sampling_prob"]
    #     )
    #     weighted_sum = np.sum(
    #         f(z_list) * self.simset.simulations_dataframe["p_det"] * probability_ratio
    #     )
    #     weighted_sum = weighted_sum * u.Mpc**2

    #     # Calculate sensitive time-volume
    #     h0 = self.cosmo_model.H(0)

    #     # Get the duration in seconds from the prior
    #     duration = (
    #         self.simset.sampling_distribution["geocent_time"].maximum
    #         - self.simset.sampling_distribution["geocent_time"].minimum
    #     )

    #     volume_sens = (
    #         (duration * u.second) * (const.c / h0) * 4 * np.pi * weighted_sum / n_total
    #     )

    #     return volume_sens.to(u.Gpc**3 * u.yr)
