import configparser
import copy
import logging
from pydoc import describe

import bilby
import configargparse as cargparse
import numpy as np

from .network import Network
from .sims import SimulationSet

logger = logging.getLogger(__name__)


def parse(config_file=None, manual_args=[]):
    """
    The main parse method for SIFCE

    Parameters
    ----------------
    config_file ::str
        Optionally a config file (if this is being called from a script, instead of with command line)
    manual_args ::list
        Any other arguments, formed in the standard argparse way.
        Could technically include config_file, but it is separated out for clarity.

    Returns
    -----------------
    arguments ::Namespace
        A Namespace object, resulting from parsing the args which were passed
    parser ::ArgumentParser
        An ArgumentParser, which was used for parsing the arguments
    """
    parser = cargparse.ArgumentParser(
        config_file_parser_class=cargparse.ConfigparserConfigFileParser
    )
    parser.add_argument(
        "config",
        is_config_file=True,
        type=str,
        help="The .cfg which should be interpreted",
    )

    # Waveform argument group
    parser_waveform = parser.add_argument_group(
        title="Waveform",
        description="Arguments concerning the conditioning of the data or the waveform generation",
    )
    parser_waveform.add_argument(
        "--delta-f",
        type=float,
        help="The delta_f at which to generate the waveform and perform analysis, equivalent to 1 / duration for TD",
    )
    parser_waveform.add_argument(
        "--f-min",
        type=float,
        default=20,
        help="The minimum frequency of integration, and of waveform generation for FD waveforms",
    )
    parser_waveform.add_argument(
        "--f-max",
        type=float,
        default=1024,
        help="The maximum frequency of integration",
    )
    parser_waveform.add_argument(
        "--injection-approximant",
        type=str,
        default=None,
        help="The injection ('data') waveform. Optional, if not passed then assume injection=recovery",
    )
    parser_waveform.add_argument(
        "--recovery-approximant",
        type=str,
        default="IMRPhenomXPHM",
        help="The recovery waveform. If no injection waveform is passed, injection=recovery",
    )

    # SNR argument group
    parser_snr = parser.add_argument_group(
        title="SNR",
        description="Arguments for computing SNRs, especially when using cases beyond optimal SNR",
    )
    parser_snr.add_argument(
        "--sky-scatter",
        action="store_true",
        default=False,
        help="If true, use the sky scattering method to sample over the sky/polarization angles",
    )
    parser_snr.add_argument(
        "--distance-scaling",
        action="store_true",
        default=False,
        help="If true, use distance scaling methods. Requires checks on distribution validity!",
    )
    parser_snr.add_argument(
        "--sky-average",
        action="store_true",
        default=False,
        help="Whether to average the SNRs over sky locations when sky scattering\
            Only has an effect if --sky-scatter is flagged True",
    )
    parser_snr.add_argument(
        "--distance-ref-mpc",
        type=float,
        default=1.0,
        help="The reference distance to use if distance scaling\
            Only used if --distance-scaling is flagged true",
    )

    # Distribution argument group
    parser_distribution = parser.add_argument_group(
        title="Distribution",
        description="Arguments governing the underlying distribution for a simulation set",
    )
    parser_distribution.add_argument(
        "--sampling-prior-file",
        type=str,
        help="The path to the .prior file which encodes the distribution",
    )
    parser_distribution.add_argument(
        "--target-prior-file",
        type=str,
        default=None,
        help="The path to the .prior file which encodes the distribution",
    )
    parser_distribution.add_argument(
        "--cosmology-model",
        type=str,
        default="Planck18",
        help="The cosmological model (per astropy standard names) to use for cosmological computations",
    )
    parser_distribution.add_argument(
        "--reduce-dmax-to-observable",
        action="store_true",
        default=False,
        help="If true, reduce the dmax in the distribution to a value\
        which will limit the number of unused configurations\
        Don't use in Condor mode right now! Workers do not presently synchronize this",
    )

    # Network argument group
    parser_network = parser.add_argument_group(
        title="Network",
        description="Arguments for IFO livetime and PSDs",
    )
    parser_network.add_argument(
        "--segments-file",
        type=str,
        default=None,
        help="The path to the .segments file which encodes the IFO live segments",
    )
    parser_network.add_argument(
        "--psds-file",
        type=str,
        default=None,
        help="The .psds file which encodes the psds to use various times",
    )
    parser_network.add_argument(
        "--asd-sensitivity-files",
        action="store_true",
        default=False,
        help="If True, treat the files in the psds file as ASDs",
    )
    parser_network.add_argument(
        "--calibrations-file",
        type=str,
        default=None,
        help="The path to the calibrations configuration file",
    )

    # Pipeline argument group
    parser_pipeline = parser.add_argument_group(
        title="Pipeline",
        description="Arguments which describe the pipeline, including sampling methods, optimizations, etc.",
    )
    # TODO should be an enum I guess? Not sure how to implement that
    parser_pipeline.add_argument(
        "--sampling-method",
        type=str,
        default="MonteCarlo",
        help="Whether to create the SimulationSet by sampling the target distribution ('MonteCarlo'),\
            by sampling some sampling distribution and then importance sampling ('Importance')\
            Or by some other method",
    )
    parser_pipeline.add_argument(
        "--num-int-samples",
        type=int,
        help="The number of intrinsic samples\
            If being used in condor mode this is number of intrinsic samples *per worker*",
    )
    parser_pipeline.add_argument(
        "--num-sky-scatter",
        type=int,
        default=5000,
        help="If sky scattering, this is the default number of points to use for the sky scatter",
    )
    parser_pipeline.add_argument(
        "--num-distance-scale",
        type=int,
        default=1000,
        help="If distance scaling, this is the default number of points for distance scaling",
    )
    parser_detection = parser.add_argument_group(
        title="Detection",
        description="Arguments for the detection function",
    )
    # TODO this should be an enum
    parser_detection.add_argument(
        "--detection-function-name-list",
        action="append",
        help="The detection function to use. Current options are:\
            'thres_step_net' for snr_net threshold\
            'thres_fake_gauss_net' for snr_net with gaussian random",
    )
    parser_detection.add_argument(
        "--detection-function-kwargs-list",
        action="append",
        help="Kwargs for named detection functions passed in",
    )

    parser_condor = parser.add_argument_group(
        title="Condor",
        description="Arguments governing the construction of the condor job",
    )
    parser_condor.add_argument(
        "--number-workers",
        type=int,
        help="The number of condor worker jobs to spawn",
    )
    parser_condor.add_argument(
        "--ligo-accounting",
        type=str,
        default=None,
        help="The accounting group to use for LIGO condor",
    )
    parser_condor.add_argument(
        "--ligo-user-name",
        type=str,
        default=None,
        help="The user name to use when submitting to LIGO HTCondor",
    )
    parser_condor.add_argument(
        "--pipeline-dir",
        type=str,
        help="The output directory",
    )
    parser_condor.add_argument(
        "--request-memory",
        type=int,
        default=8,
        help="The amount of RAM to request, in Gb",
    )

    if config_file is None and manual_args == []:
        arguments = parser.parse_args()
    else:
        arguments = parser.parse_args([config_file] + manual_args)

    return arguments, parser


def write_altered_config(
    arguments,
    parser,
    output_path,
    argument_groups_to_pop=["positional arguments", "options"],
    arguments_to_pop=["config"],
):
    """
    A function to take a parsed configargparser object, clean it up, and write it to a .cfg

    Parameters
    ---------------
    arguments ::Namespace
        The arguments parsed out of some ArgumentParser
    parser ::ArgumentParser
        The parser associated with the arguments
    output_path ::str
        The path for the new config file
    argument_groups_to_pop ::list
        A list containing the names of argument groups which should be popped out of the config
        e.g. 'parser_condor' to remove condor arguments
    arguments_to_pop ::list
        A list containing individual argument names which should be removed, e.g. 'config'

    Writes
    ----------------
    Parsed Config
        A config file which contains the full arguments (both config and command line) from the parser
        Grouped by argument group
    """
    # Dictionaries are easy to work with
    arguments_dict = arguments.__dict__

    # Special hack for cosmologies
    if type(arguments.cosmology_model) != str:
        arguments.cosmology_model = arguments.cosmology_model.name

    # Use the parser to get the argument groups, which should correspond to section headers
    argument_groups = copy.copy(parser._action_groups)

    # This will be a dict of all the arguments and their values, grouped by argument group
    grouped_argument_names = dict()

    # Loop over argument groups
    for group in argument_groups:
        # Preset the internal dictionary for the group
        parameter_dict = dict()
        # For each action in the group
        # Use the name to get the value from the argument group
        # Then make the key value pair
        for action in group._group_actions:
            action_name = action.dest
            if action_name not in ["help"]:
                parameter_dict[action_name] = arguments_dict[action_name]
        # Write the internal dict to the larger dict, under the group name
        grouped_argument_names[group.title] = parameter_dict

    # Copy out the dictionary now, as we prepare to clean it
    cleaned_grouped_arguments = copy.copy(grouped_argument_names)
    cleaned_grouped_arguments_keys = list(cleaned_grouped_arguments.keys())

    # Loop over groups
    for group in cleaned_grouped_arguments_keys:
        # Remove the whole group if appropriate
        if group in argument_groups_to_pop:
            cleaned_grouped_arguments.pop(group)
            continue
        # Otherwise, loop over arguments in the pop_list and pop them if they are in this group
        for key in arguments_to_pop:
            if key in cleaned_grouped_arguments[group]:
                cleaned_grouped_arguments[group].pop(key)

        # Set original keys to loop over
        original_cleaned_group_keys = list(cleaned_grouped_arguments[group].keys())

        # Now cast the rest back to strings
        for key in original_cleaned_group_keys:
            named_value = cleaned_grouped_arguments[group].pop(key)
            cleaned_grouped_arguments[group][key.replace("_", "-")] = str(named_value)

    # Make a new parser, to write to the config
    writing_parser = configparser.ConfigParser()
    # Looping over groups again, assign section headers for each group
    for group in cleaned_grouped_arguments:
        writing_parser[group] = cleaned_grouped_arguments[group]
    # write it all to the output path
    with open(output_path, "w") as f:
        writing_parser.write(f)


def construct_network_from_parsed_arguments(arguments):
    network = Network(
        ifo_uptime_file=arguments.segments_file,
        psd_config_file=arguments.psds_file,
        f_min=arguments.f_min,
        f_max=arguments.f_max,
        delta_f=arguments.delta_f,
        injection_approximant=arguments.injection_approximant,
        recovery_approximant=arguments.recovery_approximant,
        curves_are_asds=arguments.asd_sensitivity_files,
        calibration_config_file=arguments.calibrations_file,
    )
    network.load_psds()

    return network


def construct_simulationset_from_parsed_arguments(arguments):
    sims = SimulationSet(
        sampling_distribution=arguments.sampling_prior_file,
        target_distribution=arguments.target_prior_file,
        cosmology_model=arguments.cosmology_model,
        network=construct_network_from_parsed_arguments(arguments),
        sky_scatter=arguments.sky_scatter,
        distance_scale=arguments.distance_scaling,
        sky_average=arguments.sky_average,
        d_ref_mpc=arguments.distance_ref_mpc,
        reduce_dmax_to_observable=arguments.reduce_dmax_to_observable,
        detection_function_names_list=arguments.detection_function_name_list,
        detection_function_kwargs_list=arguments.detection_function_kwargs_list,
    )
    return sims
