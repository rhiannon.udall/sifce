from enum import Enum, auto

import numpy as np
from scipy import stats


def get_step_function(threshold=10, snr_type="opt"):
    """
    Returns a step function with the given threshold for the opt_snr_net

    Parameters
    ----------------------
    threshold ::int
        the step function threshold
    Returns
    -------------
    step: function
    """

    def step(df):
        return (df[f"{snr_type}_snr_net"] > threshold).astype(float)

    return step


def get_fake_gaussian_noise_snr_thresh(threshold=10, snr_name="opt_snr"):
    """
    Returns a function which determines the likelihood the actual SNR will be above the threshold
    under the assumption actual SNR is a Gaussian RV centered at the input SNR and with sigma=1

    Parameters
    ----------------------
    threshold ::int
        the step function threshold
    Returns
    -------------
    gaussian_simmed_step: function

    """

    def gaussian_simmed_step(df):
        # make a normal distribution
        normal = stats.norm()
        ifo_snr_keys = [
            key for key in df.keys() if snr_name in key and "net" not in key
        ]
        noise_snrs = dict()
        net_snr_sq = np.zeros(df[ifo_snr_keys[0]].shape[0])
        for key in ifo_snr_keys:
            noise_snrs[key] = normal.rvs(size=df[key].shape[0])
            ifo_snr_temp = df[key] + noise_snrs[key]
            net_snr_sq += ifo_snr_temp**2
        net_snr = np.sqrt(net_snr_sq)
        return (net_snr > threshold).astype(float)

    return gaussian_simmed_step


class StandardDetectionFunctions(Enum):
    thres_step_net = (1, get_step_function)
    thres_fake_gauss_net = (2, get_fake_gaussian_noise_snr_thresh)


def add_detection_function_to_list_from_names_and_kwargs(
    functions_list: list, function_name: str, **kwargs
) -> list:
    try:
        function_generator = StandardDetectionFunctions[function_name].value[1]
        function = function_generator(**kwargs)
        functions_list += [function]
    except KeyError:
        raise KeyError(
            "Could not find this function name in the standard detection functions.\n\
            Did you mean to pass it in manually instead?"
        )
