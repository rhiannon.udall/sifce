import copy
import logging
import os

import matplotlib.pyplot as plt
import numpy as np

from .sims import SimulationSet

logger = logging.getLogger(__name__)


def snrs_corner(dataframe, plot_params, output_path=None, snr_name="opt_snr_net"):
    """
    Parameters
    --------------
    dataframe: DataFrame
        Dataframe
    plot_params: list
        List with the plot parameters (need to be column names in the dataframe)
    output_path: str
        The output path to where the generated plot will be saved
    snr_name: str
        The name of the column from the dataframe that is going to be used as snr
    """
    local_df = copy.deepcopy(dataframe)
    grid_dim = len(plot_params)
    fig, axes = plt.subplots(
        grid_dim,
        grid_dim,
        # sharex=True,
        # sharey=True,
        figsize=(grid_dim * 3, grid_dim * 3),
        gridspec_kw=dict(
            wspace=0.15,
            hspace=0.15,
        ),
    )
    for i in range(grid_dim):
        for j in range(grid_dim):
            if j > 0:
                axes[i, j].tick_params(left=False, labelleft=False)
            else:
                axes[i, j].set_ylabel(plot_params[i])
            if i < grid_dim - 1:
                axes[i, j].tick_params(bottom=False, labelbottom=False)
            else:
                axes[i, j].set_xlabel(plot_params[j])
            if i < j:
                axes[i, j].axis("off")
            elif i == j:
                param = plot_params[i]
                samples_per_bin, hist_bins = np.histogram(
                    local_df[param].to_numpy(), bins=101
                )
                snr_weighting = copy.deepcopy(local_df[snr_name]).to_numpy()

                # digitize is stupid
                bin_indices = np.digitize(
                    local_df[param].to_numpy(), hist_bins, right=True
                )
                bin_indices[np.nonzero(bin_indices == 0)] = 1
                bin_indices -= 1
                snr_weighting /= samples_per_bin[bin_indices]

                axes[i, j].hist(local_df[param], weights=snr_weighting, bins=hist_bins)
                axes[i, j].tick_params(right=True, labelright=True)
                # axes[i,j].tick_params(bottom=True, labelbottom=True)
                axes[i, j].ticklabel_format(scilimits=(-3, 3))
                axes[i, j].set_ylabel("SNR")
                axes[i, j].yaxis.set_label_position("right")
            else:
                param_1 = plot_params[j]
                param_2 = plot_params[i]

                param_1_array = copy.deepcopy(local_df[param_1].to_numpy())
                param_2_array = copy.deepcopy(local_df[param_2].to_numpy())
                snrs = copy.deepcopy(local_df[snr_name]).to_numpy()

                axes[i, j].scatter(param_1_array, param_2_array, c=snrs)

    fig.suptitle("Sky Averaged SNR Parametric Dependence")
    if output_path is None:
        output_path = os.path.join(os.getcwd(), "snr_corner.jpg")
    fig.savefig(output_path)


def vt_histogram(list_of_lists_of_vt, fixed_axis_lim=True, output_path=None):
    """
    Parameters
    --------------
    list_of_lists_of_vt: list of lists
        List of lists of sensitive time-volume
    fixed_axis_lim: boolean
        If True, will fix the x limits to the min and max sensitive values over all the lists
    output_path: str
        The output path to where the generated plot will be saved
    """
    min_value = min(min(list_of_lists_of_vt, key=lambda x: min(x)))
    max_value = max(max(list_of_lists_of_vt, key=lambda x: max(x)))

    if len(list_of_lists_of_vt) > 1:
        fig, ax = plt.subplots(
            1, len(list_of_lists_of_vt), figsize=(6 * len(list_of_lists_of_vt), 5)
        )
        for i, list_of_vt in enumerate(list_of_lists_of_vt):
            ax[i].hist(
                list_of_vt,
                color="b",
                label=f"st. dev: {round(np.std(list_of_vt),5)}",
            )
            ax[i].legend(loc="upper right")
            ax[i].set_xlabel("Sensitive Time Volume (Gpc$^3$ yr)")
            ax[i].set_ylabel("Number of bins")
            if fixed_axis_lim:
                ax[i].set_xlim(min_value, max_value)
        if output_path is None:
            output_path = os.path.join(os.getcwd(), "vt_histogram.jpg")
        fig.savefig(output_path)

    else:
        plt.hist(
            list_of_lists_of_vt[0],
            color="b",
            label=f"st. dev.: {round(np.std(list_of_lists_of_vt[0]),5)}",
        )
        plt.legend()
        plt.xlabel("Sensitive Time Volume (Gpc$^3$ yr)")
        plt.ylabel("Number of bins")

        if output_path is None:
            output_path = os.path.join(os.getcwd(), "vt_histogram.jpg")
        plt.savefig(output_path)
