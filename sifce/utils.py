import logging
import os
from enum import Enum

import numpy as np
from pycbc.psd import read as psd_read

logger = logging.getLogger(__name__)


class Standards(Enum):
    bilby = 0
    pycbc = 1
    # 🤞 there will never be a reason to connect to lal or rift
    # lalsuite = 3
    # rift = 4


bilby_to_pycbc_map = dict(
    mass_1="mass1",
    mass_2="mass2",
    spin_1x="spin1x",
    spin_1y="spin1y",
    spin_1z="spin1z",
    spin_2x="spin2x",
    spin_2y="spin2y",
    spin_2z="spin2z",
    theta_jn="inclination",
    phase="coa_phase",
    luminosity_distance="distance",
    ra="right_ascension",
    dec="declination",
    psi="polarization",
    mass_ratio="mass_ratio",
    mass_1_source="mass_1_source",
    mass_2_source="mass_2_source",
    chirp_mass="chirp_mass",
    total_mass="total_mass",
    chirp_mass_source="chirp_mass_source",
    total_mass_source="total_mass_source",
    symmetric_mass_ratio="symmetric_mass_ratio",
    a_1="a_1",
    a_2="a_2",
    tilt_1="tilt_1",
    tilt_2="tilt_2",
    phi_jl="phi_jl",
    phi_12="phi_12",
    cos_tilt_1="cos_tilt_1",
    cos_tilt_2="cos_tilt_2",
    redshift="redshift",
    geocent_time="geocent_time",
)

pycbc_to_bilby_map = dict()
for key, val in bilby_to_pycbc_map.items():
    pycbc_to_bilby_map[val] = key


def remap_dict_keys(input_dict, map_dict):
    """
    A function to rename the keys of a dictionary according to a map.

    Parameters:
        input_dict ::dict
            The dictionary whose keys should be remapped
        map_dict ::dict
            The dictionary which defines the map

    Returns:
        mapped_dict ::dict
            The input dictionary with the keys remapped
    """
    mapped_dict = dict()
    for key, val in input_dict.items():
        mapped_dict[map_dict[key]] = val
    return mapped_dict


def read_psd_from_txt(
    psdpath,
    f_min=20,
    f_max=30,
    delta_f=1 / 8.0,
    asd=False,
):
    """
    Reads in a two column asd or psd, converts to psd if necessary,
    Sanitizes for pycbc, and interpolates to correct frequencies.

    Parameters
    ---------
    psdpath ::str
        The path to the psd
    f_min ::float
        The minimum frequency for the PSD
    f_max ::float
        The maximum frequency for the PSD
    asd ::bool
        If True, then input is treated as an ASD and squared accordingly
    verbose ::bool
        If True, do verbose logging

    Returns
    ---------
    psd ::pycbc.types.FrequencySeries
        The cleaned PSD
    """
    # Read data from file
    read_data = np.genfromtxt(psdpath)

    # Get magnitudes
    magnitude_data = read_data[:, 1]
    # Get corresponding frequencies
    frequencies = read_data[:, 0]

    # If it's an ASD square it
    if asd:
        magnitude_data = magnitude_data**2

    # Determine desired length
    length = int(f_max / delta_f)

    # plug into pycbc
    psd = psd_read.from_numpy_arrays(
        frequencies, magnitude_data, length, delta_f, f_min
    )

    # 0 is bad, so if it's zero fix it to 1 instead
    # (equivalent to just cutting out this frequency)
    psd.data[np.argwhere(psd.data == 0)] = 1

    return psd


class classproperty(object):
    def __init__(self, fget):
        self.fget = fget

    def __get__(self, owner_self, owner_cls):
        return self.fget(owner_cls)
