import ast
import logging
import os
from asyncore import write
from http.client import INSUFFICIENT_STORAGE
from multiprocessing.sharedctypes import Value
from threading import currentThread

import numpy as np
from bilby.gw.detector.calibration import read_calibration_file, write_calibration_file
from gwpy.segments import Segment, SegmentList, SegmentListDict
from pycbc.detector import Detector
from pycbc.types import FrequencySeries

from .utils import read_psd_from_txt

logger = logging.getLogger(__name__)


class Network(object):
    """
    An object which encompasses important information about the simulated network

    Attributes
    ----------
    segments ::SegmentListDict
        An object which encodes the live segments for each IFO


    """

    def __init__(
        self,
        ifo_uptime_file=None,
        ifos=None,
        psd_config_file=None,
        curves_are_asds=False,
        calibration_config_file=None,
        f_min=20,
        f_max=1024,
        delta_f=1 / 8.0,
        injection_approximant="IMRPhenomXP",
        recovery_approximant="IMRPhenomXP",
    ) -> None:
        """
        Parameters
        -----------------
        ifo_uptime_file ::str
            A path to a file which encodes the segments for each ifo
        ifos ::list
            A list of ifos to add directly
        psd_config_file ::str
            A path to a file which provides the psds corresponding to each segment
        curves_are_asds ::bool
            Pass True if the sensitivity curves in the config file should be interpreted as ASDs
        f_min ::float
            The minimum frequency for analysis with this network
        f_max ::float
            The maximum frequency for analysis with this network
        delta_f ::float
            The frequency step for analysis with this network
        injection_approximant ::str
            The approximant for generating injection waveforms, defaults to recovery in optimal SNR case
        recovery_approximant ::str
            The approximant for generating the recovery template
        """
        if ifo_uptime_file is None and ifos is not None:
            self.segments = SegmentListDict()
            for ifo in ifos:
                self.segments[ifo] = SegmentList([])
        elif ifo_uptime_file is not None and ifos is not None:
            self.segments = self.parse_ifo_uptime_file(ifo_uptime_file)
            if self.ifos != ifos:
                raise ValueError(
                    "Both an ifo list and segment file were provided, but ifos do not correspond"
                )
        elif ifo_uptime_file is not None and ifos is None:
            self.segments = self.parse_ifo_uptime_file(ifo_uptime_file)
        else:
            self.segments = SegmentListDict()
            logger.info(
                "No information was provided to construct the ifo network, this must now be done manually"
            )
        if psd_config_file is not None:
            self.psd_segments, self.psd_id_map = Network.read_psd_config_file(
                psd_config_file
            )
        else:
            self.psd_segments = {k: SegmentListDict() for k in self.ifos}
            self.psd_id_map = dict()
        self.delta_f = delta_f
        self.f_min = f_min
        self.f_max = f_max
        if calibration_config_file is not None:
            (
                self.calibration_segments,
                self.calibration_id_map,
            ) = self.parse_calibration_file(calibration_config_file)
        else:
            self.calibration_segments = None
            self.calibration_id_map = None
        self.injection_approximant = injection_approximant
        self.recovery_approximant = recovery_approximant
        self.curves_are_asds = curves_are_asds
        self.form_detector_object()

    ################################################################
    ################################################################
    ####                    Validity Checks                     ####
    ################################################################
    ################################################################

    @property
    def psds_one_to_one_with_segments(self):
        """
        A check whether there is a 1-1 map psd_id --> segment space
        """
        for ifo in self.ifos:
            if self.psd_segments[ifo].intersection(
                self.psd_segments[ifo].keys()
            ) != SegmentList([]):
                return False
        return True

    @property
    def psds_cover_segments(self):
        """
        A check whether there is a surjective map from segment space onto psd segment space
        Note this does *not* imply the map is strictly bijective,
        since we do not guarantee surjectivity the other direction
        """
        for ifo in self.ifos:
            full_psd_seglist = self.psd_segments[ifo].union(
                self.psd_segments[ifo].keys()
            )
            if full_psd_seglist + self.segments[ifo] != full_psd_seglist:
                return False
        return True

    ################################################################
    ################################################################
    ####                   IFO Segment Utilities                ####
    ################################################################
    ################################################################
    @staticmethod
    def parse_ifo_uptime_file(ifo_uptime_file):
        """
        Parameters
        ----------
        ifo_uptime_file ::str
            A path to an ifo_uptime_file, which should follow specific formatting systems

        Returns
        ---------
        segment_lists ::SegmentListDict
            IFOs with corresponding segment list
        """
        # Get the lines
        with open(ifo_uptime_file, "r") as f:
            lines = f.readlines()

        # Preparations
        segment_lists = SegmentListDict([])
        current_key = None

        # Loop
        for line in lines:
            # If IFO then switch to that IFO in the temporary segment list, and set it as our current working IFO
            if "IFO" in line:
                current_key = line.split(":")[1].strip()
                if current_key not in segment_lists.keys():
                    segment_lists[current_key] = SegmentList([])
            # If it's a blank line skip it
            elif line.strip() == "":
                continue
            # Finally, attempt to interpret as a list
            else:
                try:
                    list_form = ast.literal_eval(line)
                    logger.debug(list_form)
                    segment_lists[current_key] += SegmentList([Segment(list_form)])
                except SyntaxError:
                    logger.info(
                        f"Attempted to parse line {line} as a segment but was unsuccessful"
                    )
                    logger.exception("The exception was")

        # A final cleanup step
        for segment_list in segment_lists.values():
            segment_list.coalesce()

        return segment_lists

    @staticmethod
    def write_to_ifo_uptime_file(segment_lists, file_path):
        """
        A function to write correctly configured ifo_uptime files given a SegmentListDict

        Parameters
        ---------------
        segment_lists ::SegmentListDict
            The segments for each ifo to write
        file_path ::str
            The path to write the file to
        """
        write_lines = []
        for ifo, val in segment_lists.items():
            write_lines += [f"IFO:{ifo}\n"]
            for segment in val:
                write_lines += [f"{list(segment)}\n"]
            write_lines += ["\n"]
        logger.debug(write_lines)
        with open(file_path, "w") as f:
            f.writelines(write_lines)

    def add_ifo(self, ifo):
        """
        Adds an IFO to self.segments

        Parameters
        ------------
        ifo ::str
            The IFO to add
        """
        assert ifo not in self.ifos
        self.segments[ifo] = SegmentList([])

    def remove_ifo(self, ifo):
        """
        Removes and IFO from self.segments

        Parameters
        -------------
        ifo ::str
            The IFO to remove
        """
        if self.segments[ifo] != SegmentList([]):
            raise ValueError(
                f"Cannot remove ifo {ifo} because it is populated \
            If you are certain then clear the segment first"
            )
        else:
            self.segments.pop(ifo)

    def add_segment(self, segment, ifo):
        """
        Adds a segment to self.segments for a given ifo

        Parameters:
        ---------------
        segment ::list, tuple, or Segment
            An object which can be interpreted as a segment, to add
        ifo ::str
            The IFO to add the segment to
        """
        if ifo not in self.ifos:
            self.add_ifo(ifo)
        self.segments[ifo] += SegmentList([Segment(segment)])

    def remove_segment(self, segment, ifo):
        """
        Removes a segment to self.segments for a given ifo

        Parameters:
        ---------------
        segment ::list, tuple, or Segment
            An object which can be interpreted as a segment, to remove
        ifo ::str
            The IFO to remove the segment from
        """
        assert ifo in self.ifos
        self.segments[ifo] -= SegmentList([Segment(segment)])
        # TODO make this return the segments that have been removed

    def live_ifos_for_time(self, time):
        """
        Get the IFOs which are on at a given time, according to the network

        Parameters
        ----------
        time ::float
            The time one is inquiring about
        """
        return self.segments.keys_at(time)

    @property
    def ifos(self):
        """
        The IFOs which form the network
        """
        return list(self.segments.keys())

    ################################################################
    ################################################################
    ####                       PSD Utilities                    ####
    ################################################################
    ################################################################

    @staticmethod
    def read_psd_config_file(psd_config_file):
        """
        Parameters
        ----------
        psd_config_file ::str
            A path to a psd_config_file, which should follow specific formatting systems

        Returns
        ---------
        network_psds ::SegmentListDict
            A SegmentListDict with keys ifo:/path/to/psd and values of SegmentList
        """
        # Get the lines
        with open(psd_config_file, "r") as f:
            lines = f.readlines()

        # ifo_psd_segments will be a dict ifo:SegmentListDict(psd_id:SegmentList)
        ifo_psd_segments = dict()
        # id_to_data maps a psd id to the data path
        # This can be used to construct more elaborate network objects based on reading in the data
        # This is out of scope for this function though
        psd_id_to_data = dict()

        # Loop
        for line in lines:
            # If IFO then switch to that IFO
            if "IFO" in line:
                current_ifo = line.split(":")[1].strip()
                if current_ifo not in ifo_psd_segments.keys():
                    ifo_psd_segments[current_ifo] = SegmentListDict()
            # If it's a blank line skip it
            elif line.strip() == "":
                continue
            # Finally, attempt to interpret as a list:file_path
            else:
                try:
                    # Get the segment and corresponding path
                    segment_str, psd_path = line.strip().split(":")
                    # Get the Segment Object
                    segment = Segment(ast.literal_eval(segment_str))
                    psd_id = None
                    for key, val in psd_id_to_data.items():
                        if val == psd_path:
                            psd_id = key
                            break
                    if psd_id is None:
                        psd_id = len(psd_id_to_data.keys())
                        psd_id_to_data[psd_id] = psd_path
                    if psd_id in ifo_psd_segments[current_ifo].keys():
                        # Add it to the SegmentListDict
                        ifo_psd_segments[current_ifo][psd_id] += SegmentList([segment])
                    else:
                        ifo_psd_segments[current_ifo][psd_id] = SegmentList([segment])
                except SyntaxError:
                    logger.info(
                        f"Attempted to parse line {line} as a segment but was unsuccessful"
                    )
                    logger.exception("The exception was")

        return ifo_psd_segments, psd_id_to_data

    @staticmethod
    def write_psd_config_file(psd_segments, psd_data_map, psd_config_file):
        """
        A function to write correctly configured ifo_uptime files given a dict of psds for ifo segments

        Parameters
        ---------------
        psd_segments ::dict
            Contains SegmentListDicts for each ifo, each of which maps psd_ids to segments
        file_path ::str
            The path to write the file to
        """
        write_lines = []
        for ifo, seglistdict in psd_segments.items():
            write_lines += f"IFO:{ifo}\n"
            for psd_id, seglist in seglistdict.items():
                for segment in seglist:
                    write_lines += f"{list(segment)}:{psd_data_map[psd_id]}\n"
            write_lines += "\n"
        with open(psd_config_file, "w") as f:
            f.writelines(write_lines)

    def add_psd_to_id_map(self, psd_path):
        """
        Adds a psd_id:psd_path to self.psd_id_map

        Parameters
        ---------------
        psd_path ::str
            The path to the psd file to add
        """
        if psd_path in self.psd_id_map.values():
            raise ValueError(
                f"Could not add {psd_path} as separate id, it already has an id"
            )
        psd_id = len(self.psd_id_map)
        self.psd_id_map[psd_id] = psd_path

    def remove_psd_from_id_map(self, psd_path):
        """
        Removes a psd_id:psd_path from self.psd_id_map

        Parameters
        ---------------
        psd_path ::str
            The path to the psd file to remove
        """
        if psd_path not in self.psd_id_map.values():
            raise ValueError(
                f"Could not remove {psd_path} from id map, since it does not have an id"
            )
        for psd_id, psd in self.psd_id_map.items():
            if psd_path == psd:
                self.psd_id_map.pop(psd_id)
                break

    def add_psd_segment(self, psd_id, ifo, segment):
        """
        Adds a segment to the psd_key in self.network_psds

        Parameters
        ----------------
        psd_id ::int
            The psd_id to which the segment corresponds
        ifo ::str
            The ifo to add to
        segment ::tuple, list, or Segment
            An object which can be interpreted as the segment to add
        """
        assert psd_id in self.psd_id_map
        if (ifo not in self.psd_segments.keys()) and (ifo in self.ifos):
            self.psd_segments[ifo] = SegmentListDict()
        elif ifo not in self.ifos:
            raise ValueError(
                f"Could not add ifo {ifo} to psd_segments, \
                since it is not an IFO in this network according to livetime segments"
            )
        if psd_id not in self.psd_segments[ifo].keys():
            self.psd_segments[ifo][psd_id] = SegmentList([])
        self.psd_segments[ifo][psd_id] += SegmentList([Segment(segment)])

    def remove_psd_segment(self, psd_id, ifo, segment):
        """
        Removes a segment from the psd_key in self.network_psds

        Parameters
        ----------------
        psd ::str
            The psd_path to remove from, joins with ifo for psd_key
        ifo ::str
            The ifo to removed from, joins with psd for psd_key
        segment ::tuple, list, or Segment
            An object which can be interpreted as the segment to remove
        """
        if psd_id not in self.psd_segments[ifo].keys():
            raise ValueError(
                f"Could not subtract segment for psd_id {psd_id} in ifo {ifo} since it does not exist"
            )
        self.psd_segments[ifo][psd_id] -= SegmentList([Segment(segment)])
        if self.psd_segments[ifo][psd_id] == SegmentList([]):
            self.psd_segments[ifo].pop(psd_id)
        # TODO make this return the segments that have been removed

    def load_psds(self):
        """
        Reads in the psds set in self.psd_id_map as data, according to the config of the network
        """
        self.psd_data_map = dict()
        for psd_id, psd_path in self.psd_id_map.items():
            self.psd_data_map[psd_id] = read_psd_from_txt(
                psd_path,
                f_min=self.f_min,
                f_max=self.f_max,
                delta_f=self.delta_f,
                asd=self.curves_are_asds,
            )

    def psds_for_time(self, time):
        """
        Gets the PSDs for a given time
        """
        live_ifos = self.live_ifos_for_time(time)
        psd_dict = dict()
        for ifo in live_ifos:
            psd_id = self.psd_segments[ifo].keys_at(time)
            assert len(psd_id) == 1
            psd_id = psd_id[0]
            psd_dict[ifo] = self.psd_data_map[psd_id]
        return psd_dict

    ################################################################
    ################################################################
    ####                 Calibration Utilities                  ####
    ################################################################
    ################################################################

    def parse_calibration_file(self, calibration_config_file):
        # Get the lines
        with open(calibration_config_file, "r") as f:
            lines = f.readlines()

        # ifo_calibration_segments will be a dict ifo:SegmentListDict(psd_id:SegmentList)
        ifo_calibration_segments = dict()
        # id_to_data maps ifo and curve index to a calibration curve
        calibration_id_to_data = dict()

        for line in lines:
            # If it's an IFO line, use it to get the current IFO to add to
            if "IFO" in line:
                current_ifo = line.split(":")[1].strip()
                if current_ifo not in ifo_calibration_segments.keys():
                    ifo_calibration_segments[current_ifo] = SegmentListDict()
            # If it's a data file line, use it to set the file for the IFO
            elif "Curves_hdf5:" in line:
                if f"{current_ifo}_file" in calibration_id_to_data:
                    raise ValueError("Pass only one calibration curves file per IFO")
                else:
                    calibration_id_to_data[f"{current_ifo}_file"] = line.split(":")[-1]
            # If it's a blank line skip it
            elif line.strip() == "":
                continue
            else:
                try:
                    # Get the segment and corresponding index
                    segment_str, calibration_index = line.strip().split(":")
                    # Get the Segment Object
                    segment = Segment(ast.literal_eval(segment_str))
                    # Set the segment as the value for the index or add it to what's already present
                    if (
                        calibration_index
                        in ifo_calibration_segments[current_ifo].keys()
                    ):
                        ifo_calibration_segments[current_ifo][
                            calibration_index
                        ] += SegmentList([segment])
                    else:
                        ifo_calibration_segments[current_ifo][
                            calibration_index
                        ] = SegmentList([segment])
                except SyntaxError:
                    logger.info(
                        f"Attempted to parse line {line} as a segment but was unsuccessful"
                    )
                    logger.exception("The exception was")
        # Get the calibration file elements
        calibration_files = {
            file_id: calibration_id_to_data[file_id]
            for file_id in calibration_id_to_data
            if "file" in file_id
        }
        # Setup the frequencies to interpolate to given network information
        # This needs to start at 0 to accommodate pycbc's poor choices
        self.network_frequency_array = np.arange(
            0, self.f_max + self.delta_f, self.delta_f
        )
        # For each calibration h5
        for file_id, file_path in calibration_files.items():
            # Get the IFO and the number of requested indices
            # based on the number of SegLists in the ifo_calibration_segments
            ifo = file_id.split("_")[0]
            num_indices = len(ifo_calibration_segments[ifo])
            # Draw the curves
            file_path = file_path.strip()
            drawn_curves = read_calibration_file(
                file_path, self.network_frequency_array, num_indices
            )
            # Assign in formation ifo_idx
            for ii in range(num_indices):
                calibration_id_to_data[f"{ifo}_{ii}"] = drawn_curves[ii, :]

        return ifo_calibration_segments, calibration_id_to_data

    @staticmethod
    def apply_calibration_to_frequency_series(
        frequency_series: FrequencySeries, calibration_curve: np.ndarray
    ):
        assert len(frequency_series) == len(calibration_curve)
        return frequency_series * calibration_curve

    def get_calibrated_curve_from_time_and_ifo(self, time, ifo):
        calibration_index = self.calibration_segments[ifo].keys_at(time)
        assert len(calibration_index) == 1
        calibration_index = calibration_index[0]
        return self.calibration_id_map[f"{ifo}_{calibration_index}"]

    def calibrate_frequency_series_for_time_and_ifo(self, time, ifo, frequency_series):
        calibration_curve = self.get_calibrated_curve_from_time_and_ifo(time, ifo)
        return self.apply_calibration_to_frequency_series(
            frequency_series, calibration_curve
        )

    ################################################################
    ################################################################
    ####                 Miscellaneous Utilities                ####
    ################################################################
    ################################################################

    def form_detector_object(self):
        """
        Forms self.detectors; we don't want this as a property
        because it's expensive and we don't want to be recomputing it
        """
        self.detectors = {k: Detector(k) for k in self.ifos}
