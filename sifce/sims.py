import copy
import logging
import os
import timeit
from ast import Raise, literal_eval
from inspect import trace
from multiprocessing.sharedctypes import Value
from re import T
from typing import Callable, Tuple, Union
from unicodedata import category

import astropy.cosmology
import astropy.cosmology.units as cu
import astropy.units as u
import bilby.gw.prior
import numpy as np
import pandas as pd
from astropy.cosmology import z_at_value
from bilby.core.prior import PriorDict
from bilby.gw import conversion, cosmology
from matplotlib.pyplot import scatter
from tqdm import tqdm

from .datatools import (
    compute_hphc_fd,
    compute_snr_fd,
    project_and_combine,
    sky_scatter_snr,
)
from .detection import add_detection_function_to_list_from_names_and_kwargs
from .network import Network
from .prior import LuminosityDistanceCosmological, RedshiftedPrior
from .utils import classproperty

logger = logging.getLogger(__name__)


class SimulationSet(object):
    """
    A class for methods relating large numbers of configurations, which should share internally consistent attributes.

    Attributes
    ------------------
    sampling_distribution ::PriorDict
        A PriorDict object, for drawing samples
    target_distribution ::PriorDict
        A PriorDict object, which reflects the population whose distribution you want to reweight to
    cosmology_model ::astropy.cosmology.Cosmology




    """

    def __init__(
        self,
        sampling_distribution=None,
        target_distribution=None,
        cosmology_model=None,
        network: Network = None,
        sky_scatter=True,
        sky_average=True,
        distance_scale=True,
        d_ref_mpc=1,
        reduce_dmax_to_observable=True,
        detection_function_names_list=[],
        detection_function_kwargs_list=[],
        detection_functions_list=[],
    ) -> None:
        """
        Constructor for the class

        Parameters
        --------------
        sampling_distribution ::Union[dict, str, None]
            A valid input to bilby.core.prior.PriorDict
            https://lscsoft.docs.ligo.org/bilby/_modules/bilby/core/prior/dict.html#PriorDict
            This is the distribution samples will be drawn from
        target_distribution ::Union[dict, str, None]
            A valid input to bilby.core.prior.PriorDict
            https://lscsoft.docs.ligo.org/bilby/_modules/bilby/core/prior/dict.html#PriorDict
            This is the target distribution if importance sampling is being used
        cosmology_model ::Union[str, astropy.cosmology.Cosmology]
            A string indicating which cosmology (from the astropy list / name standards) to use
        network ::sifce.network.Network
            A network object, encoding the network properties for the simulated population
        sky_scatter ::bool
            If true, use sky scattering methods
        distance_scale ::bool
            If true, use distance scaling methods
        sky_average ::bool
            If true and sky_scatter is true, then average over sky to a single value
        d_ref_mpc ::int
            The reference distance to use if distance scaling
        reduce_dmax_to_observable ::bool
            If true, remap the distribution maximum luminosity distance to
            approximately the minimum value for which p_det=0 for all configurations
        detection_function_names_list ::list
            A list of named detection functions to use for computations
        detection_funcion_kwargs_list ::list
            The kwargs to use for the named detection functions - either already in dict form, or strings
        """
        self.network = network
        self.cosmology_model = cosmology_model
        self.sampling_distribution = sampling_distribution
        self.target_distribution = target_distribution
        # This looks weircd because of the setter
        # In fact it is just grabbing the min and max of the distance prior
        self.uniform_source_frame_distribution = (
            self.get_distance_prior_from_prior_dict(
                self.sampling_distribution, cosmology=cosmology_model
            )
        )
        self.sky_scatter = sky_scatter
        self.distance_scale = distance_scale
        self.d_ref_mpc = d_ref_mpc
        self.sky_average = sky_average
        self.reduce_dmax_to_observable = reduce_dmax_to_observable
        if isinstance(detection_function_kwargs_list[0], str):
            detection_function_kwargs_list = [
                literal_eval(x) for x in detection_function_kwargs_list
            ]
        # This also looks weird because of the setter
        # These are basically args
        self.detection_functions = (
            detection_functions_list,
            detection_function_names_list,
            detection_function_kwargs_list,
        )
        self.detection_function_names = detection_function_names_list
        self.detection_function_kwargs = detection_function_kwargs_list
        self.make_clean_simulations_dataframe()

    @property
    def cosmology_model(self) -> astropy.cosmology.Cosmology:
        """
        The cosmology_model from astropy to use
        """
        return self._cosmology_model

    @cosmology_model.setter
    def cosmology_model(
        self,
        cosmology_model: str,
    ) -> None:
        self._cosmology_model = cosmology.get_cosmology(cosmology_model)

    ################################################################
    ################################################################
    ####                    SNR Computation                     ####
    ################################################################
    ################################################################

    def fill_out_snr(
        self,
        n_sky=500,
        n_scale=100,
        benchmark=False,
    ) -> None:
        """
        Method to fill out the dataframe snrs

        Parameters
        ----------------------
        n_sky ::int
            number of sky parameter to be sampled
        N_scale ::int
            number of distances to be sampled
        benchmark ::bool
            If True logging.info will include information about the speed of component computations

        Returns
        -------------
        df: updated dataframe
        """
        # Trickery to improve memory usage of the dataframe down the line
        # This is necessary to do large concatenations without memory overflows
        dummy_df = self.make_dummy_df()

        # Scatter over many sky positions
        if self.sky_scatter:
            # Prepare list to concat at the end - much more efficient
            scatter_dfs = []
            logger.info("Beginning sky scattering")
            # Iterate over rows (equivalently intrinsic indices)
            for _, row in tqdm(self.simulations_dataframe.iterrows()):
                # Act only on rows which don't already have SNRs
                if np.isnan(row["opt_snr_net"]):
                    # If distance scaling, set each row to the reference distance first
                    if self.distance_scale:
                        row = self.set_row_to_d_ref(row, self.d_ref_mpc)
                    # Perform sky scattering and add to list
                    scatter_dfs += [
                        self.calc_snrs_sky(
                            row,
                            n_sky=n_sky,
                            dummy_df=dummy_df,
                            benchmark=benchmark,
                        )
                    ]
            # Concat and cleanup
            self.simulations_dataframe = pd.concat(scatter_dfs, ignore_index=True)
            del scatter_dfs
        # If not sky scattering, evaluate SNRs at each point respectively
        else:
            # Prepare list
            singlet_dfs = []
            for _, row in tqdm(self.simulations_dataframe.iterrows()):
                # Compute if necessary
                if np.isnan(row["opt_snr_net"]):
                    # Set to D_ref if appropriate
                    if self.distance_scale:
                        row = self.set_row_to_d_ref(row, self.d_ref_mpc)
                    # Compute SNR
                    singlet_dfs += [
                        self.calc_snr_single(
                            row,
                        )
                    ]
            # Concatenate and cleanup
            self.simulations_dataframe = pd.concat(singlet_dfs, ignore_index=True)
            del singlet_dfs

        # Convert columns to categorical where possible to save memory
        self.simulations_dataframe = self.convert_appropriate_columns_to_categorical(
            self.simulations_dataframe
        )

        # Deprecated block - in future could be used to maximize efficiency by choosing an appropriate distance
        # Maybe better to do this with trace jobs though, or by single point tests
        # if self.distance_scale and self.reduce_dmax_to_observable:
        #     self.set_observable_dmax()

        # Put the sampling distribution into a Cosmological object, and get the corresponding redshift prior
        self.sampling_distance_distribution = self.get_distance_prior_from_prior_dict(
            self.sampling_distribution, self.cosmology_model
        )
        self.sampling_redshift_distribution = (
            self.sampling_distance_distribution.get_corresponding_prior("redshift")
        )
        self.target_distance_distribution = self.get_distance_prior_from_prior_dict(
            self.target_distribution, self.cosmology_model
        )

        # Begin distance scaling procedure
        if self.distance_scale:

            # Select out the source mass prior appropriately
            self.source_mass_prior = [
                prior
                for key, prior in self.sampling_distribution.items()
                if "source" in key
            ][0]
            # Problems arise if this is identically 0
            if self.source_mass_prior.minimum == 0:
                self.source_mass_prior.minimum = 0.0001
            # Get the detector mass prior
            self.detector_mass_prior = RedshiftedPrior(
                self.source_mass_prior,
                self.sampling_distance_distribution,
            )

            # Currently - always sky average!
            if not self.sky_average:
                raise ValueError(
                    "Cannot distance scale without sky averaging\
                    - you *will* run into memory errors"
                )
            # Prepare a list - note we will be making a new dataframe
            scale_detection_dfs = []
            logger.info("Beginning distance scaling")
            # Iterate over unique intrinsic indices
            for intrinsic_index in tqdm(
                self.simulations_dataframe["intrinsic_index"].unique()
            ):
                # Generate individual detection dataframes for each intrinsic index
                scale_detection_dfs += [
                    self.scale_distances(
                        self.simulations_dataframe,
                        intrinsic_index,
                        n_dist=n_scale,
                        benchmark=benchmark,
                    )
                ]
            # Concatenate
            self.detection_dataframe = pd.concat(scale_detection_dfs, ignore_index=True)
        else:
            self.simulations_dataframe = self.get_mf_snr_from_wips(
                self.simulations_dataframe
            )

            if self.sky_average:
                intrinsic_sub_df_groups = self.simulations_dataframe.groupby(
                    "intrinsic_index"
                )
                sub_dfs_list = []
                for intrinsic_index, intrinsic_sub_df in intrinsic_sub_df_groups:
                    # This should always hold but just a check
                    assert (
                        intrinsic_sub_df["luminosity_distance"].max()
                        == intrinsic_sub_df["luminosity_distance"].min()
                    )
                    p_dets_dict = dict(
                        intrinsic_index=intrinsic_index,
                        luminosity_distance=intrinsic_sub_df[
                            "luminosity_distance"
                        ].max(),
                    )
                    for ii, function in enumerate(self.detection_functions):
                        p_dets_dict[f"p_det_{ii}"] = self.compute_pdet(
                            intrinsic_sub_df,
                            function,
                        ).mean()
                    sub_df = pd.DataFrame(p_dets_dict, index=[0])
                    sub_dfs_list += [sub_df]

                # Concat the result
                self.detection_dataframe = pd.concat(sub_dfs_list, ignore_index=True)

            else:
                self.detection_dataframe = pd.DataFrame()
                self.detection_dataframe[
                    "intrinsic_index"
                ] = self.simulations_dataframe["intrinsic_index"]
                self.detection_dataframe[
                    "luminosity_distance"
                ] = self.simulations_dataframe["luminosity_distance"]
                for ii, function in enumerate(self.detection_functions):
                    self.detection_dataframe[f"p_det_{ii}"] = self.compute_pdet(
                        self.simulations_dataframe, function
                    )
            # The simulations dataframe should only have redshifts if not distance scaling
            # Otherwise it will stay at the reference distance, while the detection dataframe
            # Reflects the changes
            self.simulations_dataframe = self.convert_distances(
                self.simulations_dataframe, self.cosmology_model
            )

            # Get probability weight from sampling to usf
            self.detection_dataframe[
                "ln_prob_usf_vs_sampling_distance"
            ] = self.uniform_source_frame_distribution.ln_prob(
                self.detection_dataframe["redshift"]
            ) - self.sampling_redshift_distribution.ln_prob(
                self.detection_dataframe["redshift"]
            )

            # Get probability weight from usf to target, if necessary, else it's just 0
            if (
                self.target_distance_distribution
                == self.uniform_source_frame_distribution
            ):
                self.detection_dataframe["ln_prob_target_vs_usf_distance"] = 0
            else:
                self.detection_dataframe[
                    "ln_prob_target_vs_usf_distance"
                ] = self.target_distance_distribution.get_corresponding_prior(
                    "redshift"
                ).ln_prob(
                    self.detection_dataframe["redshift"]
                ) - self.uniform_source_frame_distribution.ln_prob(
                    self.detection_dataframe["redshift_distance"]
                )

        # Fill out redshifts for the detection dataframe
        # This *shouldn't* be necessary, but it's safe and near instant so eh
        self.detection_dataframe = self.convert_distances(
            self.detection_dataframe, self.cosmology_model
        )

        # Assign probabilities for all non distance parameters
        logger.info("Assigning sampling distribution ln_prob")
        self.assign_sampling_distribution_log_probability()
        logger.info("Assigning target distribution ln prob")
        self.assign_target_distribution_log_probability()

        return self.simulations_dataframe

    def calc_snrs_sky(
        self, row, n_sky=500, dummy_df=None, benchmark=False
    ) -> pd.DataFrame:
        """
        A function that calculates the snr for the given row for N different sky configurations,
        and adds the row with the correspondent sky parameters to the dataframe

        adds it to the end of
        Parameters
        ----------------------
        row ::pandas dataframe row
            a row of a dataframe that will have its parameters passed into sky scatter
        n ::integer
            number of sky configurations
        dummy_df ::pd.DataFrame
            A dataframe which has the columns pre-constructed with the correct datatypes,
            to reduce time and memory costs
        benchmark ::bool
            If true, logging.info will contain time information for individual pieces of the function

        Returns
        -------------
        temp_df ::pd.DataFrame
            The sky positioned SNR as a dataframe, either a multirow or single row DF
        """
        if benchmark:
            start = timeit.default_timer()

        row_tmp = copy.copy(row)
        # generate N sky configurations:
        sky_args = {
            k: self.sampling_distribution[k].sample(n_sky)
            for k in self.detector_position_labels
        }

        if benchmark:
            create_scatter = timeit.default_timer()
            logging.info(
                f"calc_snrs_sky: Time required to create the scatter was {create_scatter - start}"
            )

        end_time = row_tmp["geocent_time"]
        psd_dict = self.network.psds_for_time(end_time)

        if benchmark:
            load_psds = timeit.default_timer()
            logging.info(
                f"calc_snrs_sky: Time required to load psds {load_psds - create_scatter}"
            )

        # Check whether any detectors are on
        if len(psd_dict.keys()) != 0:
            # If at least one is, proceed as normal
            # get hp and hc:
            param_args = {
                k: row_tmp[k]
                for k in (self.waveform_generation_labels + ["luminosity_distance"])
            }

            # TODO add if statements to accommodate the cases for noise or h_prime
            strain = dict()
            strain["h_plus"], strain["h_cross"] = compute_hphc_fd(
                param_args,
                self.network.recovery_approximant,
                delta_f=self.network.delta_f,
                f_max=self.network.f_max,
                f_min=self.network.f_min,
            )
            # hprimes for injection waveform != recovery waveform
            if self.network.injection_approximant != self.network.recovery_approximant:
                hprime_plus, hprime_cross = compute_hphc_fd(
                    param_args,
                    self.network.injection_approximant,
                    delta_f=self.network.delta_f,
                    f_max=self.network.f_max,
                    f_min=self.network.f_min,
                )
                for ifo in self.network.ifos:
                    strain[f"hprime_plus_{ifo}"] = hprime_plus
                    strain[f"hprime_cross_{ifo}"] = hprime_cross
            if self.network.calibration_segments is not None:
                for ifo in self.network.ifos:
                    if f"hprime_plus_{ifo}" not in strain.keys():
                        strain[f"hprime_plus_{ifo}"] = strain["h_plus"]
                        strain[f"hprime_cross_{ifo}"] = strain["h_cross"]
                    strain[
                        f"hprime_plus_{ifo}"
                    ] = self.network.calibrate_frequency_series_for_time_and_ifo(
                        end_time, ifo, strain[f"hprime_plus_{ifo}"]
                    )
                    strain[
                        f"hprime_cross_{ifo}"
                    ] = self.network.calibrate_frequency_series_for_time_and_ifo(
                        end_time, ifo, strain[f"hprime_cross_{ifo}"]
                    )

            # use sky scatter to get snr:
            opt_snr_dict, h_hprime_dict, h_noise_dict = sky_scatter_snr(
                sky_args,
                strain,
                end_time,
                psd_dict,
                detector_obj_dict={
                    k: self.network.detectors[k] for k in psd_dict.keys()
                },
                f_min=self.network.f_min,
                f_max=self.network.f_max,
            )
        else:
            # If there are no psds, make an array of 0s for the net SNR
            opt_snr_dict = dict()
            h_hprime_dict = dict()
            h_noise_dict = dict()
            opt_snr_dict["net"] = np.zeros(sky_args["ra"].shape)

        if benchmark:
            compute_snrs = timeit.default_timer()
            logging.info(
                f"calc_snrs_sky: Time required to compute the SNRs was {compute_snrs - load_psds}"
            )

        if dummy_df is not None:
            df = copy.deepcopy(dummy_df)
            df.loc[0, :] = row_tmp[df.columns.values]
        else:
            # Make the temporary dataframe
            df = pd.DataFrame(row_tmp).transpose()

        # Create a df with N copies of row
        df = df.loc[np.repeat(df.index.values, n_sky)]

        # Append sky configurations to the new dataframe
        for name in ["ra", "dec", "psi"]:
            df[name] = sky_args[name]
        for name in self.network.ifos + ["net"]:
            # Fill in 0s for detectors that are not on
            if name not in opt_snr_dict.keys():
                opt_snr_dict[name] = np.zeros(opt_snr_dict["net"].shape)
                h_hprime_dict[name] = np.zeros(opt_snr_dict["net"].shape)
                h_noise_dict[name] = np.zeros(opt_snr_dict["net"].shape)
            df[f"opt_snr_{name}"] = opt_snr_dict[name]
            if name != "net":
                df[f"h_hprime_wip_{name}"] = h_hprime_dict[name]
                df[f"h_noise_wip_{name}"] = h_noise_dict[name]

        if benchmark:
            form_temp_df = timeit.default_timer()
            logging.info(
                f"calc_snrs_sky: Time required to form the temp dataframe was {form_temp_df - compute_snrs}"
            )
            logging.info(
                f"calc_snrs_sky: Total time required to sky-scatter 1 row was {form_temp_df - start}"
            )

        return df

    def scale_distances(
        self, unscaled_df, intrinsic_index, n_dist=100, benchmark=False
    ):
        """
        Scale unscaled_df for a given

        """
        # Begin benchmark tracking
        if benchmark:
            start = timeit.default_timer()

        # Get the subset of the dataframe which shares the same intrinsic index
        # We have to divide it up like this to keep the sampling by distance properly defined
        scaling_df = copy.deepcopy(
            unscaled_df[unscaled_df["intrinsic_index"] == intrinsic_index]
        )

        # Continue timing
        if benchmark:
            get_scaling_df = timeit.default_timer()
            logger.info(f"Time to generate scaling df: {get_scaling_df-start}")

        # Get n_sky based on the number of rows for this given intrinsic index
        n_sky = scaling_df.shape[0]

        # Get whatever the given source frame mass prior is
        # If there are 2 then the important bit (the source to detector correlation) is redundant
        # So taking the first if fine

        # All the mass elements should be the same since the share an intrinsic index
        # we want the type of mass e.g. mass_1 which corresponds to the source, but in detector frame
        detector_mass_name = self.source_mass_prior.name.replace("_source", "")
        detector_mass = scaling_df[detector_mass_name].array[0]
        # minimum redshift is either the redshift for the largest source mass to achieve this detector mass
        # or it's the minimum of the input distribution
        min_redshift = np.maximum(
            detector_mass / self.source_mass_prior.maximum - 1,
            self.sampling_redshift_distribution.minimum,
        )
        # maximum redshift is either the redshift for the smallest source mass to achieve this detector mass
        # or it's the maximum of the input distribution
        max_redshift = np.minimum(
            detector_mass / self.source_mass_prior.minimum - 1,
            self.sampling_redshift_distribution.maximum,
        )
        # Get the unit interval bounds for min and max redshift
        min_cdf = self.sampling_redshift_distribution.cdf(min_redshift)
        max_cdf = self.sampling_redshift_distribution.cdf(max_redshift)

        # Rescale enough random samples on this boundary
        redshifts = self.sampling_redshift_distribution.rescale(
            np.random.uniform(low=min_cdf, high=max_cdf, size=n_dist)
        )

        # First get the sampling probability
        # Since we are only changing the sampling by restricting the bounds
        # It suffices to divide by the contained CDF bulk
        ln_sampling_distance_probability = self.sampling_redshift_distribution.ln_prob(
            redshifts
        ) - np.log(max_cdf - min_cdf)

        # Convert to D_L for the actual scaling
        dls = self.cosmology_model.luminosity_distance(redshifts).value

        # Tile it, so that we have n copies for redshift and dls
        dls = np.expand_dims(dls, axis=1)
        redshifts = np.expand_dims(redshifts, axis=1)
        dls_zs_array = np.concatenate((dls, redshifts), axis=1)
        dls_zs_tiled = np.tile(dls_zs_array, (n_sky, 1))

        # Get the respective arrays
        dls_tiled = dls_zs_tiled[:, 0]
        zs_tiled = dls_zs_tiled[:, 1]

        # Benchmark
        if benchmark:
            get_lum_dist_distribution = timeit.default_timer()
            logger.info(
                f"Time to generate luminosity dist: {get_lum_dist_distribution-get_scaling_df}"
            )

        # Set reference SNR, so we can change to actual SNR
        scaling_df["reference_luminosity_distance"] = copy.deepcopy(
            scaling_df["luminosity_distance"]
        )

        # make N copies, so that the array now has N times as many rows
        scaling_df = scaling_df.loc[scaling_df.index.repeat(n_dist)]
        scaling_df["luminosity_distance"] = copy.deepcopy(dls_tiled)
        scaling_df["redshift"] = copy.deepcopy(zs_tiled)

        # Benchmark
        if benchmark:
            get_repeats_scaling = timeit.default_timer()
            logger.info(
                f"Time to generate repeats in df: {get_repeats_scaling - get_lum_dist_distribution}"
            )

        # Perform opt_snr_net scaling
        scaling_df["opt_snr_net"] = (
            scaling_df["opt_snr_net"]
            * scaling_df["reference_luminosity_distance"]
            / scaling_df["luminosity_distance"]
        )

        # Scale all the ifo specific stuff - note MF SNR scaling is done implicitly by scaling WIPs
        for ifo_name in self.network.ifos:
            # Opt SNR scales linearly
            scaling_df[f"opt_snr_{ifo_name}"] = (
                scaling_df[f"opt_snr_{ifo_name}"]
                * scaling_df["reference_luminosity_distance"]
                / scaling_df["luminosity_distance"]
            )
            # h | noise WIP also scares linearly
            # Note this means it's final contribution will scale as sqrt
            scaling_df[f"h_noise_wip_{ifo_name}"] = (
                scaling_df[f"h_noise_wip_{ifo_name}"]
                * scaling_df["reference_luminosity_distance"]
                / scaling_df["luminosity_distance"]
            )
            # h | hprime WIP scales quadratically
            # So it's final contribution will scale linearly
            scaling_df[f"h_hprime_wip_{ifo_name}"] = (
                scaling_df[f"h_hprime_wip_{ifo_name}"]
                * (
                    scaling_df["reference_luminosity_distance"]
                    / scaling_df["luminosity_distance"]
                )
                ** 2
            )

        # Benchmark
        if benchmark:
            perform_scales = timeit.default_timer()
            logger.info(
                f"Time to perform scales: {perform_scales - get_repeats_scaling}"
            )

        # Get the MF SNRs
        scaling_df = self.get_mf_snr_from_wips(scaling_df)

        # Benchmark
        if benchmark:
            compute_mf_snrs = timeit.default_timer()
            logger.info(f"Time to get mf snrs: {compute_mf_snrs - perform_scales}")

        # Now perform pdet computations
        for ii, function in enumerate(self.detection_functions):
            scaling_df[f"p_det_{ii}"] = self.compute_pdet(
                scaling_df, detection_function=function
            )

        # Benchmark
        if benchmark:
            compute_pdets = timeit.default_timer()
            logger.info(f"Time to compute_pdets: {compute_pdets - compute_mf_snrs}")

        # Later we can add a weighting in case target sky and sampling sky differ
        # For now assert they don't
        assert self.sampling_distribution["ra"] == self.target_distribution["ra"]
        assert self.sampling_distribution["dec"] == self.target_distribution["dec"]
        assert self.sampling_distribution["psi"] == self.target_distribution["psi"]

        # Now filter by distance, and average over all sky points for that distance
        detection_df = (
            scaling_df[["luminosity_distance", "redshift"] + self.p_det_labels]
            .groupby("luminosity_distance", as_index=False)
            .mean()
        )

        # Get the conditioned probability for USF, and compute ln(usf_prob / sampling_prob)
        usf_conditioned_ln_probabilities = (
            self.get_mass_conditioned_redshift_ln_probabilities(
                detection_df["redshift"],
                detector_mass,
                self.uniform_source_frame_distribution,
            )
        )
        detection_df["ln_prob_usf_vs_sampling_distance"] = (
            usf_conditioned_ln_probabilities - ln_sampling_distance_probability
        )

        # If appropriate, also get the ratio for target vs usf
        if self.target_distance_distribution == self.uniform_source_frame_distribution:
            detection_df["ln_prob_target_vs_usf_distance"] = 0
        else:
            detection_df["ln_prob_target_vs_usf_distance"] = (
                self.get_mass_conditioned_redshift_ln_probabilities(
                    detection_df["redshift"],
                    detector_mass,
                    self.target_distance_distribution,
                )
                - usf_conditioned_ln_probabilities
            )

        # Assign the intrinsic index
        detection_df["intrinsic_index"] = intrinsic_index

        # Benchmark
        if benchmark:
            compress = timeit.default_timer()
            logger.info(f"Time to compress detection df: {compress - compute_pdets}")
            logger.info(f"Total time for distance scaling:{compress-start}")

        return detection_df

    def calc_snr_single(
        self,
        row,
    ) -> pd.Series:
        """
        Computes the SNR at a single (given) sky location for the row

        Parameters
        ----------
        row :: pd.Series
            The row from the dataframe to compute the SNR over

        Returns
        --------
        row_tmp :: pd.Series
            The row, with SNR computed
        """
        # setup row appropriately
        row_tmp = copy.copy(row)

        # Get wf gen args, including luminosity distance
        # Note this implies choosing to set distance of 1Mpc should be done elsewhere
        param_args = {
            k: row_tmp[k]
            for k in (self.waveform_generation_labels + ["luminosity_distance"])
        }
        position_args = {k: row_tmp[k] for k in self.detector_position_labels}

        # Get live psds from the network
        end_time = row_tmp["geocent_time"]
        psd_dict = self.network.psds_for_time(end_time)

        # Check if any detectors are on
        if len(psd_dict.keys()) != 0:
            # If at least one detector is on, proceed as normal
            # TODO add if statements to accommodate the t case (currently it's just the same as opt)
            hphc = dict()
            hphc["plus"], hphc["cross"] = compute_hphc_fd(
                param_args,
                self.network.recovery_approximant,
                delta_f=self.network.delta_f,
                f_max=self.network.f_max,
                f_min=self.network.f_min,
            )

            strains = dict()
            for ifo in psd_dict.keys():
                strains[ifo] = {
                    "h": project_and_combine(
                        position_args,
                        hphc,
                        end_time,
                        self.network.detectors[ifo],
                    )
                }

            opt_snr_dict, h_hprime_wip_dict, h_noise_wip_dict = compute_snr_fd(
                strains, psd_dict, f_min=self.network.f_min, f_max=self.network.f_max
            )
        else:
            # Otherwise setup 0s
            opt_snr_dict = dict()
            h_hprime_wip_dict = dict()
            h_noise_wip_dict = dict()

        for name in self.network.ifos + ["net"]:
            # For detectors not on, fill in 0
            if name not in opt_snr_dict.keys():
                opt_snr_dict[name] = 0
                h_hprime_wip_dict[name] = 0
                h_noise_wip_dict[name] = 0
            row_tmp[f"opt_snr_{name}"] = opt_snr_dict[name]
            if name != "net":
                row_tmp[f"h_hprime_wip_{name}"] = h_hprime_wip_dict[name]
                row_tmp[f"h_noise_wip_{name}"] = h_noise_wip_dict[name]
        return row_tmp

    def get_mf_snr_from_wips(
        self,
        df: pd.DataFrame,
    ) -> pd.DataFrame:
        """
        For a dataframe, use wips and opt_snr for each ifo to get mf_snr

        Parameters
        -----------
        df ::pd.DataFrame
            The dataframe to act upon

        Returns
        -----------
        df ::pd.DataFrame
            The input dataframe with computed mf_snr columns
        """
        mf_snr_sq = np.zeros(df["opt_snr_net"].shape)
        for ifo_name in self.network.ifos:
            # If hprimes are 0, may still have mf_snr != opt_snr due to noise
            # for this case use the template | template assumption
            # This is actually just h' = h, as is true in the default
            # But we save time by only recognizing this at the end
            if (df[f"h_hprime_wip_{ifo_name}"] == 0).all():
                df[f"mf_snr_{ifo_name}"] = (
                    df[f"opt_snr_{ifo_name}"] ** 2 + df[f"h_noise_wip_{ifo_name}"]
                ) / df[f"opt_snr_{ifo_name}"]
            else:
                df[f"mf_snr_{ifo_name}"] = (
                    df[f"h_hprime_wip_{ifo_name}"] + df[f"h_noise_wip_{ifo_name}"]
                ) / df[f"opt_snr_{ifo_name}"]
            mf_snr_sq += df[f"mf_snr_{ifo_name}"] ** 2
            df["mf_snr_net"] = np.sqrt(mf_snr_sq)
        return df

    ################################################################
    ################################################################
    ####                    Sampling Functions                  ####
    ################################################################
    ################################################################

    @property
    def uniform_source_frame_distribution(
        self,
    ) -> PriorDict:
        return self._usf_distribution

    @uniform_source_frame_distribution.setter
    def uniform_source_frame_distribution(
        self,
        distance_prior,
    ) -> None:
        self._usf_distribution = (
            self.get_uniform_source_frame_distribution_from_distance_prior(
                distance_prior, self.cosmology_model
            )
        )
        return None

    @staticmethod
    def get_uniform_source_frame_distribution_from_distance_prior(
        distance_prior,
        cosmology_model,
    ):
        if distance_prior.name == "redshift":
            if isinstance(distance_prior, bilby.gw.prior.UniformSourceFrame):
                return distance_prior
            else:
                redshift_minimum = distance_prior.minimum
                redshift_maximum = distance_prior.maximum
        elif distance_prior.name:
            redshift_minimum = SimulationSet.compute_zvals(
                distance_prior.minimum, cosmology_model
            )
            redshift_maximum = SimulationSet.compute_zvals(
                distance_prior.maximum, cosmology_model
            )
        usf_distribution = bilby.gw.prior.UniformSourceFrame(
            minimum=redshift_minimum,
            maximum=redshift_maximum,
            cosmology=cosmology_model,
            name="redshift",
            latex_label="$z$",
            unit=cu.redshift,
        )
        return usf_distribution

    @property
    def sampling_distribution(self) -> PriorDict:
        """
        The prior probability for the simulation set - the set may or may not be distributed according to this!
        """
        return self._sampling_distribution

    @sampling_distribution.setter
    def sampling_distribution(
        self,
        distribution: Union[None, dict, PriorDict],
    ) -> None:
        if distribution is not None:
            self._sampling_distribution = PriorDict(distribution)
        else:
            self._sampling_distribution = None
            logging.info(
                "No distribution input given, default to None,\
                 please form distribution before using the SimulationSet object"
            )

    @property
    def target_distribution(self) -> PriorDict:
        """
        The prior probability for the simulation set - the set may or may not be distributed according to this!
        """
        return self._target_distribution

    @target_distribution.setter
    def target_distribution(
        self,
        distribution: Union[None, dict, PriorDict],
    ) -> None:
        if distribution is not None:
            self._target_distribution = PriorDict(distribution)
        else:
            self._target_distribution = None
            logging.info(
                "No distribution input given, default to None,\
                 please form distribution before using the SimulationSet object"
            )

    def sample_from_sampling_distribution(self, n_samples: int) -> None:
        """
        A function that samples from a prior and fills out the simulations_dataframe
        Parameters
        ----------------------
        n_samples ::int
            An integer with the number of samples that will be created
        Returns
        -------------
        """
        # sample from prior:
        samples = self.sampling_distribution.sample(n_samples)

        # create the df:
        temp_df = pd.DataFrame.from_dict(samples)

        temp_df = self.convert_distances(temp_df, self.cosmology_model)

        # do the necessary conversions:
        if any([key for key in temp_df.keys() if "source" in key]):
            temp_df = self.fill_out_masses(temp_df, source=True)
            temp_df = self.convert_source_to_detector(temp_df, self.cosmology_model)
        else:
            temp_df = self.fill_out_masses(temp_df, source=False)
        temp_df = self.convert_spins(temp_df)

        # add appropriate intrinsic labels
        temp_df["intrinsic_index"] = np.NaN
        temp_df = self.add_intrinsic_labels(temp_df)

        # concatenate with the df:
        self.simulations_dataframe = pd.concat(
            [self.simulations_dataframe, temp_df], ignore_index=True
        )

        self.simulations_dataframe = self.convert_appropriate_columns_to_categorical(
            self.simulations_dataframe
        )

    def assign_sampling_distribution_log_probability(self) -> None:
        """
        A function for populating the ln_sampling_prob column
        """
        intrinsic_dataframe = copy.deepcopy(
            self.simulations_dataframe[self.intrinsic_index_tracking_labels]
        )
        intrinsic_dataframe.drop_duplicates(inplace=True)
        intrinsic_dataframe = self.cast_all_columns_to_float32(intrinsic_dataframe)

        intrinsic_dataframe["ln_sampling_prob"] = np.sum(
            self.sampling_distribution[key].ln_prob(intrinsic_dataframe[key])
            for key in self.sampling_distribution.keys()
            if self.simulations_dataframe[key].notna().all()
            and key in self.intrinsic_index_tracking_labels
        )
        for intrinsic_index in tqdm(intrinsic_dataframe["intrinsic_index"]):
            self.detection_dataframe.loc[
                self.detection_dataframe.index[
                    self.detection_dataframe["intrinsic_index"] == intrinsic_index
                ],
                "ln_sampling_prob",
            ] = float(
                intrinsic_dataframe.loc[
                    intrinsic_dataframe["intrinsic_index"] == intrinsic_index,
                    "ln_sampling_prob",
                ]
            )

        del intrinsic_dataframe

    def assign_target_distribution_log_probability(self) -> None:
        """
        A function for populating the ln_target_prob column
        """
        intrinsic_dataframe = copy.deepcopy(
            self.simulations_dataframe[self.intrinsic_index_tracking_labels]
        )
        intrinsic_dataframe.drop_duplicates(inplace=True)
        intrinsic_dataframe = self.cast_all_columns_to_float32(intrinsic_dataframe)

        intrinsic_dataframe["ln_target_prob"] = np.sum(
            self.target_distribution[key].ln_prob(intrinsic_dataframe[key])
            for key in self.target_distribution.keys()
            if self.simulations_dataframe[key].notna().all()
            and key in self.intrinsic_index_tracking_labels
        )
        for intrinsic_index in tqdm(intrinsic_dataframe["intrinsic_index"]):
            self.detection_dataframe.loc[
                self.detection_dataframe.index[
                    self.detection_dataframe["intrinsic_index"] == intrinsic_index
                ].tolist(),
                "ln_target_prob",
            ] = float(
                intrinsic_dataframe.loc[
                    intrinsic_dataframe["intrinsic_index"] == intrinsic_index,
                    "ln_target_prob",
                ]
            )

        del intrinsic_dataframe

    def get_mass_conditioned_redshift_ln_probabilities(
        self,
        redshifts: np.ndarray,
        detector_mass: float,
        unconditioned_distance_prior: bilby.gw.prior.Cosmological,
    ):
        """
        Give probabilities associated with redshift, after conditioning on mass distribution

        Parameters
        ------------
        redshifts ::np.ndarray
            An array of the redshifts for which probabilities should be computed
        detector_mass ::float
            The detector mass to condition on
        unconditioned_distance_prior ::Cosmological
            A distance prior with the get_corresponding_prior method, to get redshift probabilities

        Returns
        ------------
        ln_prob_conditioned
            The log probabilities of the redshifts after conditioning on detector mass
        """
        source_mass_prob = self.source_mass_prior.ln_prob(
            detector_mass / (1 + redshifts)
        )
        redshift_unconditioned = unconditioned_distance_prior.get_corresponding_prior(
            "redshift"
        )
        redshift_prob = redshift_unconditioned.ln_prob(redshifts)
        detector_mass_prob = self.detector_mass_prior.ln_prob(detector_mass)

        return (
            source_mass_prob
            + redshift_prob
            - np.log(1 + redshifts)
            - detector_mass_prob
        )

    @staticmethod
    def get_distance_prior_from_prior_dict(
        prior_dict,
        cosmology,
    ):
        distance_dict = {
            label: prior_dict[label]
            for label in SimulationSet.distance_labels
            if label in prior_dict
        }
        if len(distance_dict.keys()) == 1:
            original_prior = list(distance_dict.values())[0]
            if original_prior.name == "redshift":
                if isinstance(original_prior, bilby.gw.prior.Cosmological):
                    return original_prior.get_corresponding_prior("luminosity_distance")
                else:
                    # TODO make a conversion prior to call
                    raise ValueError
            elif original_prior.name == "luminosity_distance":
                return LuminosityDistanceCosmological(
                    original_prior,
                    cosmology=cosmology,
                    name=original_prior.name,
                    minimum=original_prior.minimum,
                    maximum=original_prior.maximum,
                    latex_label=original_prior.latex_label,
                    boundary=original_prior.boundary,
                )
        else:
            raise ValueError("Overdetermined distance priors")

    ################################################################
    ################################################################
    ####                    Conversion Utilities                ####
    ################################################################
    ################################################################

    @staticmethod
    def fill_out_masses(
        df: pd.DataFrame,
        source=False,
    ) -> pd.DataFrame:
        """
        A function that converts the masses to guarantee that the Dataframe has mass 1 and mass 2
        Parameters
        ----------------------
        df ::dataframe
            The input dataframe with at least one component with overall mass scaling (i.e.
            chirp_mass, mass_1, mass_2, total_mass) and then any other mass parameter.
        Returns
        -------------
        temp_df: updated dataframe
        """
        df_converted_masses = conversion.generate_mass_parameters(
            df.dropna(axis=1),
            source=source,
        )
        return df_converted_masses

    @staticmethod
    def convert_spins(
        df: pd.DataFrame,
    ) -> pd.DataFrame:
        """
        A function that adds the spins x, y, and z components to the Dataframe
        Parameters
        ----------------------
        df ::dataframe
            The input dataframe with spins parameters to be converted to
        Returns
        -------------
        temp_df: updated dataframe
        """
        # check if all necessary parameters are present in the Dataframe
        necessary_keys = [
            "mass_1",
            "mass_2",
            "theta_jn",
            "phi_jl",
            "tilt_1",
            "tilt_2",
            "phi_12",
            "a_1",
            "a_2",
            "reference_frequency",
            "phase",
        ]
        for k in necessary_keys:
            if len(df[df[k].isna()]) > 0:
                raise KeyError(f"{k} fields are missing in the dataframe.")

        # make the spins conversion:
        temp_df = conversion.generate_component_spins(df)

        return temp_df

    @staticmethod
    def convert_source_to_detector(
        df,
        cosmo_model,
    ):
        """
        A function that will fill out the source mass columns
        Parameters
        ----------------------
        df ::dataframe
            The input dataframe with spins parameters to be converted to
        Returns
        -------------
        temp_df: updated dataframe
        """
        if "redshift" not in df.keys():
            # fill out redshift
            df = SimulationSet.convert_distances(df, cosmo_model)

        # Fill out columns if necessary:
        for key in [
            "mass_1_source",
            "mass_2_source",
            "chirp_mass_source",
            "total_mass_source",
            "mass_ratio",
            "symmetric_mass_ratio",
        ]:
            if df[key].isnull().values.any():
                df = SimulationSet.fill_out_masses(df, source=True)

        # Apply redshift
        df["mass_1"] = df.apply(
            lambda row: (1 + row["redshift"]) * row["mass_1_source"], axis=1
        )
        df["mass_2"] = df.apply(
            lambda row: (1 + row["redshift"]) * row["mass_2_source"], axis=1
        )
        df["chirp_mass"] = df.apply(
            lambda row: (1 + row["redshift"]) * row["chirp_mass_source"], axis=1
        )
        df["total_mass"] = df.apply(
            lambda row: (1 + row["redshift"]) * row["total_mass_source"], axis=1
        )

        return df

    @staticmethod
    def convert_detector_to_source(
        df,
        cosmo_model,
    ):
        """
        A function that will fill out the source mass columns
        Parameters
        ----------------------
        df ::dataframe
            The input dataframe with spins parameters to be converted to
        Returns
        -------------
        temp_df: updated dataframe
        """
        # Fill out redshift
        df = SimulationSet.convert_distances(df, cosmo_model)

        # Fill out columns if necessary:
        for key in [
            "mass_1",
            "mass_2",
            "chirp_mass",
            "total_mass",
            "mass_ratio",
            "symmetric_mass_ratio",
        ]:
            if df[key].isnull().values.any():
                df = SimulationSet.fill_out_masses(df)

        # Remove redshifting
        df["mass_1_source"] = df.apply(
            lambda row: row["mass_1"] / (1 + row["redshift"]), axis=1
        )
        df["mass_2_source"] = df.apply(
            lambda row: row["mass_2"] / (1 + row["redshift"]), axis=1
        )
        df["chirp_mass_source"] = df.apply(
            lambda row: row["chirp_mass"] / (1 + row["redshift"]), axis=1
        )
        df["total_mass_source"] = df.apply(
            lambda row: row["total_mass"] / (1 + row["redshift"]), axis=1
        )

        return df

    @staticmethod
    def convert_distances(
        df,
        cosmo_model,
    ):
        """
        A function that returns an updated dataframe with all distances converted

        Parameters
        ----------------------
        df ::dataframe
            The input dataframe with spins parameters to be converted to
        Returns
        -------------
        temp_df: updated dataframe
        """
        if "luminosity_distance" in df.keys() and "redshift" not in df.keys():
            df["redshift"] = SimulationSet.compute_zvals(
                df["luminosity_distance"].to_numpy(),
                cosmo_model,
            )
        elif "redshift" in df.keys() and "luminosity_distance" not in df.keys():
            df["luminosity_distance"] = cosmo_model.luminosity_distance(
                df["redshift"].to_numpy()
            ).value
        return df

    @staticmethod
    def compute_zvals(
        dL,
        cosmo_model=None,
    ):
        """

        A wrapper of computing redshift from luminosity distance
        which works correctly for arrays
        https://docs.astropy.org/en/stable/api/astropy.cosmology.z_at_value.html

        Inputs:
        ---------
        dL ::array
            the luminosity distance (array-like) to compute the zvalues for
        cosmology ::astropy.cosmology object
            the cosmology model to use in computing redshift

        Returns:
        ----------
        zvals ::a corresponding array of redshifts for the input dLs
        """
        dL = copy.deepcopy(dL)
        dL = dL * u.Mpc
        zmin = z_at_value(cosmo_model.luminosity_distance, dL.min())
        zmax = z_at_value(cosmo_model.luminosity_distance, dL.max())
        zgrid = np.logspace(np.log10(zmin.value), np.log10(zmax.value), 100)
        Dgrid = cosmo_model.luminosity_distance(zgrid)
        zvals = np.interp(dL.value, Dgrid.value, zgrid)
        return zvals

    ################################################################
    ################################################################
    ####                    Detection Utilities                 ####
    ################################################################
    ################################################################

    @property
    def detection_functions(self) -> list:
        return self._detection_functions

    @detection_functions.setter
    def detection_functions(
        self,
        functions_function_names_and_kwargs=[[], [], []],
    ) -> None:
        self._detection_functions = []
        detection_functions = functions_function_names_and_kwargs[0]
        detection_function_name_list = functions_function_names_and_kwargs[1]
        detection_function_kwargs_list = functions_function_names_and_kwargs[2]
        for function in detection_functions:
            self.add_detection_function(function=function)
        for ii, function_name in enumerate(detection_function_name_list):
            self.add_detection_function(
                function_name=function_name,
                function_generator_kwargs=detection_function_kwargs_list[ii],
            )

    def add_detection_function(
        self,
        function=None,
        function_name=None,
        function_generator_kwargs=None,
    ):
        if function is not None:
            self._detection_functions += [function]
        elif function_name is not None and function_generator_kwargs is not None:
            add_detection_function_to_list_from_names_and_kwargs(
                self._detection_functions,
                function_name,
                **function_generator_kwargs,
            )
        else:
            raise ValueError(
                "Must specify either function or standard function name and kwargs"
            )

    @staticmethod
    def compute_pdet(
        df: pd.DataFrame,
        detection_function: Callable,
    ):
        """
        A function to compute the p_det for a dataframe's inputs, using the given detection function

        Parameters
        -----------
        df ::pd.DataFrame
            The dataframe to act on
        detection_function ::Function
            The detection function to apply
            Should be vectorized, take in a df, and output an np.ndarray or pd.Series

        Returns
        ----------
        df ::pd.DataFrame
            The computed dataframe

        """
        return detection_function(df)

    ################################################################
    ################################################################
    ####                    Dataframe Utilities                 ####
    ################################################################
    ################################################################

    def make_dummy_df(
        self,
    ) -> pd.DataFrame:
        """
        Makes a dummy dataframe to get the correct categorical structure for sub-dataframes

        Returns
        ----------
        dummy_df ::pd.DataFrame
            An empty dataframe with the correct categories and column types pre-configured
        """
        self.simulations_dataframe = self.convert_appropriate_columns_to_categorical(
            self.simulations_dataframe
        )
        dummy_df = self.simulations_dataframe.drop(self.simulations_dataframe.index)
        return dummy_df

    @staticmethod
    def convert_appropriate_columns_to_categorical(
        df,
    ):
        keys_to_read_as_float = [
            key for key in df.keys() if "snr" in key and key != "reference_snr_index"
        ]
        keys_to_read_as_float += [key for key in df.keys() if "wip" in key]
        keys_to_read_as_float += [key for key in df.keys() if "source" in key]
        keys_to_read_as_float += [
            "redshift",
            "luminosity_distance",
            "ln_sampling_prob",
            "ln_target_prob",
            "ra",
            "dec",
            "psi",
        ]
        for key in df.keys():
            if key in keys_to_read_as_float:
                df[key] = df[key].astype(np.float32)
            else:
                df[key] = df[key].astype("category")
        return df

    @staticmethod
    def cast_all_columns_to_float32(
        df,
    ):
        for key in df.keys():
            df[key] = df[key].astype(np.float32)
        return df

    def write_dataframe_and_distribution(
        self,
        output_dir: str,
        simulations_dataframe_name="simset_df.gzip",
        detection_dataframe_name="detection_df.gzip",
        sampling_distribution_name="simset_sampling.priors",
        target_distribution_name="simset_target.priors",
    ) -> None:
        """
        Writes the simulations dataframe into a parquet file

        Parameters
        ------------
        output_path ::str
            The directory to which the dataframe and distribution should be written
        """
        # Some convoluted stuff to make sure overwriting doesn't happen
        if os.path.isdir(output_dir):
            output_dir_contents = os.listdir(output_dir)
            for name in [
                simulations_dataframe_name,
                detection_dataframe_name,
                sampling_distribution_name,
                target_distribution_name,
            ]:
                name_increment = 0
                for fname in output_dir_contents:
                    if name in fname:
                        name_increment += 1
                if name_increment != 0:
                    name = f"{name_increment}_" + name
        else:
            os.makedirs(output_dir)

        self.simulations_dataframe.to_parquet(
            path=os.path.join(output_dir, simulations_dataframe_name)
        )
        self.detection_dataframe.to_parquet(
            path=os.path.join(output_dir, detection_dataframe_name)
        )
        self.sampling_distribution.to_file(
            outdir=output_dir, label=sampling_distribution_name
        )
        self.target_distribution.to_file(
            outdir=output_dir, label=target_distribution_name
        )

    @staticmethod
    def read_simset_dataframe(
        input_path: str,
    ) -> pd.DataFrame:
        """
        Reads a parquet file to construct a simulations dataframe

        Parameters
        --------------
        input_path ::str
            The path to the file

        Returns
        -----------
        dataframe ::pd.DataFrame
            The corresponding dataframe, with appropriate categories set
        TODO add a check that this is a valid SimulationSet Dataframe
        """
        simset_df = pd.read_parquet(path=input_path)
        simset_df = SimulationSet.convert_appropriate_columns_to_categorical(simset_df)

        return simset_df

    def load_simset_dataframes(
        self, simulations_dataframe_path: str, detection_dataframe_path: str
    ) -> None:
        self.simulations_dataframe = self.read_simset_dataframe(
            simulations_dataframe_path
        )
        self.detection_dataframe = pd.read_parquet(detection_dataframe_path)

        return None

    def make_clean_simulations_dataframe(
        self,
    ) -> None:
        """
        Wipes the current simulations_dataframe and makes a new one with the correct columns
        """
        self.simulations_dataframe = pd.DataFrame(
            columns=self.all_simulations_dataframe_labels
        )

    def add_intrinsic_labels(
        self,
        dataframe: pd.DataFrame,
    ) -> pd.DataFrame:
        """
        Adds intrinsic labels based on the existing simulations_dataframe to a new dataframe

        Parameters
        -----------
        dataframe ::pd.DataFrame
            The dataframe to add intrinsic indices to

        Returns
        -----------
        temp_df ::pd.DataFrame
            The input dataframe with intrinsic index added
        """
        # Add intrinsic index
        # Start at length of self.simulations_dataframe
        temp_df = copy.deepcopy(dataframe)

        # we are adding on, so we'll make the maximum current value +1
        # This may be leaving gaps in the intrinsic indices
        # Only uniqueness matters though
        if not self.simulations_dataframe["intrinsic_index"].isna().all():
            # If any are non-nan, use that as the max
            start_sampling_point = (
                self.simulations_dataframe["intrinsic_index"].max(skipna=True) + 1
            )
        else:
            # If they are all nan, then the start is 0
            start_sampling_point = 0

        # If they are all nan, then assign one by one
        # this assumes each row is unique
        # It's up to the user to not do something weird and cause that
        if temp_df["intrinsic_index"].isna().all():
            n_samples = temp_df.shape[0]
            intrinsic_indices = np.linspace(
                start_sampling_point, start_sampling_point + n_samples - 1, n_samples
            )
            temp_df["intrinsic_index"] = intrinsic_indices
        # If none are Nan, then remap what exists to the new starting point
        elif temp_df["intrinsic_index"].notna().all():
            current_intrinsic_index = temp_df["intrinsic_index"].head(1)
            index_being_assigned = start_sampling_point
            for idx, row in temp_df.iterrows():
                if row["intrinsic_index"] != current_intrinsic_index:
                    current_intrinsic_index = row["intrinsic_index"]
                    index_being_assigned += 1
                row["intrinsic_index"] = index_being_assigned
        # If there's a mixture then
        # a) why
        # b) I don't want to write this right now
        else:
            # TODO support this option
            logger.info(
                "Mixed assignment and non-assignment of intrinsic indices.\
                This configuration is not presently supported.\
                Also, how did it even happen?"
            )
            raise ValueError
        return temp_df

    @staticmethod
    def set_row_to_d_ref(
        row: pd.Series,
        d_ref_mpc=1.0,
    ) -> pd.Series:
        """
        Sets a row to d_ref, while also remembering to wipe source frame parameters

        Parameters
        -------------
        row ::pd.Series
            A row from a dataframe, upon which to apply the transformation
        d_ref_mpc ::float
            The reference luminosity distance to set the row to

        Returns
        ------------
        row ::pd.Series
            The updated row
        """
        row["luminosity_distance"] = d_ref_mpc
        row["redshift"] = np.NaN
        source_keys = [
            key for key in SimulationSet.conversion_labels if "source" in key
        ]
        for key in source_keys:
            row[key] = np.NaN
        return row

    def add_dataframe_to_simulations_dataframe(
        self,
        dataframe: pd.DataFrame,
    ) -> None:
        """
        Adds a new dataframe, with correctly set columns, to self.simulations_dataframe

        Parameters
        -----------
        dataframe ::pd.DataFrame
            The dataframe to add
        """
        dataframe = self.add_intrinsic_labels(dataframe=dataframe)
        self.simulations_dataframe = pd.concat(
            [self.simulations_dataframe, dataframe], ignore_index=True
        )
        self.simulations_dataframe.reset_index()

    ################################################################
    ################################################################
    ####                         Labels                         ####
    ################################################################
    ################################################################

    @classproperty
    def waveform_generation_labels(
        self,
    ) -> list:
        """
        10 labels which are passed into compute_strain_fd
        Excludes distance which is special
        Remember - bilby notation is detector frame by default
        So, mass_1 here is (1+z) mass_1_src
        """
        labels = [
            "mass_1",
            "mass_2",
            "spin_1x",
            "spin_1y",
            "spin_1z",
            "spin_2x",
            "spin_2y",
            "spin_2z",
            "theta_jn",
            "phase",
        ]
        return labels

    @classproperty
    def detector_position_labels(
        self,
    ) -> list:
        """
        3 labels which are passed into project_and_combine
        """
        labels = [
            "ra",
            "dec",
            "psi",
        ]
        return labels

    @classproperty
    def time_labels(
        self,
    ) -> list:
        """
        A single time label, separated for convenience
        Could store other time labels here I guess?
        """
        labels = [
            "geocent_time",
        ]
        return labels

    @classproperty
    def distance_labels(
        self,
    ) -> list:
        """
        The two labels used for distance
        """
        labels = [
            "luminosity_distance",
            "redshift",
        ]
        return labels

    @classproperty
    def conversion_labels(
        self,
    ) -> list:
        """
        Other useful params (maybe the ones which are sampled in)
        Note _source modifier if a source frame quantity
        """
        labels = [
            "chirp_mass",
            "total_mass",
            "mass_ratio",
            "symmetric_mass_ratio",
            "a_1",
            "a_2",
            "tilt_1",
            "tilt_2",
            "phi_12",
            "phi_jl",
            "reference_frequency",
        ]
        return labels

    @classproperty
    def source_labels(
        self,
    ) -> list:
        """
        Labels for source frame masses
        """
        labels = [
            "mass_1_source",
            "mass_2_source",
            "chirp_mass_source",
            "total_mass_source",
        ]
        return labels

    @property
    def network_dependent_labels(
        self,
    ) -> list:
        """
        Labels which are in someway dependent on the network in use
        """
        labels = [
            "cal_idx",
        ]
        for ifo in self.network.ifos:
            labels += [
                f"opt_snr_{ifo}",
                f"mf_snr_{ifo}",
                f"h_hprime_wip_{ifo}",
                f"h_noise_wip_{ifo}",
            ]
        labels += ["opt_snr_net", "mf_snr_net"]
        return labels

    @property
    def pipeline_labels(
        self,
    ) -> list:
        """
        Labels used by the pipeline for e.g. indexing, or importance sampling
        """
        labels = [
            "intrinsic_index",
            "reference_snr_index",
            "ln_sampling_prob",
            "ln_target_prob",
        ]
        return labels

    @property
    def p_det_labels(
        self,
    ):
        p_det_labels = []
        for ii in range(len(self.detection_functions)):
            p_det_labels += [f"p_det_{ii}"]
        return p_det_labels

    @property
    def intrinsic_index_tracking_labels(
        self,
    ):
        labels = (
            self.time_labels
            + self.waveform_generation_labels
            + self.conversion_labels
            + ["intrinsic_index"]
        )
        return labels

    @property
    def all_simulations_dataframe_labels(
        self,
    ) -> list:
        labels = (
            self.waveform_generation_labels
            + self.detector_position_labels
            + self.time_labels
            + self.distance_labels
            + self.conversion_labels
            + self.pipeline_labels
            + self.network_dependent_labels
        )
        return labels

    ################################################################
    ################################################################
    ####     Currently Broken / Deprecated but not Removed      ####
    ################################################################
    ################################################################

    def set_observable_dmax(
        self,
    ) -> None:
        """
        A function to modify the maximum distance in the distribution according to what is feasibly observable
        """
        if self.simulations_dataframe["opt_snr_net"].notna().all():
            # In this conditional, optimal SNRs are already available for at least 100 points
            # So we can take the max for the scaling baseline
            # Define this for a conditional later
            temporary_holding_simdf = None

        else:
            # If this no SNRs are computed (because this isn't inside of fill_out_snrs, likely)
            # Then make 100 cases to find a rough maximum
            # First store the current dataframe for later
            temporary_holding_simdf = copy.deepcopy(self.simulations_dataframe)
            # Make a fresh, small dataframe and sample 100 points
            self.make_clean_simulations_dataframe()
            self.sample_from_sampling_distribution(100)
            # Set to face on to maximize SNRs
            self.simulations_dataframe["inclination"] = 0
            # Get sky averaged SNRS
            scatter_dfs = []
            logger.info("Beginning sky scattering")
            dummy_df = self.make_dummy_df()
            for _, row in tqdm(self.simulations_dataframe.iterrows()):
                if self.distance_scale:
                    row = self.set_row_to_d_ref(row, self.d_ref_mpc)
                scatter_dfs += [
                    self.calc_snrs_sky(
                        row,
                        n_sky=5000,
                        dummy_df=dummy_df,
                        benchmark=False,
                    )
                ]
            # Form the DF
            self.simulations_dataframe = pd.concat(scatter_dfs, ignore_index=True)

        # Find the row with the highest SNR
        max_snr_row = self.simulations_dataframe.loc[
            self.simulations_dataframe["opt_snr_net"].idxmax()
        ]
        # Get max SNR and corresponding reference distance
        max_snr = max_snr_row["opt_snr_net"]
        max_snr_ref_dist = max_snr_row["luminosity_distance"]
        # Scale the maximum observable distance, and set the prior accordingly
        max_observable_distance = max_snr_ref_dist * max_snr / 8
        logging.info(
            f"Reconfiguring distance prior based on computed maximum observable distance:\
            The new maximum is {max_observable_distance}"
        )
        self.sampling_distribution[
            "luminosity_distance"
        ].maximum = max_observable_distance
        self.target_distribution[
            "luminosity_distance"
        ].maximum = max_observable_distance

        if temporary_holding_simdf is not None:
            self.simulations_dataframe = temporary_holding_simdf
