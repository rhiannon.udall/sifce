import copy
import logging
import os
import shutil
import unittest

import numpy as np
from bilby.core.prior import Cosine, DeltaFunction, PriorDict, Uniform
from pycbc.detector import Detector
from pycbc.types import FrequencySeries

import sifce.datatools as dtools
from sifce.utils import read_psd_from_txt

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestStrainGeneration(unittest.TestCase):
    def cleanup(self):
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setUp(self) -> None:
        self.test_param_args = dict(
            mass_1=35,
            mass_2=35,
            spin_1x=0,
            spin_1y=0,
            spin_1z=0,
            spin_2x=0,
            spin_2y=0,
            spin_2z=0,
            theta_jn=0,
            phase=0,
            luminosity_distance=1,
        )
        self.test_approximant = "IMRPhenomXPHM"
        self.test_delta_f = 1 / 4.0
        self.test_f_min = 21
        self.test_f_max = 512
        self.end_time = 10000000
        self.detector_dict = dict(
            H1=Detector("H1"), L1=Detector("L1"), V1=Detector("V1")
        )
        self.psd_dict = dict()
        for ifo in self.detector_dict.keys():
            self.psd_dict[ifo] = read_psd_from_txt(
                f"test/o3_{ifo.lower()}.txt",
                f_min=self.test_f_min,
                f_max=self.test_f_max,
                delta_f=self.test_delta_f,
                asd=True,
            )

        self.position_args_pdict = PriorDict(
            dict(ra=Uniform(0, 2 * np.pi), dec=Cosine(), psi=Uniform(0, np.pi))
        )

        self.hp, self.hc = dtools.compute_hphc_fd(
            self.test_param_args,
            self.test_approximant,
            delta_f=self.test_delta_f,
            f_min=self.test_f_min,
            f_max=self.test_f_max,
        )

        self.test_position_args = self.position_args_pdict.sample(1)

        self.strain_dict = dict()
        for ifo, detector in self.detector_dict.items():
            self.strain_dict[ifo] = dict(
                h=dtools.project_and_combine(
                    self.test_position_args,
                    dict(plus=self.hp, cross=self.hc),
                    end_time=self.end_time,
                    detector_obj=detector,
                )
            )

        self.opt_snr_dict, _, _ = dtools.compute_snr_fd(
            self.strain_dict,
            self.psd_dict,
            f_min=self.test_f_min,
            f_max=self.test_f_max,
        )

        self.test_dir = "test/datatools_tests"
        self.cleanup()

    def tearDown(self) -> None:
        del self.test_param_args
        del self.test_approximant
        del self.test_delta_f
        del self.test_f_min
        del self.test_f_max

    def test_hphc_generation(self):
        # Check that the sample frequencies are correct
        assert self.hp.delta_f == self.test_delta_f
        assert self.hc.delta_f == self.test_delta_f
        assert self.hp.sample_frequencies[-1] == self.test_f_max
        assert self.hc.sample_frequencies[-1] == self.test_f_max

        # Check that the values are identically 0 below f_min
        idx_f_min = int(self.test_f_min / self.test_delta_f)
        assert np.all(self.hp[:idx_f_min].numpy() == np.zeros((idx_f_min)))
        assert np.all(self.hc[:idx_f_min].numpy() == np.zeros((idx_f_min)))

    def test_project_and_combine(self):
        for ifo in self.detector_dict.keys():
            # Checks as on hp, hc for waveform conditions
            assert (
                self.strain_dict[ifo]["h"].sample_frequencies
                == self.hp.sample_frequencies
            )

            idx_f_min = int(self.test_f_min / self.test_delta_f)
            assert np.all(
                self.strain_dict[ifo]["h"][:idx_f_min].numpy() == np.zeros((idx_f_min))
            )

            # Check that projection has decreased the magnitude, as it must
            assert np.all(
                np.less_equal(
                    np.linalg.norm(self.strain_dict[ifo]["h"].data),
                    np.linalg.norm((self.hp + self.hc).data),
                )
            )

    def test_compute_snr_fd(self):
        # If we pass arrays of 0s for noise and hprime, we should get 0 back
        strain_dict_test_no_modification = copy.copy(self.strain_dict)
        for ifo in strain_dict_test_no_modification.keys():
            strain_dict_test_no_modification[ifo]["hprime"] = FrequencySeries(
                np.zeros(
                    strain_dict_test_no_modification[ifo]["h"].sample_frequencies.shape[
                        0
                    ]
                ),
                delta_f=self.test_delta_f,
            )
            strain_dict_test_no_modification[ifo]["noise"] = FrequencySeries(
                np.zeros(
                    strain_dict_test_no_modification[ifo]["h"].sample_frequencies.shape[
                        0
                    ]
                ),
                delta_f=self.test_delta_f,
            )

        opt_snrs, h_hprime_wips, h_noise_wips = dtools.compute_snr_fd(
            self.strain_dict,
            self.psd_dict,
            f_min=self.test_f_min,
            f_max=self.test_f_max,
        )

        for ifo in strain_dict_test_no_modification.keys():
            assert opt_snrs[ifo] == self.opt_snr_dict[ifo]
            assert h_hprime_wips[ifo] == 0
            assert h_noise_wips[ifo] == 0

        # if hprime = h, we should get the optimal SNR ** 2 as the WIP
        strain_dict_test_hprime = copy.copy(self.strain_dict)
        for ifo in strain_dict_test_hprime.keys():
            strain_dict_test_hprime[ifo]["hprime"] = copy.copy(
                self.strain_dict[ifo]["h"]
            )

        opt_snrs, h_hprime_wips, h_noise_wips = dtools.compute_snr_fd(
            self.strain_dict,
            self.psd_dict,
            f_min=self.test_f_min,
            f_max=self.test_f_max,
        )

        for ifo in strain_dict_test_hprime.keys():
            assert opt_snrs[ifo] == self.opt_snr_dict[ifo]
            assert np.isclose(opt_snrs[ifo] ** 2, h_hprime_wips[ifo])
            assert h_noise_wips[ifo] == 0
