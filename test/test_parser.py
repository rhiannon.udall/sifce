import copy
import logging
import os
import shutil
import unittest

import numpy as np
from astropy import cosmology as cosmo
from astropy.cosmology import Planck15, Planck18
from bilby.core.prior import PriorDict
from bilby.gw import cosmology
from gwpy.segments import Segment, SegmentList, SegmentListDict
from matplotlib.pyplot import semilogx

import sifce.datatools as dtools
import sifce.parser as sparse

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class TestParserMethods(unittest.TestCase):
    def cleanup(self):
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setUp(self) -> None:
        self.arguments, self.parser = sparse.parse(config_file="test/test_config.cfg")
        self.arguments.cosmology_model = Planck15
        self.network = sparse.construct_network_from_parsed_arguments(self.arguments)
        self.simset = sparse.construct_simulationset_from_parsed_arguments(
            self.arguments
        )

        self.test_dir = "test/parser_tests"
        self.cleanup()

    def test_parser_write(self):
        os.makedirs(self.test_dir)
        sparse.write_altered_config(
            self.arguments,
            self.parser,
            output_path=os.path.join(self.test_dir, "test_write_config.cfg"),
        )
        test_write_arguments, _ = sparse.parse(
            config_file=os.path.join(self.test_dir, "test_write_config.cfg")
        )
        for key in [
            key for key in test_write_arguments.__dict__.keys() if key != "config"
        ]:
            assert test_write_arguments.__dict__[key] == self.arguments.__dict__[key]
        self.cleanup()

    def test_network_constructor(self):
        assert self.network.segments != SegmentListDict()
        assert self.network.ifos != []
        assert self.network.psd_id_map != dict()

        self.network.load_psds()
        test_get_psds = self.network.psds_for_time(4)
        assert test_get_psds != dict()

    def test_simset_constructor(self):
        assert self.simset.network.segments == self.network.segments
        assert isinstance(self.simset.sampling_distribution, PriorDict)
