""" A script to verify that the .AUTHOR.md file is up to date
- taken from Bilby https://git.ligo.org/lscsoft/bilby/-/blob/master/test/check_author_list.py"""


import re
import subprocess

AUTHORS_list = []
with open("AUTHORS.md", "r") as f:
    AUTHORS_list = " ".join([line for line in f]).lower()


lines = (
    subprocess.check_output(["git", "shortlog", "HEAD", "-sn"])
    .decode("utf-8")
    .split("\n")
)

if len(lines) == 0:
    raise Exception("No authors to check against")


def remove_accents(raw_text):

    raw_text = re.sub("[àáâãäå]", "a", raw_text)
    raw_text = re.sub("[èéêë]", "e", raw_text)
    raw_text = re.sub("[ìíîï]", "i", raw_text)
    raw_text = re.sub("[òóôõö]", "o", raw_text)
    raw_text = re.sub("[ùúûü]", "u", raw_text)
    raw_text = re.sub("[ýÿ]", "y", raw_text)
    raw_text = re.sub("[ß]", "ss", raw_text)
    raw_text = re.sub("[ñ]", "n", raw_text)

    return raw_text


fail_test = False
for line in lines:
    line = line.replace(".", " ")
    line = re.sub("([A-Z][a-z]+)", r" \1", re.sub("([A-Z]+)", r" \1", line))
    line = remove_accents(line)
    for element in line.split()[-1:]:
        element = element.lower()
        if element not in AUTHORS_list:
            print(f"Failure: {element} not in AUTHOR.md")
            fail_test += True

if fail_test:
    raise Exception(
        "Author check list failed.. have you added your name to the .AUTHOR file?"
    )
