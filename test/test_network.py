import copy
import logging
import os
import shutil
import unittest

from gwpy.segments import Segment, SegmentList, SegmentListDict

from sifce.network import Network

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestNetworkMethods(unittest.TestCase):
    def cleanup(self):
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def setUp(self) -> None:
        self.test_network = Network()
        self.correct_ifo_segments = SegmentListDict(
            dict(
                H1=SegmentList([Segment([1, 4]), Segment([7, 9])]),
                L1=SegmentList([Segment([3, 5]), Segment([17, 81])]),
            )
        )
        self.correct_id_to_data_map = {0: "test/o3_h1.txt", 1: "test/o3_l1.txt"}
        self.correct_psd_segments = dict(
            H1=SegmentListDict(
                {
                    0: SegmentList([Segment(1, 5)]),
                    1: SegmentList([Segment(5, 9)]),
                }
            ),
            L1=SegmentListDict({1: SegmentList([Segment(3, 5), Segment(17, 81)])}),
        )
        self.test_dir = "test/network_tests"
        self.cleanup()

    def tearDown(self) -> None:
        del self.test_network
        del self.correct_ifo_segments
        self.cleanup()

    def test_check_read_segments(self):
        os.makedirs(self.test_dir)
        shutil.copy("test/test_segments_list.segs", self.test_dir)
        self.test_network.segments = Network.parse_ifo_uptime_file(
            os.path.join(self.test_dir, "test_segments_list.segs")
        )
        assert self.test_network.segments == self.correct_ifo_segments

    def test_check_write_segments(self):
        os.makedirs(self.test_dir)
        Network.write_to_ifo_uptime_file(
            self.correct_ifo_segments,
            os.path.join(self.test_dir, "test_write_segments.segs"),
        )
        self.test_network.segments = Network.parse_ifo_uptime_file(
            os.path.join(self.test_dir, "test_write_segments.segs")
        )
        assert self.test_network.segments == self.correct_ifo_segments

    def test_deconstruct_segments(self):
        self.test_network.segments = self.correct_ifo_segments
        self.test_network.remove_segment([1, 4], "H1")
        self.test_network.remove_segment([7, 9], "H1")

        self.test_network.remove_segment([3, 5], "L1")
        self.test_network.remove_segment([17, 81], "L1")

        self.test_network.remove_ifo("H1")
        self.test_network.remove_ifo("L1")

        assert self.test_network.segments == SegmentListDict()

    def test_build_segments(self):
        # Build up a sample segment list which matches the test list
        self.test_network.add_ifo("H1")
        self.test_network.add_ifo("L1")

        self.test_network.add_segment([1, 3], "H1")
        self.test_network.add_segment([2, 4], "H1")
        self.test_network.add_segment([7, 9], "H1")

        self.test_network.add_segment([3, 5], "L1")
        # An extra test to make sure coalescing is behaving
        self.test_network.add_segment([17, 29], "L1")
        self.test_network.add_segment([29, 81], "L1")

        assert self.test_network.segments == self.correct_ifo_segments

    def test_read_psds(self):
        os.makedirs(self.test_dir)
        shutil.copy("test/test_network_psds.psds", self.test_dir)
        ifo_psd_segments, psd_id_to_data = Network.read_psd_config_file(
            os.path.join(self.test_dir, "test_network_psds.psds")
        )
        assert self.correct_id_to_data_map == psd_id_to_data
        assert self.correct_psd_segments == ifo_psd_segments

    def test_write_psds(self):
        os.makedirs(self.test_dir)
        Network.write_psd_config_file(
            self.correct_psd_segments,
            self.correct_id_to_data_map,
            os.path.join(self.test_dir, "test_write_psds.psds"),
        )
        psd_segments, id_to_data_map = Network.read_psd_config_file(
            os.path.join(self.test_dir, "test_write_psds.psds")
        )
        assert psd_segments == self.correct_psd_segments
        assert id_to_data_map == self.correct_id_to_data_map

    def test_build_network_psds(self):
        self.test_network.segments = self.correct_ifo_segments
        self.test_network.add_psd_to_id_map("test/o3_h1.txt")
        self.test_network.add_psd_to_id_map("test/o3_l1.txt")

        # add the segments
        self.test_network.add_psd_segment(0, "H1", [1, 5])
        self.test_network.add_psd_segment(1, "H1", [5, 9])
        self.test_network.add_psd_segment(1, "L1", [3, 5])
        self.test_network.add_psd_segment(1, "L1", [17, 29])
        self.test_network.add_psd_segment(1, "L1", [29, 81])
        assert self.test_network.psd_id_map == self.correct_id_to_data_map
        assert self.test_network.psd_segments == self.correct_psd_segments

    def test_deconstruct_network_psds(self):
        self.test_network.psd_segments = self.correct_psd_segments
        self.test_network.psd_id_map = self.correct_id_to_data_map

        # Remove the segments
        self.test_network.remove_psd_segment(0, "H1", [1, 5])
        self.test_network.remove_psd_segment(1, "H1", [5, 9])
        self.test_network.remove_psd_segment(1, "L1", [3, 5])
        self.test_network.remove_psd_segment(1, "L1", [17, 29])
        # do a check to make sure partial segment removal works
        assert 22 not in self.test_network.psd_segments["L1"][1]
        assert 34 in self.test_network.psd_segments["L1"][1]
        self.test_network.remove_psd_segment(1, "L1", [29, 81])

        # remove the psd keys
        self.test_network.remove_psd_from_id_map("test/o3_h1.txt")
        self.test_network.remove_psd_from_id_map("test/o3_l1.txt")
        assert self.test_network.psd_segments == dict(
            H1=SegmentListDict(),
            L1=SegmentListDict(),
        )
        assert self.test_network.psd_id_map == dict()

    def test_psd_well_formed(self):
        assert self.test_network.psds_cover_segments
        assert self.test_network.psds_one_to_one_with_segments
