import codecs
import os.path

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


def get_requirements():
    with open("requirements.txt", "r") as ff:
        requirements = ff.readlines()
    return requirements


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setuptools.setup(
    name="sifce",
    version=get_version("sifce/__init__.py"),
    author="Richard Udall, Danila H Yano, Jacob Golomb",
    author_email="rudall@caltech.edu",
    description="A package for doing simplistic injection campaigns, to study e.g. detector uncertainty",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/richard.udall/sifce",
    classifiers=[
        "Programming Language :: Python :: 3.8" "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
    install_requires=get_requirements(),
    packages=["sifce", "sifce_cli"],
    package_dir={"sifce": "sifce", "sifce_cli": "sifce_cli"},
    entry_points={
        "console_scripts": [
            "sifce_pipe=sifce_cli.pipeline_constructor:main",
            "sifce_worker=sifce_cli.worker:main",
            "sifce_run_results=sifce_cli.statistics:main",
            "sifce_distance_adjust=sifce_cli.adjust_dmax:main",
        ]
    },
)
