\subsection{Purpose} \label{Introduction:Purpose}
\SIFCE{} (Simple Injections for Computational Estimates) provides estimates of aggregate quantities for a given detector network, using approximations to allow rapid evaluation.
These should be used to investigate questions which relate the sensitivity of the detectors to astrophysical populations which produce coalescing binaries.
The prototypical such question is propagation of calibration uncertainty into estimates of Sensitive Hyper-Volume.
Future design goals are to provide tools for investigating detector configurations which have not been constructed yet.
This document will elucidate these plans and lay out the schematic of the software, to assist in a major refactor (the 0.4 series of updates) and improve maintainability in the future.

\subsection{Output Quantities}\label{Introduction:Output_Quantities}
\subsubsection{Sensitive Hyper-Volume}\label{Introduction:Output_Quantities:Sensitive_Hyper-Volume}
The principal quantity which this pipeline is designed to compute is the sensitive hypervolume \VT. 
This quantity characterizes the sensitivity of the detector, and may be formulated in a variety of manners depending upon context. 
Parameterized \VT is computed as a function of all CBC parameters except the luminosity distance (see \ref{Conventions} for explanation of these). 
This is almost always marginalized over some or all of these parameters: when this is done it is important that the inputs over which it is marginalized are either drawn from motivated agnostic priors (e.g. assuming an isotropic homogeneous universe), or reflect only one parameter configuration (e.g. fixed masses).
For \SIFCE{} $< 0.4.0$, this method is used, with a run performed given input priors and integrated to produce a single value.
Alternate methods may produce functional dependence, or express total \VT as a function of the population hyper-parameters; the latter is used in R\&P papers.
See \ref{Deriv_VT} for the methods by which these sensitive volumes are computed.
Finally, \SIFCE{} can compare fractional change in this quantity in various cases, motivated by non-astrophysical changes such as calibration or PSD, and these comparisons are the principle output of versions $<= 0.4.0$.

\SIFCE{} is necessarily \emph{not} an exact method of estimating \VT, because it is also dependent upon the search pipeline in use, and \SIFCE{} cannot mimic these behaviors exactly.
However, it is intended for cases where use of the true search pipelines is impossible (due to, e.g. unknown characteristics of future detectors) or too computationally expensive to justify.
Where possible, \SIFCE{} values should be tuned to true values for the pipeline, such that these estimates may be as effective as possible, but this should be considered a phenomenological tuning, rather than theoretically predictive.

\subsection{Design Principles}\label{Introduction:Design_Principles}
\SIFCE{} should incorporate best practices of modern programming:
\begin{enumerate}
  \item Flexibility and Generalizability
  \item Modularity
  \item Scalability
  \item Readabillity
  \item Computational Efficiency (primarily minimizing time complexity)
\end{enumerate}
To implement these, the following overarching design principles should be maintained:
\begin{enumerate}
  \item Minimal repetition (any procedure performed more than once should be packaged as its own function)
  \item Minimal assumptions for inputs, and always favoring labeled data types over unlabeled data types
  \item Standard build tools, CI pipeline, pre-commit checks, \texttt{black} formatting
  \item Relying on standard GW analysis libraries to the greatest possible extent (see \ref{Libraries}) 
  \item Benchmarking and analyzing run-time bottlenecks. 
\end{enumerate}
