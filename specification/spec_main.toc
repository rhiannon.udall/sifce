\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Purpose}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Output Quantities}{2}{subsection.1.2}%
\contentsline {subsubsection}{\numberline {1.2.1}Sensitive Hyper-Volume}{2}{subsubsection.1.2.1}%
\contentsline {subsection}{\numberline {1.3}Design Principles}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}Features}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Produce CBC Draws ("Population") from Source Distributions}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Produce Simulated Data given CBC Parameter Configurations}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Compute Various Quantities for Simulated Data}{4}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Produce Plots for Final and Intermediary Data}{4}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Compute Sensitive Volume for a Simulated Set of Collections}{4}{subsection.2.5}%
\contentsline {section}{\numberline {3}Code Schematic}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}General Utilities}{4}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Classes}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Standards}{4}{section*.2}%
\contentsline {subsubsection}{\numberline {3.1.2}Defined Objects}{4}{subsubsection.3.1.2}%
\contentsline {paragraph}{bilby\_to\_pycbc\_map}{4}{section*.3}%
\contentsline {paragraph}{pycbc\_to\_bilby\_map}{4}{section*.4}%
\contentsline {subsubsection}{\numberline {3.1.3}Functions}{4}{subsubsection.3.1.3}%
\contentsline {paragraph}{remap\_dict}{4}{section*.5}%
\contentsline {paragraph}{read\_psd\_from\_txt}{5}{section*.6}%
\contentsline {subsection}{\numberline {3.2}Data Simulation and Statistics}{5}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Functions}{6}{subsubsection.3.2.1}%
\contentsline {paragraph}{compute\_strain\_fd}{6}{section*.7}%
\contentsline {paragraph}{project\_and\_combine}{6}{section*.8}%
\contentsline {paragraph}{compute\_snr\_fd}{7}{section*.9}%
\contentsline {subsection}{\numberline {3.3}Population Generation and Statistics}{7}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Classes}{8}{subsubsection.3.3.1}%
\contentsline {paragraph}{SimulationSet}{8}{section*.10}%
\contentsline {subparagraph}{Attributes}{8}{section*.11}%
\contentsline {subparagraph}{Methods}{8}{section*.12}%
\contentsline {subsubsection}{\numberline {3.3.2}Functions}{8}{subsubsection.3.3.2}%
\contentsline {subsection}{\numberline {3.4}Plotting}{8}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Functions}{8}{subsubsection.3.4.1}%
\contentsline {subsection}{\numberline {3.5}Parser}{8}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Functions}{8}{subsubsection.3.5.1}%
\contentsline {subsection}{\numberline {3.6}Condor Utils}{8}{subsection.3.6}%
\contentsline {subsubsection}{\numberline {3.6.1}Functions}{9}{subsubsection.3.6.1}%
\contentsline {section}{\numberline {A}Terminology, Definitions, and Notation}{9}{appendix.A}%
\contentsline {section}{\numberline {B}Important Libraries Upon Which \texttt {SIFCE}{} Depends}{9}{appendix.B}%
\contentsline {section}{\numberline {C}Computation of $\langle VT \rangle $ }{9}{appendix.C}%
\contentsline {subsection}{\numberline {C.1}CBC Parameterized Computation}{9}{subsection.C.1}%
\contentsline {subsection}{\numberline {C.2}Population Parameterized Computation}{9}{subsection.C.2}%
