import os

import configargparse as cfgarg
import pandas as pd

from sifce import workflowutils


def main():
    """

    Takes in a worker directory and a config file, performs SNR calcs on sub-population
    """
    parser = cfgarg.ArgumentParser()
    parser.add_argument(
        "-c", required=True, is_config_file=True, help="The config file"
    )
    parser.add_argument(
        "--working-dir",
        type=str,
        help="The directory where the worker will run and output",
    )
    parser.add_argument(
        "--rundir", help="The directory to construct and produce analysis in"
    )
    parser.add_argument("--outname", help="The name of the analysis output")
    parser.add_argument(
        "--ninj", help="The number of injections in the simulated population"
    )
    parser.add_argument(
        "--prior_dict", help="The dictionary to construct a bilby prior dict with"
    )
    parser.add_argument("--cosmo_model", help="The cosmology model to use")
    parser.add_argument(
        "--read_population",
        default=None,
        help="Optionally, pass in a pre-generated population",
    )
    parser.add_argument(
        "--grid_param",
        help="The parameter to grid in, None if no grid,\
    redshift or luminosity_distance",
    )
    parser.add_argument(
        "--num_grid_bins", help="The number of grid elements in the distance grid"
    )
    parser.add_argument(
        "--psd_basedir", help="The base directory to search for psds/asds"
    )
    parser.add_argument(
        "--asds", help="The asds to pass in, dict of ifo to asd file name"
    )
    parser.add_argument(
        "--psds", help="The psds to pass in, dict of ifo to psd file name"
    )
    parser.add_argument(
        "--psd_low_freq_cutoff", help="The minimum psd frequency to read in"
    )
    parser.add_argument(
        "--consistent_psd", help="True if the same psd should be used for all analyses"
    )
    parser.add_argument(
        "--marginalize_psd", help="True if should marginalize over entered psds"
    )
    parser.add_argument(
        "--calibration_file", help="The file from which to draw calibration instances"
    )
    parser.add_argument(
        "--marginalize_calibration",
        help="Whether to marginalize over all calibration instances",
    )
    parser.add_argument("--inj_wf", help="The waveform to use in SNR computation")
    parser.add_argument(
        "--variable_network",
        help="Whether to use a variable network,\
    determined by segments of availability",
    )
    parser.add_argument(
        "--ifos", action="append", help="The ifos to use in the analysis"
    )
    parser.add_argument(
        "--n_pool", help="The number of cpus to use in the pool for computation"
    )
    parser.add_argument(
        "--worker_memory_request",
        help="The amount of memory to request for SNR computation workers",
    )
    parser.add_argument(
        "--threshold", help="The SNR threshold to use for the efficiency computation"
    )
    parser.add_argument(
        "--duration",
        help="The amount of livetime to use in sensitive volume computation,\
    may be overridden",
    )
    parser.add_argument(
        "--post_marg_likel_file",
        help="A gstlal post_marg_likel file to use for segments",
    )
    parser.add_argument(
        "--chunk_dir",
        help="The directory where *.intermediate files may be found for segments",
    )
    parser.add_argument(
        "--find_max_dist",
        help="pass true if you wish for the code to automatically decrease \
    distance to an observability threshold (set by safety_snr)",
    )
    parser.add_argument(
        "--safety_snr",
        default="3",
        help="For auto-decreasing length - more heterogeneous populations \
    may need lower values",
    )
    opts = parser.parse_args()

    csv_is_empty = True
    if "Checkpoint_SNR.csv" in os.listdir(opts.working_dir):
        df = pd.read_csv(os.path.join(opts.working_dir, "Checkpoint_SNR.csv"))
        csv_is_empty = df.empty
        if not csv_is_empty:
            population = workflowutils.interpret_config_args(
                opts.__dict__,
                overwrite_read=os.path.join(opts.working_dir, "Checkpoint_SNR.csv"),
            )
    if csv_is_empty:
        population = workflowutils.interpret_config_args(
            opts.__dict__,
            overwrite_read=os.path.join(
                opts.working_dir, "Clean_Separated_Population.csv"
            ),
        )
    population.working_dir = opts.working_dir
    population.outname = "Completed_SNR"
    inj_wf = opts.inj_wf
    print(f"Computation with {inj_wf} waveform")
    ifos = opts.ifos
    print(f"Network in use is: {ifos}")
    variable_network = opts.variable_network
    print(f"Computation with variable network is {variable_network}")

    snr_keys = [
        "snr_fmin",
        "snr_fmax",
        "snr_delta_t",
        "snr_delta_f",
        "snr_benchmark",
        "snr_cal_snr_method",
    ]
    snr_args = {}
    for key in snr_keys:
        if key in opts.__dict__.keys():
            # hacky map to the kwargs expected by calc_snrs
            snr_args[key[4:]] = opts.__dict__[key]
    population.calc_snrs(inj_wf, **snr_args)
    population.save_population()
