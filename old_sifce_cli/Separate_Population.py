import configargparse as cfgarg

from sifce import workflowutils


def main():
    """

    Takes in a starting directory and number of workers to split into, generates n sub-populations
    """
    parser = cfgarg.ArgumentParser()
    parser.add_argument(
        "--working-dir", type=str, help="The directory where the worker is located"
    )
    parser.add_argument("--n-pool", type=int, help="the number of pooled jobs")
    parser.add_argument(
        "--csv-name",
        type=str,
        default="Generated_Population.csv",
        help="the name of the csv to read in",
    )
    opts = parser.parse_args()

    workflowutils.split_population(
        opts.working_dir, opts.n_pool, csv_name=opts.csv_name
    )
