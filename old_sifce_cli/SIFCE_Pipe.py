import configparser
import os
import shutil
from shutil import which

import configargparse as cfgarg
from glue import pipeline


def main():
    """

    Constructor for the pipeline, takes in config
    """
    parser = cfgarg.ArgumentParser(
        config_file_parser_class=cfgarg.ConfigparserConfigFileParser
    )
    parser.add_argument(
        "-c", required=True, is_config_file=True, help="The config file"
    )
    parser.add_argument(
        "--ligo_user_name",
        type=str,
        default=os.environ["LIGO_USER_NAME"],
        help="LIGO User Name for condor",
    )
    parser.add_argument(
        "--ligo_accounting",
        type=str,
        default=os.environ["LIGO_ACCOUNTING"],
        help="LIGO Accounting for condor",
    )
    parser.add_argument(
        "--rundir", help="The directory to construct and produce analysis in"
    )
    parser.add_argument("--outname", help="The name of the analysis output")
    parser.add_argument(
        "--ninj", help="The number of injections in the simulated population"
    )
    parser.add_argument(
        "--prior_dict", help="The dictionary to construct a bilby prior dict with"
    )
    parser.add_argument("--cosmo_model", help="The cosmology model to use")
    parser.add_argument(
        "--read_population",
        default=None,
        help="Optionally, pass in a pre-generated population",
    )
    parser.add_argument(
        "--grid_param",
        help="The parameter to grid in, None if no grid,\
    redshift or luminosity_distance",
    )
    parser.add_argument(
        "--num_grid_bins", help="The number of grid elements in the distance grid"
    )
    parser.add_argument(
        "--psd_basedir", help="The base directory to search for psds/asds"
    )
    parser.add_argument(
        "--asds", help="The asds to pass in, dict of ifo to asd file name"
    )
    parser.add_argument(
        "--psds", help="The psds to pass in, dict of ifo to psd file name"
    )
    parser.add_argument(
        "--psd_low_freq_cutoff", help="The minimum psd frequency to read in"
    )
    parser.add_argument(
        "--consistent_psd", help="True if the same psd should be used for all analyses"
    )
    parser.add_argument(
        "--marginalize_psd", help="True if should marginalize over entered psds"
    )
    parser.add_argument(
        "--calibration_file", help="The file from which to draw calibration instances"
    )
    parser.add_argument(
        "--marginalize_calibration",
        help="Whether to marginalize over all calibration instances",
    )
    parser.add_argument("--inj_wf", help="The waveform to use in SNR computation")
    parser.add_argument(
        "--variable_network",
        help="Whether to use a variable network,\
    determined by segments of availability",
    )
    parser.add_argument(
        "--ifos", action="append", help="The ifos to use in the analysis"
    )
    parser.add_argument(
        "--n_pool", help="The number of cpus to use in the pool for computation"
    )
    parser.add_argument(
        "--worker_memory_request",
        help="The amount of memory to request for SNR computation workers",
    )
    parser.add_argument(
        "--threshold", help="The SNR threshold to use for the efficiency computation"
    )
    parser.add_argument(
        "--duration",
        help="The amount of livetime to use in sensitive volume computation,\
    may be overridden",
    )
    parser.add_argument(
        "--post_marg_likel_file",
        help="A gstlal post_marg_likel file to use for segments",
    )
    parser.add_argument(
        "--chunk_dir",
        help="The directory where *.intermediate files may be found for segments",
    )
    parser.add_argument(
        "--find_max_dist",
        help="pass true if you wish for the code to automatically decrease \
    distance to an observability threshold (set by safety_snr)",
    )
    parser.add_argument(
        "--safety_snr",
        default="3",
        help="For auto-decreasing length - more heterogeneous populations \
    may need lower values",
    )
    opts = parser.parse_args()

    rundir = opts.rundir
    n_pool = int(opts.n_pool)

    ligo_accounting_group = opts.ligo_accounting
    ligo_accounting_user = opts.ligo_user_name

    assert not os.path.isdir(rundir)
    os.mkdir(rundir)
    for i in range(n_pool):
        os.mkdir(os.path.join(rundir, f"worker_{i}"))
        os.mkdir(os.path.join(rundir, f"worker_{i}", "logs"))

    config_name = opts.c.split("/")[-1]
    config_in_rundir = os.path.join(rundir, config_name)

    config_dict = opts.__dict__
    config_dict.pop("c")
    config_dict.pop("ligo_user_name")
    config_dict.pop("ligo_accounting")

    ifos = "["
    for ifo in config_dict["ifos"]:
        ifos += f"{ifo},"
    ifos = ifos[:-1] + "]"
    config_dict["ifos"] = ifos

    for key, value in config_dict.items():
        config_dict[key] = str(value)
    parser = configparser.ConfigParser(defaults=config_dict)

    with open(config_in_rundir, "w") as f:
        parser.write(f)

    injection_dag = pipeline.CondorDAG(
        log=os.path.join(rundir, "dag_injections_pipe_daglog.log")
    )
    injection_dag.set_dag_file(os.path.join(rundir, "dag_injections_pipe"))

    generator_exe = which("generate_population")
    generator_job = pipeline.CondorDAGJob(universe="vanilla", executable=generator_exe)
    generator_job.set_log_file(os.path.join(rundir, "generator.log"))
    generator_job.set_stdout_file(os.path.join(rundir, "generator.out"))
    generator_job.set_stderr_file(os.path.join(rundir, "generator.err"))
    generator_job.add_condor_cmd("accounting_group", ligo_accounting_group)
    generator_job.add_condor_cmd("accounting_group_user", ligo_accounting_user)
    generator_job.add_condor_cmd("request_memory", "12 Gb")
    generator_job.add_condor_cmd("request_disk", "1 Gb")
    generator_job.add_condor_cmd("notification", "never")
    generator_job.add_condor_cmd("initialdir", rundir)
    generator_args = f" -c {config_in_rundir}"
    generator_job.add_arg(generator_args)
    generator_job.set_sub_file(os.path.join(rundir, "generator.sub"))
    generator_node = generator_job.create_node()
    generator_node.set_retry(5)

    separator_exe = which("separate_population")
    separator_job = pipeline.CondorDAGJob(universe="vanilla", executable=separator_exe)
    separator_job.set_log_file(os.path.join(rundir, "separator.log"))
    separator_job.set_stdout_file(os.path.join(rundir, "separator.out"))
    separator_job.set_stderr_file(os.path.join(rundir, "separator.err"))
    separator_job.add_condor_cmd("accounting_group", ligo_accounting_group)
    separator_job.add_condor_cmd("accounting_group_user", ligo_accounting_user)
    separator_job.add_condor_cmd("request_memory", "6 Gb")
    separator_job.add_condor_cmd("request_disk", "1 Gb")
    separator_job.add_condor_cmd("notification", "never")
    separator_job.add_condor_cmd("initialdir", rundir)
    separator_args = f" --working-dir {rundir} --n-pool {n_pool}"
    separator_job.add_arg(separator_args)
    separator_job.set_sub_file(os.path.join(rundir, "separator.sub"))
    separator_node = separator_job.create_node()
    separator_node.add_parent(generator_node)
    separator_node.set_retry(5)

    worker_exe = which("worker_snr")
    worker_job = pipeline.CondorDAGJob(universe="vanilla", executable=worker_exe)
    worker_job.set_log_file(
        os.path.join(
            rundir, "worker_$(MACROID)", "logs", "injection_$(MACROID)_$(cluster).log"
        )
    )
    worker_job.set_stdout_file(
        os.path.join(
            rundir, "worker_$(MACROID)", "logs", "injection_$(MACROID)_$(cluster).out"
        )
    )
    worker_job.set_stderr_file(
        os.path.join(
            rundir, "worker_$(MACROID)", "logs", "injection_$(MACROID)_$(cluster).err"
        )
    )
    worker_job.add_condor_cmd("accounting_group", ligo_accounting_group)
    worker_job.add_condor_cmd("accounting_group_user", ligo_accounting_user)
    worker_job.add_condor_cmd("request_memory", opts.worker_memory_request)
    worker_job.add_condor_cmd("request_disk", "1 Gb")
    worker_job.add_condor_cmd("notification", "never")
    worker_job.add_condor_cmd("checkpoint_exit_code", "83")
    worker_job.add_condor_cmd("+WantFTOnCheckpoint", "True")
    worker_job.add_condor_cmd("initialdir", os.path.join(rundir, "worker_$(MACROID)"))
    worker_args = f" --working-dir {os.path.join(rundir,'worker_$(MACROID)')}"
    worker_args += f" -c {os.path.join(rundir,config_name)}"
    worker_job.add_arg(worker_args)
    worker_job._CondorJob__queue = 1
    worker_job.set_sub_file(os.path.join(rundir, "injections_worker.sub"))
    worker_nodes = []
    for worker_iter in range(n_pool):
        worker_node = pipeline.CondorDAGNode(worker_job)
        worker_node.add_parent(separator_node)
        worker_node.add_macro("MACROID", worker_iter)
        worker_node.set_retry(5)
        injection_dag.add_node(worker_node)
        worker_nodes += [worker_node]

    combiner_exe = which("combine_recovery")
    combiner_job = pipeline.CondorDAGJob(universe="vanilla", executable=combiner_exe)
    combiner_job.set_log_file(os.path.join(rundir, "combiner.log"))
    combiner_job.set_stdout_file(os.path.join(rundir, "combiner.out"))
    combiner_job.set_stderr_file(os.path.join(rundir, "combiner.err"))
    combiner_job.add_condor_cmd("accounting_group", ligo_accounting_group)
    combiner_job.add_condor_cmd("accounting_group_user", ligo_accounting_user)
    combiner_job.add_condor_cmd("request_memory", "12 Gb")
    combiner_job.add_condor_cmd("request_disk", "1 Gb")
    combiner_job.add_condor_cmd("notification", "never")
    combiner_job.add_condor_cmd("initialdir", rundir)
    combiner_args = f" -c {config_in_rundir}"
    combiner_job.add_arg(combiner_args)
    combiner_job.set_sub_file(os.path.join(rundir, "combiner.sub"))
    combiner_node = combiner_job.create_node()
    for worker_node in worker_nodes:
        combiner_node.add_parent(worker_node)
    combiner_node.set_retry(5)

    efficiency_exe = which("compute_statistics")
    efficiency_job = pipeline.CondorDAGJob(
        universe="vanilla", executable=efficiency_exe
    )
    efficiency_job.set_log_file(os.path.join(rundir, "efficiency.log"))
    efficiency_job.set_stdout_file(os.path.join(rundir, "efficiency.out"))
    efficiency_job.set_stderr_file(os.path.join(rundir, "efficiency.err"))
    efficiency_job.add_condor_cmd("accounting_group", ligo_accounting_group)
    efficiency_job.add_condor_cmd("accounting_group_user", ligo_accounting_user)
    efficiency_job.add_condor_cmd("request_memory", "3 Gb")
    efficiency_job.add_condor_cmd("request_disk", "1 Gb")
    efficiency_job.add_condor_cmd("notification", "never")
    efficiency_job.add_condor_cmd("initialdir", rundir)
    efficiency_args = f" -c {config_in_rundir}"
    efficiency_job.add_arg(efficiency_args)
    efficiency_job.set_sub_file(os.path.join(rundir, "efficiency.sub"))
    efficiency_node = efficiency_job.create_node()
    efficiency_node.add_parent(combiner_node)
    efficiency_node.set_retry(5)

    plot_exe = which("sifce_efficiency_plot")
    plot_job = pipeline.CondorDAGJob(universe="vanilla", executable=plot_exe)
    plot_job.set_log_file(os.path.join(rundir, "plot.log"))
    plot_job.set_stdout_file(os.path.join(rundir, "plot.out"))
    plot_job.set_stderr_file(os.path.join(rundir, "plot.err"))
    plot_job.add_condor_cmd("accounting_group", ligo_accounting_group)
    plot_job.add_condor_cmd("accounting_group_user", ligo_accounting_user)
    plot_job.add_condor_cmd("request_memory", "3 Gb")
    plot_job.add_condor_cmd("request_disk", "1 Gb")
    plot_job.add_condor_cmd("notification", "never")
    plot_job.add_condor_cmd("initialdir", rundir)
    plot_args = f" -c {config_in_rundir}"
    plot_job.add_arg(plot_args)
    plot_job.set_sub_file(os.path.join(rundir, "plot.sub"))
    plot_node = plot_job.create_node()
    plot_node.add_parent(combiner_node)
    plot_node.set_retry(5)

    injection_dag.add_node(generator_node)
    injection_dag.add_node(separator_node)
    injection_dag.add_node(combiner_node)
    injection_dag.add_node(efficiency_node)
    injection_dag.add_node(plot_node)
    injection_dag.write_sub_files()
    injection_dag.write_dag()

    os.system(f"condor_submit_dag {os.path.join(rundir, 'dag_injections_pipe.dag')}")
