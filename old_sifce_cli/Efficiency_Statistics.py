import ast
import os

import configargparse as cfgarg
import pandas as pd
from astropy import cosmology

from sifce import cosmoutils, ligoutils, workflowutils


def VT_sens_df(population_df, duration=1, cosmo_model=cosmology.Planck15, threshold=11):
    """

    Gets VT Sens for a given df, assuming equal weights

    Inputs:
    -------------
    population_df - a dataframe of complete population with SNR
    duration - the nominal duration of the run in years
    cosmo_model - a cosmology model to use for VC calculations
    threshold - an optimal SNR threshold to apply

    Returns:
    -----------
    VT_sens - The sensitive volume for the data frame
    """
    dLs = population_df["luminosity_distance"].values
    opt_snrs = population_df["opt_snr"].values
    efficiency_dL = cosmoutils.compute_efficiency_dL(opt_snrs, dLs, threshold=threshold)
    VT_sens_integral = cosmoutils.compute_VTsens(
        efficiency_dL, cosmo_model=cosmo_model, years=duration
    )
    return VT_sens_integral[-1]


def result_by_calibration(
    population_df, duration=1, cosmo_model=cosmology.Planck15, threshold=11
):
    """
    Gets VT Sens for each calibration

    Inputs:
    -------------
    population_df - a dataframe of complete population with SNR
    duration - the nominal duration of the run in years
    cosmo_model - a cosmology model to use for VC calculations
    threshold - an optimal SNR threshold to apply

    Returns:
    -----------
    dict_VT - a dictionary of sensitive volume by calibration index
    """
    split_df_dict = {k: x for k, x in population_df.groupby(population_df["cal_idx"])}
    dict_VT = {}
    for cal_idx, df in split_df_dict.items():
        if df.shape[0] >= 2:
            dLs = df["luminosity_distance"].values
            opt_snrs = df["opt_snr"].values
            efficiency_dL = cosmoutils.compute_efficiency_dL(
                opt_snrs, dLs, threshold=threshold
            )
            VT_sens_integral = cosmoutils.compute_VTsens(
                efficiency_dL, cosmo_model=cosmo_model, years=duration
            )
            VT_final = VT_sens_integral[-1]
            dict_VT[cal_idx] = VT_final.value
    return dict_VT


def main():
    """
    Takes in config, computes efficiency for various calibrations, writes a file of calibration
    for all indices, and if marginalizing = True also prints base case vs marginalizing
    """
    parser = cfgarg.ArgumentParser()
    parser.add_argument(
        "-c", required=True, is_config_file=True, help="The config file"
    )
    parser.add_argument(
        "--rundir", help="The directory to construct and produce analysis in"
    )
    parser.add_argument("--outname", help="The name of the analysis output")
    parser.add_argument(
        "--ninj", help="The number of injections in the simulated population"
    )
    parser.add_argument(
        "--prior_dict", help="The dictionary to construct a bilby prior dict with"
    )
    parser.add_argument("--cosmo_model", help="The cosmology model to use")
    parser.add_argument(
        "--read_population",
        default=None,
        help="Optionally, pass in a pre-generated population",
    )
    parser.add_argument(
        "--grid_param",
        help="The parameter to grid in, None if no grid,\
    redshift or luminosity_distance",
    )
    parser.add_argument(
        "--num_grid_bins", help="The number of grid elements in the distance grid"
    )
    parser.add_argument(
        "--psd_basedir", help="The base directory to search for psds/asds"
    )
    parser.add_argument(
        "--asds", help="The asds to pass in, dict of ifo to asd file name"
    )
    parser.add_argument(
        "--psds", help="The psds to pass in, dict of ifo to psd file name"
    )
    parser.add_argument(
        "--psd_low_freq_cutoff", help="The minimum psd frequency to read in"
    )
    parser.add_argument(
        "--consistent_psd", help="True if the same psd should be used for all analyses"
    )
    parser.add_argument(
        "--marginalize_psd", help="True if should marginalize over entered psds"
    )
    parser.add_argument(
        "--calibration_file", help="The file from which to draw calibration instances"
    )
    parser.add_argument(
        "--marginalize_calibration",
        help="Whether to marginalize over all calibration instances",
    )
    parser.add_argument("--inj_wf", help="The waveform to use in SNR computation")
    parser.add_argument(
        "--variable_network",
        help="Whether to use a variable network,\
    determined by segments of availability",
    )
    parser.add_argument(
        "--ifos", action="append", help="The ifos to use in the analysis"
    )
    parser.add_argument(
        "--n_pool", help="The number of cpus to use in the pool for computation"
    )
    parser.add_argument(
        "--worker_memory_request",
        help="The amount of memory to request for SNR computation workers",
    )
    parser.add_argument(
        "--threshold", help="The SNR threshold to use for the efficiency computation"
    )
    parser.add_argument(
        "--duration",
        help="The amount of live time to use in sensitive volume computation,\
    may be overridden",
    )
    parser.add_argument(
        "--post_marg_likel_file",
        help="A gstlal post_marg_likel file to use for segments",
    )
    parser.add_argument(
        "--chunk_dir",
        help="The directory where *.intermediate files may be found for segments",
    )
    parser.add_argument(
        "--find_max_dist",
        help="pass true if you wish for the code to automatically decrease \
    distance to an observability threshold (set by safety_snr)",
    )
    parser.add_argument(
        "--safety_snr",
        default="3",
        help="For auto-decreasing length - more heterogeneous populations \
    may need lower values",
    )
    opts = parser.parse_args()

    population = workflowutils.interpret_config_args(
        opts.__dict__, overwrite_read=os.path.join(opts.rundir, opts.outname + ".csv")
    )

    if population.seg_list_dict is not None:
        # override if you have a segment list, and convert to 1/yr
        if population.variable_network:
            # if variable network, we are injecting over the entire possible time span
            # and have limited efficiency accordingly
            # thus apples to apples requires that the live time be the entire injection segment
            duration = (
                (
                    population.pop_distro["tc"].maximum
                    - population.pop_distro["tc"].minimum
                )
                / 60
                / 60
                / 24
                / 365.25
            )
        else:
            # if not, then just make the duration the total duration of the chunks
            live_segments = population.seg_list_dict.union(
                population.seg_list_dict.keys()
            )
            duration = (
                ligoutils.get_livetime_from_segment_list(live_segments)
                / 60
                / 60
                / 24
                / 365.25
            )
    else:
        duration = float(opts.duration)

    dict_VT = result_by_calibration(
        population.population_df,
        cosmo_model=eval(opts.cosmo_model),
        duration=float(opts.duration),
        threshold=float(opts.threshold),
    )

    VT_df = pd.DataFrame.from_dict(dict_VT, orient="index")
    VT_df.index.name = "Calibration Index"
    VT_df.to_csv(os.path.join(opts.rundir, opts.outname + "_VT_by_cal_idx.csv"))

    marginalizing = ast.literal_eval(opts.marginalize_calibration)
    margcase = None

    uncalibrated = population.population_df.loc[
        population.population_df["cal_idx"] == -1
    ]
    basecase = VT_sens_df(
        uncalibrated,
        cosmo_model=eval(opts.cosmo_model),
        duration=duration,
        threshold=float(opts.threshold),
    )

    if (
        marginalizing
        and opts.calibration_file != "None"
        and opts.calibration_file is not None
    ):
        calibrated = population.population_df.loc[
            population.population_df["cal_idx"] != -1
        ]
        margcase = VT_sens_df(
            calibrated,
            cosmo_model=eval(opts.cosmo_model),
            duration=duration,
            threshold=float(opts.threshold),
        )
    with open("Marginalizing_Results.txt", "w") as f:
        f.write(f"Live time used in computation: {duration} years\n")
        f.write(f"No Calibration Sensitive Volume Element: {basecase}\n")
        if margcase is not None:
            f.write(f"Marginalized Sensitive Volume Element: {margcase}\n")
