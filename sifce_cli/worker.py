import argparse
import logging
import os

import bilby
import numpy as np

from sifce import parser as sparse
from sifce import tv

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    # Stuff to pass to workers so they know where to read and write
    worker_parser = argparse.ArgumentParser()
    worker_parser.add_argument(
        "config",
        type=str,
        help="The config file for this run",
    )
    worker_parser.add_argument(
        "--worker-output-dir",
        type=str,
        help="The output directory name for this worker",
    )
    worker_parser.add_argument(
        "--worker-output-label",
        type=str,
        default="simset_df",
        help="If a special output label should be used, pass this string. Use .gzip suffix",
    )
    worker_arguments = worker_parser.parse_args()

    # Standard pipeline stuff
    arguments, parser = sparse.parse(config_file=worker_arguments.config)
    simset = sparse.construct_simulationset_from_parsed_arguments(arguments)

    n_int = arguments.num_int_samples
    simset.make_clean_simulations_dataframe()
    simset.sample_from_sampling_distribution(n_int)
    simset.fill_out_snr(
        n_sky=arguments.num_sky_scatter,
        n_scale=arguments.num_distance_scale,
        benchmark=False,
    )

    # Setup the timevolume object and compute vt_sens_dict
    timevol = tv.TimeVolume(simulation=simset)
    vt_sens_dict, det_function_info, n_eff_dict, _ = timevol.get_sensitive_volume(
        importance_sampling=(arguments.sampling_method == "Importance")
    )

    # Write the data of the process to the output directory
    simset.write_dataframe_and_distribution(
        worker_arguments.worker_output_dir,
        simulations_dataframe_name=f"{worker_arguments.worker_output_label}_simulations.gzip",
        detection_dataframe_name=f"{worker_arguments.worker_output_label}_detection.gzip",
    )
    # Write the computed vt
    with open(
        os.path.join(worker_arguments.worker_output_dir, "computed_vt.txt"), "w"
    ) as f:
        for key, val in vt_sens_dict.items():
            f.write(f"Detection Function Name: {det_function_info[0][key]}\n")
            f.write(f"Detection Function Kwargs: {det_function_info[1][key]}\n")
            f.write(f"Efficiency of Weighting: {n_eff_dict[key]}\n")
            f.write(f"Sensitive Volume Computed: {val}\n")
