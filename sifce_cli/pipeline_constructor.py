import argparse
import logging
import os
import shutil

from glue import pipeline

from sifce import parser as sparse

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    arguments, parser = sparse.parse()

    os.makedirs(arguments.pipeline_dir)

    for key in [
        "sampling_prior_file",
        "target_prior_file",
        "segments_file",
        "psds_file",
        "calibrations_file",
    ]:
        original_file_path = arguments.__dict__[key]
        new_file_path = os.path.join(
            arguments.pipeline_dir, original_file_path.split("/")[-1]
        )
        shutil.copy(original_file_path, new_file_path)
        arguments.__dict__[key] = new_file_path

    pipeline_config_path = os.path.join(
        arguments.pipeline_dir, "sifce_pipeline_config.cfg"
    )
    sparse.write_altered_config(
        arguments=arguments, parser=parser, output_path=pipeline_config_path
    )

    dag = pipeline.CondorDAG(
        log=os.path.join(arguments.pipeline_dir, "dag_sifce_pipe.log")
    )
    dag.set_dag_file(os.path.join(arguments.pipeline_dir, "dag_sifce_pipe"))

    if arguments.ligo_accounting is None:
        assert "LIGO_ACCOUNTING" in os.environ.keys()
        ligo_accounting = os.environ["LIGO_ACCOUNTING"]
    else:
        ligo_accounting = arguments.ligo_accounting

    if arguments.ligo_user_name is None:
        assert "LIGO_USER_NAME" in os.environ.keys()
        ligo_user_name = os.environ["LIGO_USER_NAME"]
    else:
        ligo_user_name = arguments.ligo_user_name

    def standard_job_constructor(
        executable_name,
        job_name,
        job_arguments,
        universe="vanilla",
        user_name=ligo_user_name,
        accounting=ligo_accounting,
        request_memory="1 Gb",
        request_disk="1 Gb",
        initialdir=arguments.pipeline_dir,
        logdir=os.path.join(arguments.pipeline_dir, "logs"),
        extra_cmds=None,
    ):
        # Fetch the exe path
        job_exe = shutil.which(executable_name)

        # Setup the dagjob
        job = pipeline.CondorDAGJob(universe="vanilla", executable=job_exe)

        # Set the log/err/out file destinations
        job.set_log_file(os.path.join(logdir, f"{job_name}_$(cluster).log"))
        job.set_stdout_file(os.path.join(logdir, f"{job_name}_$(cluster).out"))
        job.set_stderr_file(os.path.join(logdir, f"{job_name}_$(cluster).err"))

        # Setup igwn info
        job.add_condor_cmd("accounting_group", accounting)
        job.add_condor_cmd("accounting_group_user", user_name)

        # Setup job requests
        job.add_condor_cmd("request_memory", request_memory)
        job.add_condor_cmd("request_disk", request_disk)

        # Don't get notified
        # TODO - get notified?
        job.add_condor_cmd("notification", "never")

        # Set the initialdir
        job.add_condor_cmd("initialdir", initialdir)

        # I have no idea what this does but apparently it was needed at some point in the past?
        job._CondorJob__queue = 1

        # Add the arguments
        job.add_arg(job_arguments)

        # Set the subfile
        job.set_sub_file(os.path.join(arguments.pipeline_dir, f"{job_name}.sub"))

        # If there are extra lines to add do so here
        if extra_cmds is not None:
            for cmd in extra_cmds:
                job.add_condor_cmd(cmd[0], cmd[1])

        return job

    def standard_node_constructor(job, parents=[], macros=[], retry=5):
        # Basic setup
        job_node = pipeline.CondorDAGNode(job)
        # Add parents iteratively
        for parent in parents:
            job_node.add_parent(parent)
        # Add any macros
        for macro in macros:
            job_node.add_macro(macro[0], macro[1])
        # Set the retry number
        job_node.set_retry(retry)
        # Add to the dag
        dag.add_node(job_node)

        return job_node

    os.makedirs(os.path.join(arguments.pipeline_dir, "logs"))

    # Setup the tracer job - a single worker to give a sample, allowing the determination of a proper dmax
    tracer_job = standard_job_constructor(
        "sifce_worker",
        "tracer_job",
        f" {pipeline_config_path} --worker-output-dir {arguments.pipeline_dir} --worker-output-label 'trace_job'",
        request_memory=f"{arguments.request_memory} Gb",
    )
    # Setup the modify job to apply that dmax to the configs
    tracer_modify_job = standard_job_constructor(
        "sifce_distance_adjust",
        "tracer_modify",
        f" {pipeline_config_path}",
        request_memory="4 Gb",
    )
    # Setup the main worker jobs - some complicated use of macros
    worker_job = standard_job_constructor(
        "sifce_worker",
        "worker_job",
        f" {pipeline_config_path} \
            --worker-output-dir {os.path.join(arguments.pipeline_dir, 'worker_$(MACROID)', 'output')}",
        request_memory=f"{arguments.request_memory} Gb",
        initialdir=os.path.join(arguments.pipeline_dir, "worker_$(MACROID)"),
        logdir=os.path.join(arguments.pipeline_dir, "worker_$(MACROID)", "logs"),
        extra_cmds=[("checkpoint_exit_code", "83"), ("+WantFTOnCheckpoint", "True")],
    )
    # Setup the final statistics job
    run_compile_job = standard_job_constructor(
        "sifce_run_results",
        "compile_job",
        f" {pipeline_config_path}",
        request_memory="4 Gb",
    )

    # Setup the nodes for each job
    tracer_node = standard_node_constructor(
        tracer_job,
    )
    tracer_modify_node = standard_node_constructor(
        tracer_modify_job, parents=[tracer_node]
    )
    worker_nodes = []
    for worker_iter in range(arguments.number_workers):
        os.makedirs(
            os.path.join(arguments.pipeline_dir, f"worker_{worker_iter}", "logs")
        )
        worker_node = standard_node_constructor(
            worker_job,
            parents=[tracer_modify_node],
            macros=[("MACROID", worker_iter)],
        )
        worker_nodes += [worker_node]
    _ = standard_node_constructor(run_compile_job, parents=worker_nodes)

    # Write sub files and dag
    dag.write_sub_files()
    dag.write_dag()

    # Automatically submit
    os.system(
        f"condor_submit_dag {os.path.join(arguments.pipeline_dir, 'dag_sifce_pipe.dag')}"
    )
