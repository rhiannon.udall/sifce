import copy
import logging
import os

import astropy.units as u
import numpy as np
from astropy.cosmology import Cosmology, z_at_value

import sifce.parser as sparse

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    arguments, parser = sparse.parse()

    simset = sparse.construct_simulationset_from_parsed_arguments(arguments)
    simset.load_simset_dataframes(
        simulations_dataframe_path=os.path.join(
            arguments.pipeline_dir, "trace_job_simulations.gzip"
        ),
        detection_dataframe_path=os.path.join(
            arguments.pipeline_dir, "trace_job_detection.gzip"
        ),
    )

    luminosity_distance_max = 0
    p_det_keys = [key for key in simset.detection_dataframe.keys() if "p_det" in key]
    for ii, key in enumerate(p_det_keys):
        test_df = simset.detection_dataframe[simset.detection_dataframe[key] > 0]
        luminosity_distance_max = np.maximum(
            luminosity_distance_max, test_df["luminosity_distance"].max()
        )

    redshift_max = z_at_value(
        simset.cosmology_model.luminosity_distance, luminosity_distance_max * u.Mpc
    ).value

    if "luminosity_distance" in simset.sampling_distribution.keys():
        simset.sampling_distribution[
            "luminosity_distance"
        ].maximum = luminosity_distance_max
        if isinstance(
            simset.sampling_distribution["luminosity_distance"].cosmology, Cosmology
        ):
            simset.sampling_distribution[
                "luminosity_distance"
            ].cosmology = simset.sampling_distribution[
                "luminosity_distance"
            ].cosmology.name
    elif "redshift" in simset.sampling_distribution.keys():
        simset.sampling_distribution["redshift"].maximum = redshift_max
        if isinstance(simset.sampling_distribution["redshift"].cosmology, Cosmology):
            simset.sampling_distribution[
                "redshift"
            ].cosmology = simset.sampling_distribution["redshift"].cosmology.name

    if "luminosity_distance" in simset.target_distribution.keys():
        simset.target_distribution[
            "luminosity_distance"
        ].maximum = luminosity_distance_max
        if isinstance(
            simset.target_distribution["luminosity_distance"].cosmology, Cosmology
        ):
            simset.target_distribution[
                "luminosity_distance"
            ].cosmology = simset.target_distribution[
                "luminosity_distance"
            ].cosmology.name
    elif "redshift" in simset.target_distribution.keys():
        simset.target_distribution["redshift"].maximum = redshift_max
        if isinstance(simset.target_distribution["redshift"].cosmology, Cosmology):
            simset.target_distribution[
                "redshift"
            ].cosmology = simset.target_distribution["redshift"].cosmology.name

    simset.sampling_distribution.to_file(
        arguments.pipeline_dir,
        arguments.sampling_prior_file.split("/")[-1].split(".")[0],
    )
    simset.target_distribution.to_file(
        arguments.pipeline_dir, arguments.target_prior_file.split("/")[-1].split(".")[0]
    )
