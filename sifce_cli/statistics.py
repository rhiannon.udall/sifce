import logging
import os

import numpy as np

import sifce.parser as sparse

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    arguments, parser = sparse.parse()
    simset = sparse.construct_simulationset_from_parsed_arguments(arguments)

    worker_dirs = [
        (x, os.path.join(arguments.pipeline_dir, x))
        for x in os.listdir(arguments.pipeline_dir)
        if "worker_" in x and "sub" not in x
    ]
    vts = np.empty((len(worker_dirs), len(simset.detection_functions)))
    for ii, wdir in enumerate(worker_dirs):
        name, wpath = wdir
        volume_file = os.path.join(wpath, "output", "computed_vt.txt")
        with open(volume_file, "r") as f:
            values_text = f.readlines()
        det_func_counter = 0
        if ii == 0:
            det_function_names = []
            det_function_kwargs = []
        for line in values_text:
            if ii == 0:
                if "Detection Function Name" in line:
                    det_function_names += [
                        line.replace("Detection Function Name: ", "")
                    ]
                if "Detection Function Kwargs:" in line:
                    det_function_kwargs += [
                        line.replace("Detection Function Kwargs:", "")
                    ]
            if "Sensitive Volume Computed" in line:
                vts[ii, det_func_counter] = line.split(":")[-1].strip().split()[0]
                det_func_counter += 1

    with open(os.path.join(arguments.pipeline_dir, "MeanVolume.txt"), "w") as f:
        for det_func_idx in range(vts.shape[1]):
            f.write(f"Detection Function Name: {det_function_names[det_func_idx]}")
            f.write(f"Detection Function Kwargs: {det_function_kwargs[det_func_idx]}")
            f.write(
                f"Mean Sensitive Volume Computed: {np.mean(vts[:, det_func_idx])}\n"
            )
            f.write(
                f"Variance was {np.var(vts[:, det_func_idx])} over {len(worker_dirs)} workers,\n"
            )
            f.write(
                f"Implying variance of {np.var(vts[:, det_func_idx]) / len(worker_dirs)} for the run\n"
            )
