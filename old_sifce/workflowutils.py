import ast
import copy
import os

import bilby
import numpy as np
from astropy import cosmology

from sifce import population as spop


def q_from_m1_m2(m1_m2_dict):
    # https://lscsoft.docs.ligo.org/bilby/prior.html#prior-constraints
    conv_dict = copy.copy(m1_m2_dict)
    conv_dict["mass_ratio"] = conv_dict["mass_2"] / conv_dict["mass_1"]
    return conv_dict


def interpret_config_args(opts, overwrite_read=None):
    """

    Interprets the configuration file, returns a population

    Inputs:
    ------------
    opts - the opts.__dict__ dictionary, where opts come from configargparse
    overwrite_read - if passed, overwrites the read argument of the config and reads in this population instead
    """
    # an obtuse way to safety proof against the presence of this element in the config
    if "calibration_file" in opts.keys():
        if opts["calibration_file"] != "None":
            calibration_file = opts["calibration_file"]
        else:
            calibration_file = None
    else:
        calibration_file = None
    if "read_population" in opts.keys():
        if opts["read_population"] != "None":
            read_population = opts["read_population"]
        else:
            read_population = None
    else:
        read_population = None
    if overwrite_read is not None:
        read_population = overwrite_read
    chunk_dir = opts["chunk_dir"]
    # grab a bilby prior dict and plug that in
    source_population = bilby.core.prior.PriorDict(
        dictionary=eval(opts["prior_dict"]), conversion_function=q_from_m1_m2
    )
    working_dir = opts["rundir"]
    outname = opts["outname"]
    if not os.path.isdir(working_dir):
        os.mkdir(working_dir)
    assert outname not in os.listdir(working_dir)
    ifos = opts["ifos"]
    variable_network = ast.literal_eval(opts["variable_network"])
    population = spop.Population(
        source_population=source_population,
        cosmo_model=eval(opts["cosmo_model"]),
        working_dir=working_dir,
        outname=outname,
        input_population=read_population,
        variable_network=variable_network,
        ifos=ifos,
    )
    if calibration_file is not None:
        population.set_cal_file(calibration_file)
    if chunk_dir != "None":
        population.set_ifo_segmentlist(chunk_dir)
    psd_load_keys = ["asds", "psds", "psd_low_freq_cutoff", "psd_delta_f", "psd_length"]
    psd_load_args = {}
    for key in psd_load_keys:
        if key in opts.keys():
            psd_load_args[key] = opts[key]
    for key, value in psd_load_args.items():
        # All psd args will be evaluatable,
        psd_load_args[key] = ast.literal_eval(value)
    psd_load_args["psd_basedir"] = opts["psd_basedir"]
    population.set_psds(**psd_load_args)
    return population


def split_population(rundir, n_pool, csv_name="Generated_Population.csv"):
    """
    Splits a population into n parts


    Inputs:
    ---------------
    rundir - the directory to find the splittable population in, also where worker directories will be written
    n_pool - the number of sub-populations to split into
    csv_name - the name of the population to read
    """
    population = spop.Population(input_population=os.path.join(rundir, csv_name))
    inj_per_pool = int(population.population_df.shape[0] / n_pool)
    for i in range(n_pool):
        if i < (n_pool - 1):
            sub_pop = population.population_df.iloc[
                i * inj_per_pool : inj_per_pool * (i + 1), :
            ]
        else:
            sub_pop = population.population_df.iloc[i * inj_per_pool :, :]
        sub_pop.index = np.arange(sub_pop.shape[0])
        sub_pop.to_csv(
            os.path.join(rundir, f"worker_{i}", "Clean_Separated_Population.csv")
        )
