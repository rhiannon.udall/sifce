import copy
import os
import signal
import sys
import time

import numpy as np
import pandas as pd
from astropy import cosmology
from astropy import units as u
from bilby.gw.conversion import transform_precessing_spins
from ligo.segments import segmentlistdict
from natsort import natsorted
from pycbc.detector import Detector
from pycbc.filter import make_frequency_series
from pycbc.waveform import fd_approximants, td_approximants

from sifce import cosmoutils, ligoutils, wfwraps


class Population:
    def __init__(
        self,
        source_population=None,
        cosmo_model=cosmology.Planck15,
        working_dir=None,
        input_population=None,
        outname=None,
        ifos=["H1", "L1", "V1"],
        variable_network=False,
    ):
        """

        Initializes a population

        Inputs:
        ---------
        source_population - a bilby PriorDict object used to draw a population
        cosmo_model - a cosmology model
        working_dir - the default directory for writing files
        input_population - optional, a csv to read
        in a pre-generated population data frame
        Don't use if you intend to generate a population (see below)
        outname - default output name for saved file
        ifos - the ifos to use in the computation, psds will be needed for each
        variable_network - Whether to use actual network
        uptime in computing sensitivity,
        requires a set of network segments to work with
        """
        if source_population is not None:
            self.pop_distro = source_population
        else:
            self.pop_distro = None
        self.param_labels = [
            "mass_1",
            "mass_2",
            "spin_1x",
            "spin_1y",
            "spin_1z",
            "spin_2x",
            "spin_2y",
            "spin_2z",
            "theta_jn",
            "luminosity_distance",
            "phase",
            "psi",
            "ra",
            "dec",
            "tc",
            "cal_idx",
            "redshift",
            "opt_snr",
        ]
        self.cosmo = cosmo_model
        if input_population is not None:
            self.population_df = pd.read_csv(input_population, index_col=0)
        else:
            self.population_df = None
        if working_dir is None:
            self.working_dir = os.getcwd()
        else:
            self.working_dir = working_dir
        self.outname = outname
        self.num_cals = None
        self.seg_list_dict = None
        self.ifos = ifos
        self.variable_network = variable_network
        self.population_psds_configured = False
        self.psd_dict = {}
        for ifo in self.ifos:
            self.psd_dict[ifo] = []
        self.num_psds = None

    def generate_population(self, size, grid_param=None, num_bins=100, benchmark=False):
        """
        Creates a population based on the input
        population prior and a few extra args

        Inputs:
        ---------
        size - the number of samples to draw from the population
        grid_param - the parameter to make a distance grid in. Options are:
            'redshift' - uniform in redshift grid
            'luminosity_distance' - uniform in dL grid
            None - draw from the prior
        num_bins - if gridding, how many bins in the grid
        """
        assert self.pop_distro is not None
        assert self.population_df is None
        if benchmark:
            start = time.time()
        samples = self.pop_distro.sample(size)
        (
            samples["iota"],
            samples["spin_1x"],
            samples["spin_1y"],
            samples["spin_1z"],
            samples["spin_2x"],
            samples["spin_2y"],
            samples["spin_2z"],
        ) = transform_precessing_spins(
            samples["theta_jn"],
            samples["phi_jl"],
            samples["tilt_1"],
            samples["tilt_2"],
            samples["phi_12"],
            samples["a_1"],
            samples["a_2"],
            samples["mass_1"],
            samples["mass_2"],
            20,
            samples["phase"],
        )

        if benchmark:
            print(
                "Time sampling with bilby:\
                  {} seconds".format(
                    time.time() - start
                )
            )

        # now handle distance / redshift, since it is special
        # first, a grid in distance
        if benchmark:
            start = time.time()
        if grid_param == "luminosity_distance":
            # still takes minimum and maximum from the bilby distribution
            minimum = self.pop_distro["luminosity_distance"].minimum
            maximum = self.pop_distro["luminosity_distance"].maximum
            # gets the correct bins
            distance_bins = np.linspace(minimum, maximum, num=num_bins)
            # check that the number of bins evenly
            # divides the number of samples
            assert size % len(distance_bins) == 0
            num_per_bin = int(size / len(distance_bins))
            # create dummy array
            distances = np.empty((size,))
            # populate evenly over bins
            for i, val in enumerate(distance_bins):
                distances[i * num_per_bin : num_per_bin * (i + 1)] = val

        # a grid in redshift
        elif grid_param == "redshift":
            # converts min and max from distribution then grids in that
            # and converts back for dL values
            # still takes minimum and maximum from the bilby distribution
            minimum = cosmology.z_at_value(
                self.cosmo.luminosity_distance,
                self.pop_distro["luminosity_distance"].minimum * u.Mpc,
            )
            maximum = cosmology.z_at_value(
                self.cosmo.luminosity_distance,
                self.pop_distro["luminosity_distance"].maximum * u.Mpc,
            )
            # make bins and perform checks as before
            z_bins = np.linspace(minimum, maximum, num=num_bins)
            distance_bins = self.cosmo.luminosity_distance(z_bins).value
            assert size % len(distance_bins) == 0
            num_per_bin = int(size / len(distance_bins))
            # populate with these bins
            distances = np.empty((size,))
            for i, val in enumerate(distance_bins):
                distances[i * num_per_bin : num_per_bin * (i + 1)] = val

        # Finally, the case where we are sampling the given distribution
        else:
            # Sample the input distrbiution
            # this is ok for large number of populations
            # but could fail badly for small injection sets
            # however, this does reflect a real population construction
            # so this is a "true" population (i.e. can be interpreted
            # without the efficiency curve)
            distances = samples["luminosity_distance"]
            # sort to facilitate redshifting
            distances = sorted(distances)
        redshifts = cosmoutils.compute_zvals(distances, cosmo_model=self.cosmo)

        if benchmark:
            print(
                "Time producing distances / redshifts\
                   : {} seconds".format(
                    time.time() - start
                )
            )

        # set up a data frame
        self.population_df = pd.DataFrame()
        # read in values from the samples we've constructed
        for param in self.param_labels:
            if param in samples.keys():
                self.population_df[param] = samples[param]
        # manually set the distances and redshifts we've computed

        self.population_df["luminosity_distance"] = distances
        self.population_df["redshift"] = redshifts

        # redshift masses
        self.population_df["mass_1"] = self.population_df.apply(
            lambda row: row["mass_1"] * (1 + row["redshift"]), axis=1
        )
        self.population_df["mass_2"] = self.population_df.apply(
            lambda row: row["mass_2"] * (1 + row["redshift"]), axis=1
        )
        # set up these columns which will be used later
        self.population_df["cal_idx"] = -1
        self.population_df["psd_idx"] = 0
        self.population_df["opt_snr"] = 0

    def check_max_observable(
        self,
        Inj_WF,
        safety_snr=3,
        fmin=11,
        fmax=2048,
        delta_t=1.0 / 4096.0,
        delta_f=0.25,
        cal_dict=None,
        benchmark=False,
        use_stored_wf=False,
        cal_snr_method="original",
        psds=None,
        scale=0.9,
    ):
        """
        Checks against max distance given to see if it satisfies the requirement that
        a) it shouldn't rail and
        b) if it's so high as to be totally unobservable

        Will raise an error if the distance rails, else will return a new max distance if the input was too large

        Inputs:
        Inj_WF - the waveform to use in the waveform generation
        fmin - the minimum frequency of integration
        fmax - the maximum frequency of integration
        delta_t - the delta_t to generate the waveform at
        delta_f - the delta_f to FFT (if necessary) or generate the waveform to
        cal_dict - a dictionary to potentially use
        in performing calibration adjustment
        scale - the factor by which to scale down distance if it's proving unobservable, higher makes it converge
        faster but risks overshoot
        """
        max_distance = self.pop_distro["luminosity_distance"].maximum
        unobservable = True
        if psds is None:
            psds = {}
            for ifo in self.ifos:
                psds[ifo] = self.psd_dict[ifo][0]
        while unobservable:
            # prepare a set of 10 test cases
            test_cases = self.pop_distro.sample(10)
            (
                test_cases["iota"],
                test_cases["spin_1x"],
                test_cases["spin_1y"],
                test_cases["spin_1z"],
                test_cases["spin_2x"],
                test_cases["spin_2y"],
                test_cases["spin_2z"],
            ) = transform_precessing_spins(
                test_cases["theta_jn"],
                test_cases["phi_jl"],
                test_cases["tilt_1"],
                test_cases["tilt_2"],
                test_cases["phi_12"],
                test_cases["a_1"],
                test_cases["a_2"],
                test_cases["mass_1"],
                test_cases["mass_2"],
                20,
                test_cases["phase"],
            )
            # put them at the current maximum distance
            test_cases["luminosity_distance"] = max_distance
            # experimentally determined hot spots at optimal inclination (face on)
            # psi and coa phase don't really matter
            test_cases["right_ascension"] = 0.746125
            test_cases["declination"] = 0.640079
            test_cases["opt_snr"] = 0
            # set up a data frame
            test_cases_df = pd.DataFrame()
            # read in values from the samples we've constructed
            for param in self.param_labels:
                if param in test_cases.keys():
                    test_cases_df[param] = test_cases[param]
            # check all of their SNRs
            for i in range(test_cases_df.shape[0]):
                test_cases_df.iloc[
                    i, test_cases_df.columns.get_loc("opt_snr")
                ] = self.calc_snr(
                    Inj_WF,
                    fmin=fmin,
                    fmax=fmax,
                    delta_t=delta_t,
                    delta_f=delta_f,
                    cal_dict={},
                    benchmark=benchmark,
                    use_stored_wf=False,
                    cal_snr_method=cal_snr_method,
                    psds=psds,
                    param_series=test_cases_df.iloc[i],
                )
                # if any are greater than 8, this risks railing, and should be prevented from occurring
                assert (
                    test_cases_df.iloc[i, test_cases_df.columns.get_loc("opt_snr")] < 8
                )
            # if not are greater than 3, there will be a gap of unobservable data,
            # so we should scale down the maximum distance
            if not (test_cases_df["opt_snr"] > safety_snr).any():
                max_distance *= scale
            # if one is now >4, we can choose this as a maximum distance
            else:
                unobservable = False
                self.pop_distro["luminosity_distance"].maximum = max_distance
                return max_distance

    def add_calibration_to_population(self, marginalizing=True):
        """
        Adds calibration indices to population, allowing for calibration study

        Inputs:
        ---------
        marginalizing - whether to do a marginalization study or index by index
            True - marginalize, i.e. randomly sample indices for comparison
            False - don't, generate a copy of the population for each index
        """
        assert self.num_cals is not None
        # catch if population_psds not configured
        assert self.population_psds_configured
        # create an image of the base population
        temp_df = copy.copy(self.population_df)
        # if marginalizing, one base case and
        # one case with random calibration instance
        if marginalizing:
            temp_df["cal_idx"] = np.random.randint(
                0, self.num_cals, size=self.population_df.shape[0]
            )
            self.population_df = pd.concat([self.population_df, temp_df]).sort_index(
                kind="merge"
            )
        # if not, base case and n cases,
        # each the same pop w/ different cal instance
        else:
            for i in range(self.num_cals):
                internal_df = copy.copy(temp_df)
                internal_df["cal_idx"] = i
                self.population_df = pd.concat(
                    [self.population_df, internal_df]
                ).sort_index(kind="merge")

    def add_alternate_psds_to_population(self, marginalizing=True):
        """
        Adds calibration indices to population, allowing for calibration study

        Inputs:
        ---------
        marginalizing - whether to do a marginalization study or index by index
            True - marginalize, i.e. randomly sample indices for comparison
            False - don't, generate a copy of the population for each index

        Modifies:
        ---------
        self.population_df['psd_idx'] - either edits in place or
        produces copies of the population with varying index
        self.population_psds_configured - a parameter to catch if
        you accidentally make population manipulations before setting psds
        this would be *bad*, since e.g. the same configuration analyzed with
        *both* varying calibration and varying psd would
        not be a valid comparison
        """
        assert self.num_psds is not None
        # create an image of the base population
        temp_df = copy.copy(self.population_df)
        # if marginalizing, randomize the indices
        # note unlike in calibrations,
        # we don't create a base case for marginalizing,
        # since that doesn't really make sense
        # (there shouldn't be any sense of a "preferred psd")
        # if you really want one, then make 2n psds,
        # where n is the marginalizing number of psds,
        # and then make the first n identical
        # more likely you may want to e.g.
        # compare 1000 welch psds with 1000 BW psds
        # in this case, just enter them all together in some order,
        # then later on compute VTSens conditioned on psd index > or < 1000
        # if you do that then remember to make sure
        # number of injections is sufficient!
        if marginalizing:
            self.population_df["psd_idx"] = np.random.randint(
                0, self.num_psds, size=self.population_df.shape[0]
            )
        # In the case where you are *not* marginalizing,
        # e.g. comparing one fiducial BW psd to one fiducial Welch psd
        # then do the same copy routine as for cals
        else:
            for i in range(self.num_psds):
                internal_df = copy.copy(temp_df)
                internal_df["psd_idx"] = i
                self.population_df = pd.concat(
                    [self.population_df, internal_df]
                ).sort_index(kind="merge")
        self.population_psds_configured = True

    def affirm_only_psd(self):
        """
        A method to confirm that psds are configured already,
        for when a single set of psds is used
        catches if psds not configured,
        or if multiple psds are configured
        (in which case should add to population,
        instead of calling this method)

        Modifies:
        -----------
        self.population_psds_configured to True
        """
        assert self.num_psds == 1
        self.population_psds_configured = True

    def checkpoint_wrapper(self, signum=None, frame=None):
        """
        A wrapper of save_population for calls by the checkpointing system

        Inputs:
        ---------------
        signum - standard signal.signal input, unused
        frame - standard signal.signal input, unused
        """
        self.save_population(csv_save_name="Checkpoint_SNR")
        sys.exit(83)

    def save_population(self, outdir=None, csv_save_name=None):
        """
        Saves the population in its current state to the file

        Inputs:
        --------
        outdir - optional directory to write to,
        if None defaults to Population.working_dir
        csv_save_name - optional csv name to save to,
        if None defaults to Population.outname
        """
        if outdir is None:
            outdir = self.working_dir
        if csv_save_name is None:
            csv_save_name = self.outname
        assert csv_save_name is not None
        # standard saving stuff
        csv_save_name = str(csv_save_name)
        outdir = str(outdir)
        outcsv = os.path.join(outdir, csv_save_name + ".csv")
        self.population_df.to_csv(outcsv)

    def set_psds(
        self,
        psds=None,
        asds=None,
        psd_basedir="/home/richard.udall/Refactor_SIFCE/psd_standard_dir/from_dcc_single",
        psd_delta_f=0.25,
        psd_length=16384,
        psd_low_freq_cutoff=10,
    ):
        """
        Sets the psds for use in SNR calculations

        Inputs:
        ------------
        psds - a dictionary of {ifo:psd_dir} for psd files to read in
        asds - a dictionary of {ifo:asd_dir} for asd_files to read in
        naturally, each ifo should have only one of either an asd or a psd
        psd_dir or asd_dir will be directories which will
        have one or many psds/asds in them
        each will be read in and assigned an index
        they will be sorted by natsort
        https://en.wikipedia.org/wiki/Natural_sort_order
        note there must be the same number of psds/asds for each detector
        if there is a discrepance in the number you can just make
        copies to fill out order, but of course be
        careful to keep numbers balanced...
        psd_basedir - the overarching directory
        psd_delta_f - the delta_f to interpolate the psds to
        psd_length - the number of elements to include in the psd,
        sets the fmax with psd_delta_f
        psd_low_freq_cutoff - below this frequency, sets values to 1 for safety
        """
        if psds is not None:
            for ifo, psdpath in psds.items():
                # get psd files within the given psd base path
                # make sure only psds are in this dir!
                psd_files = [
                    os.path.join(psd_basedir, psdpath, x)
                    for x in os.listdir(os.path.join(psd_basedir, psdpath))
                ]
                # natsort them for use readability / reproducibility of indices
                psd_files = natsorted(psd_files, key=lambda x: x.split("/")[-1])
                # convert psds into pycbc fseries for use
                for fname in psd_files:
                    psd = wfwraps.read_psd_from_txt(
                        fname,
                        delta_f=psd_delta_f,
                        length=psd_length,
                        low_freq_cutoff=psd_low_freq_cutoff,
                        basedir=psd_basedir,
                    )
                self.psd_dict[ifo] += [psd]
                # set the number of psds in use for later if not already set
                if self.num_psds is None:
                    self.num_psds = len(self.psd_dict[ifo])
                # else assertion to make sure number of psds is set correctly
                else:
                    assert self.num_psds == len(self.psd_dict[ifo])
        if asds is not None:
            for ifo, asdpath in asds.items():
                # get psd files within the given psd base path
                # make sure only psds are in this dir!
                asd_files = [
                    os.path.join(psd_basedir, asdpath, x)
                    for x in os.listdir(os.path.join(psd_basedir, asdpath))
                ]
                # natsort them for use readability / reproducibility of indices
                asd_files = natsorted(asd_files, key=lambda x: x.split("/")[-1])
                # convert psds into pycbc fseries for use
                for fname in asd_files:
                    psd = wfwraps.read_psd_from_txt(
                        fname,
                        delta_f=psd_delta_f,
                        length=psd_length,
                        low_freq_cutoff=psd_low_freq_cutoff,
                        basedir=psd_basedir,
                        asd=True,
                    )
                self.psd_dict[ifo] += [psd]
                # set the number of psds in use for later if not already set
                if self.num_psds is None:
                    self.num_psds = len(self.psd_dict[ifo])
                # else assertion to make sure number of psds is set correctly
                else:
                    assert self.num_psds == len(self.psd_dict[ifo])
        self.det_obj_dict = None

    def set_cal_file(self, cal_file):
        """
        Sets the calibration information for the population

        Inputs:
        -------------
        cal_file - a pickled file containing a calibration instances object
        """
        self.cal_file = cal_file
        self.num_cals = wfwraps.get_cal_dict_list(cal_file, return_length=True)

    def set_ifo_segmentlist(self, chunk_dir):
        """
        Sets the segment list to use, from a gstlal
        marginalized likelihood file

        Inputs:
        -------------
        post_marg_likel_xml - a post marginalized likelihood file
        from the gstlal combination/reranking dir

        Modifies:
        -------------
        sets self.segment_list to correspond to this file's segment list
        """
        self.seg_list_dict = {}
        for ifo in self.ifos:
            seg_file = os.path.join(chunk_dir, f"{ifo}.intermediate")
            self.seg_list_dict[ifo] = ligoutils.get_segs_from_ifo_segs(seg_file)
            self.seg_list_dict[ifo].coalesce()
        self.seg_list_dict = segmentlistdict(self.seg_list_dict)

    def calc_snr(
        self,
        Inj_WF,
        row=0,
        fmin=11,
        fmax=2048,
        delta_t=1.0 / 4096.0,
        delta_f=0.25,
        cal_dict=None,
        benchmark=False,
        use_stored_wf=False,
        cal_snr_method="original",
        psds=None,
        param_series=None,
    ):
        """
        Calculates the SNR given input row and waveform

        Inputs:
        ------------
        Inj_WF - the waveform to use in the waveform generation
        row - the index of the row to take as args
        fmin - the minimum frequency of integration
        fmax - the maximum frequency of integration
        delta_t - the delta_t to generate the waveform at
        delta_f - the delta_f to FFT (if necessary) or generate the waveform to
        cal_dict - a dictionary to potentially use
        in performing calibration adjustment
        param_series - a series, as from a dataframe, but manually passed instead of obtained by index
        """
        # get a single row's parameters
        if param_series is not None:
            df_slice = param_series
        else:
            df_slice = self.population_df.iloc[row]
        # convert it to a parameters dict
        param_args = df_slice.to_dict()
        strain_dict = {}
        # preload detectors
        if self.det_obj_dict is None:
            self.det_obj_dict = {}
            for ifo in psds.keys():
                self.det_obj_dict[ifo] = Detector(ifo)
        # compute strains
        for ifo, psd in psds.items():
            if use_stored_wf:
                # if using the stored wf we want to skip
                continue
            if Inj_WF in fd_approximants():
                print("Waveform call using get_waveform_fd")
                strain_dict[ifo], _, _ = wfwraps.compute_strain_fd(
                    param_args,
                    Inj_WF,
                    ifo,
                    delta_f,
                    fmin,
                    df_slice["tc"],
                    detector_obj=self.det_obj_dict[ifo],
                    benchmark=benchmark,
                )
            elif Inj_WF in td_approximants():
                print("Waveform call using get_waveform_td")
                strain_td, _, _ = wfwraps.compute_strain_td(
                    param_args,
                    Inj_WF,
                    ifo,
                    delta_t,
                    fmin,
                    df_slice["tc"],
                    detector_obj=self.det_obj_dict[ifo],
                    benchmark=benchmark,
                )
                strain_dict[ifo] = make_frequency_series(strain_td)
            else:
                raise ValueError
        if not use_stored_wf:
            self.temp_wf_strain = strain_dict
        # compute net snr
        snr_net = wfwraps.compute_snr_fd(
            self.temp_wf_strain,
            psds,
            fmin=fmin,
            fmax=fmax,
            delta_t=delta_t,
            cal_dict=cal_dict,
            cal_snr_method=cal_snr_method,
            benchmark=benchmark,
        )["net"]
        return snr_net

    def calc_snrs(
        self,
        Inj_WF,
        fmin=20,
        fmax=2048,
        delta_t=1.0 / 4096.0,
        delta_f=0.25,
        benchmark=False,
        cal_snr_method="derek",
        manual_dataframe=None,
    ):
        """
        A loop of SNR calculation over the population, with checkpointing

        Inputs:
        --------------
        Inj_WF - the waveform to use in the waveform generation
        fmin - the minimum frequency of integration
        fmax - the maximum frequency of integration
        delta_t - the delta_t to generate the waveform at
        """
        # set signal responses to allow checkpointing
        try:
            signal.signal(signal.SIGTERM, self.checkpoint_wrapper)
            signal.signal(signal.SIGINT, self.checkpoint_wrapper)
            signal.signal(signal.SIGALRM, self.checkpoint_wrapper)
        except AttributeError:
            print("Failed to set signal responses")
        # preload detectors
        self.det_obj_dict = {}
        for ifo in self.psd_dict.keys():
            self.det_obj_dict[ifo] = Detector(ifo)
        if self.num_cals is not None:
            calibration_data = wfwraps.get_cal_dict_list(self.cal_file)
        # loop over the population, computing snrs
        now = time.time()
        self.temp_wf_strain = None
        self.previous_config = pd.DataFrame()
        if not self.variable_network:
            usable_ifos = self.ifos
        for i in range(self.population_df.shape[0]):
            if time.time() - now > 3600:
                self.checkpoint_wrapper()
            # SNR is the last term, if it's 0 populate it
            if (
                self.population_df.iloc[
                    i, self.population_df.columns.get_loc("opt_snr")
                ]
                == 0
            ):
                # if segment list, then first filter if
                # the time is in the live segments
                if self.variable_network:
                    assert self.seg_list_dict is not None
                    geocent_time = self.population_df.iloc[
                        i, self.population_df.columns.get_loc("tc")
                    ]
                    usable_ifos = []
                    for ifo, seglist in self.seg_list_dict.items():
                        if geocent_time in seglist:
                            usable_ifos += [ifo]
                    # if it's not in a live seg for any detector
                    # set the optimal SNR very low
                    # non-zero just to prevent these points
                    # from being reanalyzed
                    if usable_ifos == []:
                        self.population_df.iloc[
                            i, self.population_df.columns.get_loc("opt_snr")
                        ] = 0.0001
                # get the calibration index
                cal_idx = int(
                    self.population_df.iloc[
                        i, self.population_df.columns.get_loc("cal_idx")
                    ]
                )
                # if it's an interesting (non-uncalibrated)
                # index get the calibration dict
                if cal_idx != -1:
                    active_cal_dict = calibration_data[cal_idx]
                else:
                    active_cal_dict = {}

                psds = {}
                psd_idx = int(
                    self.population_df.iloc[
                        i, self.population_df.columns.get_loc("psd_idx")
                    ]
                )
                for ifo in usable_ifos:
                    psds[ifo] = self.psd_dict[ifo][psd_idx]

                self.current_config = self.population_df.iloc[
                    i, : self.population_df.columns.get_loc("cal_idx")
                ]
                if self.current_config.equals(self.previous_config):
                    self.population_df.iloc[
                        i, self.population_df.columns.get_loc("opt_snr")
                    ] = self.calc_snr(
                        Inj_WF,
                        row=i,
                        fmin=fmin,
                        fmax=fmax,
                        delta_t=delta_t,
                        delta_f=delta_f,
                        cal_dict=active_cal_dict,
                        benchmark=benchmark,
                        use_stored_wf=True,
                        cal_snr_method=cal_snr_method,
                        psds=psds,
                    )
                else:
                    self.population_df.iloc[
                        i, self.population_df.columns.get_loc("opt_snr")
                    ] = self.calc_snr(
                        Inj_WF,
                        row=i,
                        fmin=fmin,
                        fmax=fmax,
                        delta_t=delta_t,
                        delta_f=delta_f,
                        cal_dict=active_cal_dict,
                        benchmark=benchmark,
                        use_stored_wf=False,
                        cal_snr_method=cal_snr_method,
                        psds=psds,
                    )
                self.previous_config = copy.copy(self.current_config)
        return self.population_df
