from gstlal import far
from gwpy.table import EventTable
from ligo.lw import utils as ligolw_utils
from ligo.segments import segment, segmentlist


def gstlal_get_segment_list_from_post_marg_likel_file(filename):
    """


    A wrapper to get the segment list used by gstlal out of a post marginalized likelihood file.

    Inputs:
    ----------
    filename - the name of the post marginalized likelihood file

    Returns:
    -----------
    segment_list - a segment list containing the live segments from gstlal
    """
    _, rankingstatpdf = far.parse_likelihood_control_doc(
        ligolw_utils.load_filename(
            filename, contenthandler=far.RankingStat.LIGOLWContentHandler
        )
    )
    segment_list = rankingstatpdf.segments
    return segment_list


def get_livetime_from_segment_list(segmentlist):
    """
    Returns the live time associated with a given segment list

    Inputs:
    ----------------
    segmentlist - the list of segments to compute live time for

    Returns:
    ---------------
    livetime - the time in seconds that the segments span
    """
    livetime = 0
    for seg in segmentlist:
        livetime += seg[1] - seg[0]
    return livetime


def get_segs_from_ifo_segs(seg_file):
    """
    Gets a segment intermediate file, and produces an associated segment list

    Inputs:
    ----------
    seg_file - an xml containing a 'segment_summary' table with beginning and end times for segments

    Returns:
    ----------
    seglist - a segment list of the segments from the input file
    """
    t = EventTable.read(seg_file, tablename="segment")
    seglist = []
    for i in range(len(t)):
        start_time = t["start_time"][i]
        end_time = t["end_time"][i]
        seglist += [segment(start_time, end_time)]
    seglist = segmentlist(seglist)
    return seglist
