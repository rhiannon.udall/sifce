import os
import sys
import time

import numpy as np
import pycbc.strain.recalibrate as recalibrate
from pycbc.detector import Detector
from pycbc.filter import sigmasq
from pycbc.psd import interpolate as psd_interpolate
from pycbc.psd import read as psd_read
from pycbc.types import TimeSeries
from pycbc.waveform import get_fd_waveform, get_td_waveform, taper_timeseries

sys.modules["recalibrate"] = recalibrate


def pad_timeseries_to_integer_length(timeseries, sample_rate):
    """

    This function zero pads a time series so that its length is an integer
    multiple of the sampling rate.

    Padding is adding symmetically to the start and end of the time series.
    If the number of samples to pad is odd then the end zero padding will have
    one more sample than the start zero padding.

    Inputs:
    --------------
    timeseries - the timeseries to pad to integer length
    sample_rate - the sample rate to pad based on

    Returns:
    ------------
    the padded timeseries
    """

    # calculate how many sample points needed to pad to get
    # integer second time series
    remainder = sample_rate - len(timeseries) % sample_rate
    start_pad = int(remainder / 2)
    end_pad = int(remainder - start_pad)

    # make arrays of zeroes
    start_array = np.zeros(start_pad)
    end_array = np.zeros(end_pad)

    # pad waveform with arrays of zeroes
    initial_array = np.concatenate([start_array, timeseries, end_array])
    return TimeSeries(
        initial_array,
        delta_t=timeseries.delta_t,
        epoch=timeseries.start_time,
        dtype=timeseries.dtype,
    )


def pad_to_seglen(timeseries, sample_rate, seglen=8):
    """
    Pads a timeseries to the desired seglen

    Inputs:
    ----------
    timeseries - a timeseries to pad
    sample_rate - the sample rate to pad based on
    seglen - the segment length to pad to

    Returns:
    ---------
    The padded timeseries

    """
    # calculate how many sample points needed to pad to get
    # integer second time series
    remainder = sample_rate * seglen - len(timeseries)
    start_pad = int(remainder / 2)
    end_pad = int(remainder - start_pad)

    # make arrays of zeroes
    start_array = np.zeros(start_pad)
    end_array = np.zeros(end_pad)

    # pad waveform with arrays of zeroes
    initial_array = np.concatenate([start_array, timeseries, end_array])
    return TimeSeries(
        initial_array,
        delta_t=timeseries.delta_t,
        epoch=timeseries.start_time,
        dtype=timeseries.dtype,
    )


def compute_strain_td(
    param_args, approx, ifo, delta_t, f_lower, end_time, detector_obj=None
):
    """
    Generate strain TimeSeries for IFO

    Inputs:
    -------
    param_args - a dictionary of arguments to pass get_td_waveform,
    using Bilby naming conventions
    approx - the waveform approximant to use
    ifo - the ifo in question (e.g. H1)
    delta_t - inverse sample rate to produce waveform at
    f_lower - starting frequency of the waveform
    end_time - time at which segment will end (for antenna pattern)

    Returns:
    ---------
    strain - the time domain detector strain
    hp - the plus polarization strain
    hc - the cross polarization strain

    """

    hp, hc = get_td_waveform(
        approximant=approx,
        mass1=param_args["mass_1"],
        mass2=param_args["mass_2"],
        spin1z=param_args["spin_1z"],
        spin1x=param_args["spin_1x"],
        spin1y=param_args["spin_1y"],
        spin2z=param_args["spin_2z"],
        spin2x=param_args["spin_2x"],
        spin2y=param_args["spin_2y"],
        inclination=param_args["theta_jn"],
        coa_phase=param_args["phase"],
        distance=param_args["luminosity_distance"],
        f_lower=f_lower,
        delta_t=delta_t,
    )

    # zero pad polarizations to get integer second time series
    sample_rate = 1.0 / delta_t
    hp = pad_timeseries_to_integer_length(hp, sample_rate)
    hc = pad_timeseries_to_integer_length(hc, sample_rate)

    # get Detector instance for IFO
    if detector_obj is None:
        det = Detector(ifo)
    else:
        det = detector_obj

    # get antenna pattern
    fp, fc = det.antenna_pattern(
        param_args["ra"], param_args["dec"], param_args["psi"], end_time
    )

    # calculate strain
    strain = fp * hp + fc * hc
    strain = pad_timeseries_to_integer_length(strain, sample_rate)

    # taper waveform
    hp = taper_timeseries(hp, tapermethod="TAPER_START")
    hc = taper_timeseries(hc, tapermethod="TAPER_START")
    strain = taper_timeseries(strain, tapermethod="TAPER_START")

    return strain, hp, hc


def compute_strain_fd(
    param_args,
    approx,
    ifo,
    delta_f,
    f_lower,
    end_time,
    detector_obj=None,
    benchmark=False,
):
    """
    Generate strain TimeSeries for IFO

    Inputs:
    -------
    param_args - a dictionary of arguments to pass get_td_waveform,
    using Bilby naming conventions
    approx - the waveform approximant to use
    ifo - the ifo in question (e.g. H1)
    delta_t - inverse sample rate to produce waveform at
    f_lower - starting frequency of the waveform
    end_time - time at which segment will end (for antenna pattern)

    Returns:
    ---------
    strain - the time domain detector strain
    hp - the plus polarization strain
    hc - the cross polarization strain

    """
    if benchmark:
        start = time.time()

    hptilde, hctilde = get_fd_waveform(
        approximant=approx,
        mass1=param_args["mass_1"],
        mass2=param_args["mass_2"],
        spin1z=param_args["spin_1z"],
        spin1x=param_args["spin_1x"],
        spin1y=param_args["spin_1y"],
        spin2z=param_args["spin_2z"],
        spin2x=param_args["spin_2x"],
        spin2y=param_args["spin_2y"],
        inclination=param_args["theta_jn"],
        coa_phase=param_args["phase"],
        distance=param_args["luminosity_distance"],
        f_lower=f_lower,
        delta_f=delta_f,
    )

    # get Detector instance for IFO
    if detector_obj is None:
        det = Detector(ifo)
    else:
        det = detector_obj

    # get antenna pattern
    fp, fc = det.antenna_pattern(
        param_args["ra"], param_args["dec"], param_args["psi"], end_time
    )

    # calculate strain
    straintilde = fp * hptilde + fc * hctilde

    if benchmark:
        print(f"Waveform computation took {time.time() - start} seconds")

    return straintilde, hptilde, hctilde


def compute_snr_fd(
    strains,
    psd_dict,
    fmin=10,
    fmax=2048,
    delta_t=1.0 / 4096.0,
    cal_dict={},
    cal_snr_method="original",
    benchmark=False,
):
    """
    Compute optimal network SNR for some set of ifos,
    optionally applying calibration

    Inputs:
    -------
    strains - a dictionary of strains (FrequencySeries objects), ifo label
    psd_dict -  a dictionary of psds (FrequencySeries objects), ifo label
    fmin - minimum frequency of integration
    fmax - maximum frequency of integration
    delta_t - inverse sample rate
    cal_dict - a dictionary of calibration splines, ifo label
    cal_snr_method - 'original', 'derek', or 'match' for type of SNR computation to use

    Returns:
    --------
    snr_dict - a dict of optimal snr for each ifo, and network snr

    """

    snr_dict = dict()
    ifos = psd_dict.keys()
    netsnr = 0.0

    for i, ifo in enumerate(ifos):

        strain_tilde = strains[ifo]

        if fmax is None:
            fmax = min([0.5 / delta_t, psd_dict[ifo].sample_frequencies.max()])

        # interpolate PSD to waveform delta_f
        if psd_dict[ifo].delta_f != strain_tilde.delta_f:
            psd_dict[ifo] = psd_interpolate(psd_dict[ifo], strain_tilde.delta_f)

        if cal_dict == {}:
            # calculate sigma-squared SNR
            if benchmark:
                start = time.time()
            snr = np.sqrt(
                float(
                    sigmasq(
                        strain_tilde,
                        psd=psd_dict[ifo],
                        low_frequency_cutoff=fmin,
                        high_frequency_cutoff=fmax,
                    )
                )
            )
            if benchmark:
                print(f"sigmasq call took {time.time() - start} seconds")
        else:
            if cal_snr_method == "original":
                snr = float(
                    cal_dict[ifo].calibration_internal_snr(
                        strain_tilde,
                        psd=psd_dict[ifo],
                        low_frequency_cutoff=fmin,
                        high_frequency_cutoff=fmax,
                        benchmark=benchmark,
                    )
                )
            elif cal_snr_method == "derek":
                snr = float(
                    cal_dict[ifo].snr_against_calibrated(
                        strain_tilde,
                        psd=psd_dict[ifo],
                        low_frequency_cutoff=fmin,
                        high_frequency_cutoff=fmax,
                        benchmark=benchmark,
                    )
                )
            elif cal_snr_method == "match":
                snr = float(
                    cal_dict[ifo].match_snr_against_calibrated(
                        strain_tilde,
                        psd=psd_dict[ifo],
                        low_frequency_cutoff=fmin,
                        high_frequency_cutoff=fmax,
                        benchmark=benchmark,
                    )
                )
            else:
                print("Method of Calibration SNR computation not recognized")
                raise (TypeError)

        # Compute SNR
        snr_dict[ifo] = snr

        netsnr += snr_dict[ifo] ** 2

    snr_dict["net"] = np.sqrt(netsnr)

    return snr_dict


def read_psd_from_txt(
    psdname,
    delta_f=0.25,
    length=16384,
    low_freq_cutoff=11,
    basedir="/home/richard.udall/approx_sens_curves",
    asd=False,
):
    """
    Reads in a two column asd or psd, converts to psd if necessary,
    and performs sanitization operations to keep pycbc happy

    Inputs:
    ---------
    psdname - the name of the file in the base directory to read in
    delta_f - the inverse sampling rate to interpolate the psd to
    length - the length of the psd to generate
    low_freq_cutoff - the lower frequency to start psd generation at
    (everything below will go to 1)
    basedir - the directory to read the psds from
    asd - True/False to determine if values should be squared to get psd

    Returns:
    ---------
    psd - the psd as a FrequencySeries
    """
    # read data
    read_data = np.genfromtxt(os.path.join(basedir, psdname))
    # get the amplitude portion, as opposed to the frequencies
    psd_data = read_data[:, 1]
    # if it's an asd square it
    if asd:
        psd_data = psd_data**2
    # the frequencies
    freq = read_data[:, 0]
    # plug into pycbc
    psd = psd_read.from_numpy_arrays(freq, psd_data, length, delta_f, low_freq_cutoff)
    # 0 is bad, so if it's zero fix it to 1 instead
    # (equivalent to just cutting out this frequency)
    psd.data[np.argwhere(psd.data == 0)] = 1
    return psd


def get_cal_dict_list(fpath, return_length=False):
    """
    Reads a calibration file, returns either the data file or the number of instances

    Inputs:
    ----------
    fpath - the calibration file's path
    return_length - If true, returns the number of indices, rather than a calibration dict
    if false, returns the full calibration data file instead
    """
    import dill

    with open(fpath, "rb") as f:
        # standard opening of a pickle
        data = dill.load(f)
    # for when we are getting the length during population creation
    if return_length:
        return len(data["instances"])
    # all the rest of the time we just want to get one dictionary out
    else:
        return data["instances"]
