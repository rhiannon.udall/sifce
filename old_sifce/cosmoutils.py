import copy
import os

import matplotlib.pyplot as plt
import numpy as np
from astropy import constants as const
from astropy import cosmology as cosmology
from astropy import units as u
from scipy import integrate


def compute_zvals(dL, cosmo_model=cosmology.Planck15):
    """

    A wrapper of computing redshift from luminosity distance
    which works correctly for arrays
    https://docs.astropy.org/en/stable/api/astropy.cosmology.z_at_value.html

    Inputs:
    ---------
    dL - the luminosity distance (array-like) to compute the zvalues for
    cosmo_model - the cosmology model to use in computing redshift

    Returns:
    ----------
    zvals - a corresponding array of redshifts for the input dLs
    """
    dL = dL * u.Mpc
    zmin = cosmology.z_at_value(cosmo_model.luminosity_distance, dL.min())
    zmax = cosmology.z_at_value(cosmo_model.luminosity_distance, dL.max())
    zgrid = np.logspace(np.log10(zmin.value), np.log10(zmax.value), 100)
    Dgrid = cosmo_model.luminosity_distance(zgrid)
    zvals = np.interp(dL.value, Dgrid.value, zgrid)
    return zvals


def redshift_mass(m_source, dL, cosmo_model=cosmology.Planck15):
    """
    A function to get detector mass for a fixed
    source mass at varying distances

    Inputs:
    ----------
    m_source - a float value for the source mass
    dL - an array of distances to get the redshifted mass for
    cosmo_model - the cosmological model to use in computing the redshift

    Returns:
    -------------
    An array (of same dimensions as dL) with redshifted masses,
    corresponding to input distances
    """
    zvals = compute_zvals(dL, cosmo_model)
    return m_source * (1 + zvals)


# a placeholder function to satisfy requirements of structuring
def constant_rate(z, constant=1):
    """
    Returns a constant rate for an array of z values

    Inputs:
    ----------
    z - an array of z values
    constant - the constant to set

    Returns:
    -----------
    rate_array - an array with the shape of z, all values = constant
    """
    rate_array = np.zeros(z.shape)
    rate_array[:] = constant
    return rate_array


def compute_efficiency_dL(opt_snrs, dLs, threshold=11):
    """
    A method to get the efficiency per distance bin, for use in the sensitive volume integral

    Inputs:
    -----------
    opt_snrs - an np array of optimal snrs
    dLs - an np array of luminosity distances, corresponding to the above
    threshold - an snr threshold to cut on

    Returns:
    ----------
    efficiency_dL - an array of distance bins and corresponding efficiencies
    (0th and 1st column respectively)

    """
    # find the indices on distances where the snr is above threshold
    above_thresh_dL = dLs[np.where(opt_snrs > threshold)]
    # get the relevant bins
    dist_bins = np.unique(dLs)
    # find the total number of injections per bin
    # TODO modify to separate by calibration realization
    num_per_dist = int(len(dLs) / len(dist_bins))
    # make an efficiency array : it will be (distance bin, success fraction in bin)
    efficiency_dL = np.empty((dist_bins.shape[0], 2))
    efficiency_dL[:, 0] = dist_bins[:]
    # some hacking to make the bins work correctly
    dummy_bins = np.concatenate(
        (dist_bins, [dist_bins[-1] + (dist_bins[1] - dist_bins[0])])
    )
    above_thresh_histo, thresh_histo_bins = np.histogram(
        above_thresh_dL, bins=dummy_bins
    )
    # get the success fraction
    for i in range(efficiency_dL.shape[0]):
        efficiency_dL[i, 1] = above_thresh_histo[i] / num_per_dist
    return efficiency_dL


def compute_VTsens(efficiency_dL, cosmo_model=cosmology.Planck15, years=1):
    """
    Computes the sensitive volume given an efficiency array

    Inputs:
    -----------
    efficiency_dL - an array of distance bins and efficiencies, as outputted by
    compute_efficiency_dL
    cosmo_model - the cosmological model to use
    years - the nominal duration of the injection campaign

    Returns:
    -----------
    A cumulative integral of sensitive volume
    """
    dist_bins = efficiency_dL[:, 0]
    # get redshifts for our distance bins
    redshifts = compute_zvals(dist_bins, cosmo_model)
    # get our efficiency bins, put on units (they actually come later but this makes astropy happy)
    kernel = copy.copy(efficiency_dL[:, 1])
    # Mpc^2 # introduce distance^2 element
    kernel *= dist_bins**2
    # for dVc/dz, and also for time dilation
    kernel /= (1 + redshifts) ** 3
    # an Efunc from the dVc/dz
    kernel /= cosmo_model.efunc(redshifts)
    # integrate
    integral = integrate.cumtrapz(kernel, x=redshifts) * u.Mpc**2
    # multiply by constants and put into helpful units
    return (integral * 4 * np.pi * const.c / cosmo_model.H(0) * years * u.yr).decompose(
        bases=[u.Gpc, u.yr]
    )


def compute_N_over_T(
    self, efficiency_dL, rate_func, cosmo_model=cosmology.Planck15, rate_kwargs={}
):
    """
    Computes the sensitive volume given an efficiency array

    Inputs:
    -----------
    efficiency_dL - an array of distance bins and efficiencies, as outputted by
    compute_efficiency_dL
    rate_func - a fiducial rate function
    cosmo_model - the cosmological model to use
    rate_kwargs - kwargs for the rate function

    Returns:
    -----------
    A cumulative integral of the expected rate of detections, over the sensitive volume
    """
    dist_bins = efficiency_dL[:, 0]
    # basically as above, but with a rate function as well
    redshifts = compute_zvals(dist_bins, cosmo_model)
    kernel = copy.copy(efficiency_dL[:, 1]) * u.Mpc**2
    # Mpc^2
    kernel *= dist_bins**2
    # Therefore, this should be in rate Mpc^-3 yr^-1
    kernel *= rate_func(redshifts, **rate_kwargs) / u.Mpc**3 / u.yr
    kernel /= (1 + redshifts) ** 3
    kernel /= cosmo_model.efunc(redshifts)
    integral = integrate.cumtrapz(kernel, x=redshifts)
    return (integral * 4 * np.pi * const.c / cosmo_model.H(0)).decompose(
        bases=[u.m, u.yr]
    )


def plot_efficiency_dL(
    self,
    efficiency_dL,
    outdir=None,
    outname="my_population_efficiency_histogram",
    threshold=11,
):
    """
    Plots efficiency as a function of distance

    Inputs:
    -----------
    efficiency_dL - an array of distance bins and efficiencies, as outputted by
    compute_efficiency_dL
    outdir - the directory to output the plot
    outname - the name of the plot
    threshold - an snr threshold to cut on

    """
    if not outdir:
        outdir = os.getcwd()
    # plot the efficiencies we've computed
    plt.hist(efficiency_dL[:, 1], bins=efficiency_dL[:, 0])
    plt.xlim(0, efficiency_dL[-1, 1])
    plt.xlabel("Luminosity Distance (Mpc)")
    plt.ylabel("Efficiency")
    plt.savefig(os.path.join(outdir, outname + ".png"))
